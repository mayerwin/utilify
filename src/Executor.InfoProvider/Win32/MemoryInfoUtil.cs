using Utilify.Platform.Contract;
using System.Runtime.InteropServices;

namespace Utilify.Platform.Executor.InfoProvider.Win32
{
    internal class MemoryInfoUtil
    {
        public struct MEMORYSTATUSEX
        {
            public int dwLength;
            public int dwMemoryLoad;
            public ulong ullTotalPhys;
            public ulong ullAvailPhys;
            public ulong ullTotalPageFile;
            public ulong ullAvailPageFile;
            public ulong ullTotalVirtual;
            public ulong ullAvailVirtual;
            public ulong ullAvailExtendedVirtual;
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool GlobalMemoryStatusEx(ref MEMORYSTATUSEX lpBuffer);

        public static long GetTotalMemory()
        {
            MEMORYSTATUSEX memStat = new MEMORYSTATUSEX();
            memStat.dwLength = 64;
            bool b = GlobalMemoryStatusEx(ref memStat);
            return (long)memStat.ullTotalPhys;
        }

        public static long GetAvailableMemory()
        {
            MEMORYSTATUSEX memStat = new MEMORYSTATUSEX();
            memStat.dwLength = 64;
            bool b = GlobalMemoryStatusEx(ref memStat);
            return (long)memStat.ullAvailPhys;
        }

        public static MemorySystemInfo GetMemoryInfo()
        {
            MemorySystemInfo info = new MemorySystemInfo(GetTotalMemory());
            //logger.Debug("Total Memory : " + MemoryInfoUtil.GetTotalMemory());
            //logger.Debug("Available Memory : " + MemoryInfoUtil.GetAvailableMemory());

            return info;
        }

        public static MemoryPerformanceInfo GetMemoryPerfInfo()
        {
            // Issue 056: May decide to report Current Usage of this process instead of overal system usage
            MemoryPerformanceInfo perfInfo = new MemoryPerformanceInfo(GetAvailableMemory(), GetTotalMemory() - GetAvailableMemory());

            return perfInfo;
        }
    }
}