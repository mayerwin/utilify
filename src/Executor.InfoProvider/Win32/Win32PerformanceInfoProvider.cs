using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider.Win32
{
    internal class Win32PerformanceInfoProvider : IPerformanceInfoProvider
    {
        public PerformanceInfo GetPerformanceInfo(string id)
        {
            PerformanceInfo perfInfo = 
                new PerformanceInfo(
                    id,
                    GetProcessorPerfInfo(),
                    GetDiskPerfInfo(), 
                    GetMemoryPerfInfo());

            return perfInfo;
        }

        private ProcessorInfoUtil processorInfoUtil;
        private ProcessorInfoUtil ProcessorInfoUtil
        {
            get
            {
                if (processorInfoUtil == null)
                    processorInfoUtil = new ProcessorInfoUtil();
                return processorInfoUtil;
            }
        }

        public ProcessorPerformanceInfo[] GetProcessorPerfInfo()
        {
            return ProcessorInfoUtil.GetProcessorPerfInfo();
        }

        public DiskPerformanceInfo[] GetDiskPerfInfo()
        {
            return DiskInfoUtil.GetDiskPerfInfo();
        }

        public MemoryPerformanceInfo GetMemoryPerfInfo()
        {
            return MemoryInfoUtil.GetMemoryPerfInfo();
        }
    }
}