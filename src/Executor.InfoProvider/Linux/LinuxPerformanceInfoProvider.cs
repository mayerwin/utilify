using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor.InfoProvider.Linux
{
    internal class LinuxPerformanceInfoProvider : IPerformanceInfoProvider
    {
        public PerformanceInfo GetPerformanceInfo(string id)
        {
            PerformanceInfo perfInfo = 
                new PerformanceInfo(
                    id,
                    GetProcessorPerfInfo(),
                    GetDiskPerfInfo(), 
                    GetMemoryPerfInfo());

            return perfInfo;
        }

        public ProcessorPerformanceInfo[] GetProcessorPerfInfo()
        {
            return ProcessorInfoUtil.GetProcessorPerfInfo();
        }

        public DiskPerformanceInfo[] GetDiskPerfInfo()
        {
            return DiskInfoUtil.GetDiskPerfInfo();
        }

        public MemoryPerformanceInfo GetMemoryPerfInfo()
        {
            return MemoryInfoUtil.GetMemoryPerfInfo();
        }
    }
}
