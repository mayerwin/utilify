Platform
25/05/2008 11:54 PM

Project Name                                            Assembly Version    File Version        Product Version     
--------------------------------------------------------------------------------------------------------------------
Demos                                                                                                               
 FractalGenerator                                       1.0.0.0             1.0.0.0                                 
 FractalGeneratorLib                                    1.0.0.0             1.0.0.0                                 
 Renderer                                               1.0.0.0             1.0.0.0                                 
 RendererLibrary                                        1.0.0.0             1.0.0.0                                 
Executor Projects                                                                                                   
 DotNetExecutor                                         0.9.0.1                                                     
 Execution                                              0.9.0.1                                                     
 Execution.Shared                                       0.9.0.1                                                     
 Executor                                               0.9.0.1                                                     
 Executor.ConsoleHost                                   0.9.0.1                                                     
 Executor.InfoProvider                                  0.9.0.1                                                     
 Executor.UI                                            0.9.0.1                                                     
 ExecutorService                                        0.9.0.1                                                     
HelperTools                                                                                                         
 BuildGacList                                           1.0.0.0             1.0.0.0                                 
 TestManagement                                         1.0.0.0             1.0.0.0                                 
Manager Projects                                                                                                    
 D:\Utilify\software\platform\src\ManagerWCFService\                                                                
 ManagementConsole                                      0.9.1.*                                                     
 Manager                                                0.9.1.*                                                     
 Manager.ConsoleHost                                    0.9.1.*                                                     
 ManagerService                                         0.9.1.*                                                     
Setup                                                                                                               
 Manager.VdProj                                                                                 1.0.0               
Solution Items                                                                                                      
Tests                                                                                                               
 AClassLibrary                                          1.0.0.0             1.0.0.0                                 
 AdditionJob                                            1.0.0.0             1.0.0.0                                 
 Client.Tests                                           1.0.0.0             1.0.0.0                                 
 DependentClassLibrary                                  1.0.0.0             1.0.0.0                                 
 Executor.Tests                                         1.0.0.0             1.0.0.0                                 
 Framework.Tests                                        1.0.0.0             1.0.0.0                                 
 Manager.Tests                                          1.0.0.0             1.0.0.0                                 
 Runtime.Tests                                          1.0.0.0             1.0.0.0                                 
 TestConsole                                            1.0.0.0             1.0.0.0                                 
 TestHelper                                             1.0.0.0             1.0.0.0                                 
Framework                                               0.9.0.1                                                     
Runtime                                                 0.9.0.1                                                     
