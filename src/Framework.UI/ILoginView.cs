﻿using System;

namespace Utilify.Framework.UI
{
    public interface ILoginView<T>
    {
        //public event EventHandler<EventArgs> Connected;
        /// <summary>
        /// Occurs when the connection settings are available and the user has requested to connect.
        /// The caller can handle this even to begin connecting to the service.
        /// </summary>
        event EventHandler<ConnectionEventArgs<T>> Connecting;
        /// <summary>
        /// Occurs when the user has requested cancelling the connection process.
        /// </summary>
        event EventHandler<EventArgs> Cancelling;

        string ErrorDetail { get; set; }
        string ErrorText { get; set; }
        T GetConnectionSettings();
        string MessageText { get; set; }
        void SaveSettings();
        void SetConnectionSettings(T settings);
        LoginViewState State { get; set; }
    }
}
