﻿using System;
using System.Drawing;
using System.Net;
using System.Security.Principal;
using System.Windows.Forms;
using Utilify.Platform;
using Utilify.Platform.Configuration;

namespace Utilify.Framework.UI
{
    /// <summary>
    /// Represents the LoginView UI component used for getting user input for connecting to a service on the Manager.
    /// </summary>
    public partial class LoginView : UserControl, ILoginView<ConnectionSettings>
    {
        public event EventHandler<ConnectionEventArgs<ConnectionSettings>> Connecting;
        public event EventHandler<EventArgs> Cancelling;

        private LoginViewState state = LoginViewState.LoginForm;
        private string errorDetail;

        /// <summary>
        /// Represents the logger to use for logging messages.
        /// </summary>
        protected Logger logger;

        /// <summary>
        /// Represents the main panel component of the UI, that can be manipulated by derived classes,
        /// to extend or modify the user interface.
        /// </summary>
        protected System.Windows.Forms.Panel MainPanel;

        //private bool settingsModified = false;
        //private ConnectionSettings settings = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginView"/> class.
        /// </summary>
        public LoginView()
        {
            InitializeComponent();

            // Setup Logging.
            logger = new Logger();

            if (!DesignMode)
            {
                //init auth-mode list items
                string[] types = Enum.GetNames(typeof(CredentialType));
                cbAuthMode.DataSource = types;

                //init protocol list items
                string[] protos = Enum.GetNames(typeof(CommunicationProtocol));
                cbProtocol.DataSource = protos;

                SetInitialSettings();

                //looks like setting Visible = true/false doesnt work! 
                //when a link is in a user control nested in another user control
                lnkErrorDetail.Text = ""; //initial don't show the details link
            }

            //subscribe to change events:
            //foreach (Control control in this.Controls)
            //{
            //    if (control is TextBoxBase)
            //    {
            //        TextBoxBase txBox = control as TextBoxBase;
            //        txBox.TextChanged += new EventHandler(Control_ValueChanged);
            //    }
            //    else if (control is ListControl)
            //    {
            //        ListControl list = control as ListControl;
            //        list.SelectedValueChanged += new EventHandler(Control_ValueChanged);
            //    }
            //    else if (control is CheckBox)
            //    {
            //        CheckBox chk = control as CheckBox;
            //        chk.CheckStateChanged += new EventHandler(Control_ValueChanged);
            //    }
            //}
        }

        //void Control_ValueChanged(object sender, EventArgs e)
        //{
        //    //this.settingsModified = true;
        //}

        internal void InvokeShow()
        {
            this.Invoke(new MethodInvoker(this.Show));
        }

        internal void InvokeHide()
        {
            this.Invoke(new MethodInvoker(this.Hide));
        }

        /// <summary>
        /// Sets the connection settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public void SetConnectionSettings(ConnectionSettings settings) //, bool excludePassword)
        {
            if (settings == null)
                throw new ArgumentNullException("settings");

            //set credential type in UI
            for (int i = 0; i < cbAuthMode.Items.Count; i++)
            {
                string item = cbAuthMode.Items[i].ToString();
                if (item == settings.CredentialType.ToString())
                {
                    cbAuthMode.SelectedIndex = i;
                    break;
                }
            }

            //set proto in UI
            for (int i = 0; i < cbProtocol.Items.Count; i++)
            {
                string item = cbProtocol.Items[i].ToString();
                //if (item == settings.Protocol.ToString())
                //{
                //    cbProtocol.SelectedIndex = i;
                //    break;
                //}
            }

            tbHostname.Text = settings.Host;
            tbPort.Text = settings.Port.ToString();

            txUsername.Text = settings.UsernameCredential.Username;
            //if (!excludePassword)
                txPassword.Text = settings.UsernameCredential.Password;

            lbWindowsUsername.Text = WindowsIdentity.GetCurrent().Name;

            txDnsIdentity.Text = settings.ServiceCertificate.DnsIdentity;
            chkValidateDnsIdentity.Checked = settings.ServiceCertificate.Validate;

            //for this version, let us avoid automatically changing the DnsIdentity value:
            txDnsIdentity.Tag = new object(); //this should force it to skip the automatic change

            //set our internal state and reset modified flag
            //this.settings = settings;
            //this.settingsModified = false;
        }

        //public void SetConnectionSettings(ConnectionSettings settings)
        //{
        //    if (settings == null)
        //        throw new ArgumentNullException("settings");

        //    SetConnectionSettings(settings, false);
        //}

        #region Event Handling

        private void btnConnect_Click(object sender, EventArgs e)
        {
            DoConnect();
        }

        private void DoConnect()
        {
            // Change view
            State = LoginViewState.Connecting;

            try
            {
                ConnectionSettings settings = GetConnectionSettings();
                // Do connection stuff
                // Raise Event that connection was successful
                OnConnecting(settings);
            }
            catch (ArgumentException ex)
            {
                //validation error:

                this.ErrorText = ex.Message;
                logger.Debug("Errors Validating Connection settings: " +this.ErrorText);

                // Errors, so go back to form
                State = LoginViewState.LoginForm;
            }
        }

        private void SetInitialSettings()
        {
            ConnectionSettings settings = null;
            try
            {
                settings = ConfigurationHelper.GetConnectionSettings();
            }
            catch (Exception cx)
            {
                logger.Warn("Could not retrieve settings from configuration: ", cx);
                settings = new ConnectionSettings();
            }

            //set some defaults
            if (string.IsNullOrEmpty(settings.Host))
            {
                settings.Host = Dns.GetHostName();
                settings.ServiceCertificate.DnsIdentity = "Utilify.Platform.Manager"; //settings.Host; // set it to the same value initially.
            }
            if (string.IsNullOrEmpty(settings.UsernameCredential.Username))
                settings.UsernameCredential.Username = "admin"; //default

            //fill the UI with the settings 
            //todoLater: attempted to do this without the password - but didnt get the flow right
            SetConnectionSettings(settings);//, true);
        }

        /// <summary>
        /// Gets the connection settings.
        /// </summary>
        /// <returns></returns>
        public ConnectionSettings GetConnectionSettings()
        {
            //if (settings == null || settingsModified)
            //{
                logger.Debug("Called GetConnectionSettings");
                //Validation of the values is done in the ConnectionSettings object itself!

                // Create ConnectionSettings to pass on
                ConnectionSettings settings = new ConnectionSettings();
                int port = 0;
                Int32.TryParse(tbPort.Text, out port);

                settings.Host = tbHostname.Text;
                settings.Port = port;
                //todoFix
                settings.IsInteropClient = ((string)cbProtocol.SelectedItem != CommunicationProtocol.Tcp.ToString());
                
                //settings.Protocol =
                //    (CommunicationProtocol)Enum.Parse(typeof(CommunicationProtocol), (string)cbProtocol.SelectedItem);

                settings.CredentialType =
                    (CredentialType)Enum.Parse(typeof(CredentialType), (string)cbAuthMode.SelectedItem);

                if (settings.CredentialType == CredentialType.Username)
                {
                    settings.UsernameCredential.Username = txUsername.Text;
                    settings.UsernameCredential.Password = txPassword.Text;
                }

                settings.ServiceCertificate.DnsIdentity = txDnsIdentity.Text;
                settings.ServiceCertificate.Validate = chkValidateDnsIdentity.Checked;

            //    settingsModified = false;//reset
            //}

            return settings;
        }

        /// <summary>
        /// Saves the settings to disk.
        /// </summary>
        public void SaveSettings()
        {
            try
            {
                ConnectionSettings settings = this.GetConnectionSettings();
                ConfigurationHelper.SaveConnectionSettings(settings);
            }
            catch (Exception cx)
            {
#if DEBUG
                logger.Info("Error saving connection settings: " + cx.ToString());
#else
                logger.Warn("Error saving connection settings: " + cx.Message);
#endif
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // Change view
            State = LoginViewState.LoginForm;

            // Kill connection

            // Raise Cancelling Event
            OnCancelling();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the state of the UI.
        /// </summary>
        /// <value>The state.</value>
        public LoginViewState State
        {
            get { return state;}
            set 
            {
                switch (value)
                {
                    case LoginViewState.Connecting:
                        advancedSettingsPanel.Visible = false;
                        formPanel.Visible = false;
                        connectingPanel.Visible = true;

                        btnConnect.Enabled = false;
                        btnCancel.Enabled = true;

                        btnBack.Visible = false; 
                        btnConnect.Visible = true;
                        btnCancel.Visible = true;
                        break;
                    case LoginViewState.LoginForm:
                        advancedSettingsPanel.Visible = false;
                        connectingPanel.Visible = false;
                        formPanel.Visible = true;

                        btnConnect.Enabled = true;
                        btnCancel.Enabled = false;

                        btnBack.Visible = false;
                        btnConnect.Visible = true;
                        btnCancel.Visible = true;
                        break;
                    case LoginViewState.AdvancedSettings:
                        connectingPanel.Visible = false;
                        formPanel.Visible = false;
                        advancedSettingsPanel.Visible = true;

                        btnConnect.Enabled = true;

                        btnBack.Visible = true;
                        btnConnect.Visible = false;
                        btnCancel.Visible = false;
                        break;
                }
                state = value;
                this.Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the message text.
        /// </summary>
        /// <value>The message text.</value>
        public string MessageText
        {
            get { return lbMessage.Text; }
            set
            {
                if (this.InvokeRequired)
                {
                    Action<string> setMessage = SetMessageText;
                    setMessage.Invoke(value);
                }
                else
                {
                    SetMessageText(value);
                }
            }
        }

        private void SetMessageText(string value)
        {
            lbMessage.Text = value ?? string.Empty;
            lbMessage.Width = 326; //don't need space for the details link
            lbMessage.ForeColor = Color.Black;
            ErrorDetail = ""; //reset the error detail
        }

        //todo: ensure we call the UI thread properly.

        /// <summary>
        /// Gets or sets the error text.
        /// </summary>
        /// <value>The error text.</value>
        public string ErrorText
        {
            get { return lbMessage.Text; }
            set
            {
                if (this.InvokeRequired)
                {
                    Action<string> setError = SetErrorText;
                    this.Invoke(setError, value);
                }
                else
                {
                    SetErrorText(value);
                }
            }
        }

        private void SetErrorText(string value)
        {
            lbMessage.Text = value ?? string.Empty;
            lbMessage.Width = 279; //to allow space for the details link
            lbMessage.ForeColor = Color.Red;
            ErrorDetail = "";
        }

        /// <summary>
        /// Gets or sets the error detail.
        /// </summary>
        /// <value>The error detail.</value>
        public string ErrorDetail
        {
            get { return errorDetail; }
            set
            {
                errorDetail = value ?? string.Empty;     
                if (this.InvokeRequired)
                {
                    Action<string> setError = SetErrorDetail;
                    this.Invoke(setError, errorDetail);
                }
                else
                {
                    SetErrorDetail(errorDetail);
                }
            }
        }

        private void SetErrorDetail(string value)
        {
            if (!string.IsNullOrEmpty(errorDetail))
            {
                lnkErrorDetail.Text = "Details...";
            }
            else
            {
                lnkErrorDetail.Text = string.Empty;
            }
            //the following doesn't work, so we just blank out the string for the link:
            //lnkErrorDetail.Visible = !string.IsNullOrEmpty(errorDetail);       
        }

        private void lnkErrorDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show(this, errorDetail, "Error details", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        #endregion

        #region Event Raising

        //protected virtual void OnConnected()
        //{
        //    EventHelper.RaiseEvent<EventArgs>(Connected, this, new EventArgs());
        //}

        /// <summary>
        /// Called when the connecting event needs to be raised.
        /// </summary>
        /// <param name="settings">The settings.</param>
        protected virtual void OnConnecting(ConnectionSettings settings)
        {
            var args = new ConnectionEventArgs<ConnectionSettings>(settings);
            EventHelper.RaiseEvent(Connecting, this, args);
        }

        /// <summary>
        /// Called when the cancelling needs to be raised.
        /// </summary>
        protected virtual void OnCancelling()
        {
            EventHelper.RaiseEvent<EventArgs>(Cancelling, this, new EventArgs());
        }

        #endregion

        #region Keypress Event Handling to Submit Form on 'ENTER'

        // This doesn't work. Only captures the event if nothing on the form has focus.
        private void LoginView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) 
                DoConnect();
        }

        private void tbHostname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                DoConnect();
        }

        private void tbPort_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                DoConnect();
        }

        private void tbUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                DoConnect();
        }

        private void tbPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                DoConnect();
        }

        #endregion

        private void cbAuthMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //show/hide stuff based on what the selectd value is:
            CredentialType type = CredentialType.None;
            if (Enum.IsDefined(typeof(CredentialType), cbAuthMode.SelectedItem))
            {
                type = (CredentialType)Enum.Parse(typeof(CredentialType), 
                    (string)cbAuthMode.SelectedItem);
            }

            noCredsPanel.Visible = (type == CredentialType.None);
            windowsAuthPanel.Visible = (type == CredentialType.Windows);
            usernameAuthPanel.Visible = (type == CredentialType.Username);

            formPanel.Refresh();
        }

        private void lbAdvanced_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.State = LoginViewState.AdvancedSettings;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.State = LoginViewState.LoginForm;
        }

        private void tbHostname_TextChanged(object sender, EventArgs e)
        {
            //we use the Tag to identify that the user has manually modified the txDnsIdentity value
            if (txDnsIdentity.Tag == null)
            {
                txDnsIdentity.Text = tbHostname.Text;
            }

            //MessageBox.Show("Test: " + tbHostname.Text + ", null? " + (txDnsIdentity.Tag));
        }

        private void txDnsIdentity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txDnsIdentity.Tag == null)
            {
                //mark it as changed
                txDnsIdentity.Tag = new object(); //any non-null value is good enough!
            }
        }
    }
}
