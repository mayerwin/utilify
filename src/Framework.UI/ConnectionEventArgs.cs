﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilify.Platform;

namespace Utilify.Framework.UI
{
    /// <summary>
    /// Contains the data for the connection event.
    /// </summary>
    public class ConnectionEventArgs<T> : EventArgs
    {
        private T connectionData;

        internal ConnectionEventArgs()
            : base()
        { }

        public ConnectionEventArgs(T connectionSettings)
            : base()
        {
            this.connectionData = connectionSettings;
        }

        /// <summary>
        /// Gets or sets the connection settings/result data.
        /// </summary>
        /// <value>The connection settings/result data.</value>
        public T ConnectionData
        {
            get { return connectionData; }
            set { connectionData = value; }
        }
    }
}
