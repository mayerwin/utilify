﻿namespace Utilify.Framework.UI
{
    partial class LoginView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectingPanel = new System.Windows.Forms.Panel();
            this.lblConnecting = new System.Windows.Forms.Label();
            this.pbConnecting = new System.Windows.Forms.PictureBox();
            this.formPanel = new System.Windows.Forms.Panel();
            this.lnkErrorDetail = new System.Windows.Forms.LinkLabel();
            this.lbAdvanced = new System.Windows.Forms.LinkLabel();
            this.usernameAuthPanel = new System.Windows.Forms.Panel();
            this.lbUsername = new System.Windows.Forms.Label();
            this.txUsername = new System.Windows.Forms.TextBox();
            this.txPassword = new System.Windows.Forms.TextBox();
            this.lbPassword = new System.Windows.Forms.Label();
            this.windowsAuthPanel = new System.Windows.Forms.Panel();
            this.lbWindowsUsername = new System.Windows.Forms.Label();
            this.lbWindowsAuth = new System.Windows.Forms.Label();
            this.lbProto = new System.Windows.Forms.Label();
            this.cbProtocol = new System.Windows.Forms.ComboBox();
            this.lbCredType = new System.Windows.Forms.Label();
            this.cbAuthMode = new System.Windows.Forms.ComboBox();
            this.lbMessage = new System.Windows.Forms.Label();
            this.lbHost = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.lbPort = new System.Windows.Forms.Label();
            this.tbHostname = new System.Windows.Forms.TextBox();
            this.noCredsPanel = new System.Windows.Forms.Panel();
            this.lbNoCreds = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.advancedSettingsPanel = new System.Windows.Forms.Panel();
            this.lbDnsIdentity = new System.Windows.Forms.Label();
            this.chkValidateDnsIdentity = new System.Windows.Forms.CheckBox();
            this.txDnsIdentity = new System.Windows.Forms.TextBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.connectingPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbConnecting)).BeginInit();
            this.formPanel.SuspendLayout();
            this.usernameAuthPanel.SuspendLayout();
            this.windowsAuthPanel.SuspendLayout();
            this.noCredsPanel.SuspendLayout();
            this.advancedSettingsPanel.SuspendLayout();
            this.MainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // connectingPanel
            // 
            this.connectingPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.connectingPanel.BackColor = System.Drawing.Color.White;
            this.connectingPanel.Controls.Add(this.lblConnecting);
            this.connectingPanel.Controls.Add(this.pbConnecting);
            this.connectingPanel.Location = new System.Drawing.Point(1, 8);
            this.connectingPanel.Name = "connectingPanel";
            this.connectingPanel.Size = new System.Drawing.Size(350, 186);
            this.connectingPanel.TabIndex = 8;
            this.connectingPanel.Visible = false;
            // 
            // lblConnecting
            // 
            this.lblConnecting.AutoSize = true;
            this.lblConnecting.Location = new System.Drawing.Point(142, 123);
            this.lblConnecting.Name = "lblConnecting";
            this.lblConnecting.Size = new System.Drawing.Size(70, 13);
            this.lblConnecting.TabIndex = 1;
            this.lblConnecting.Text = "Connecting...";
            // 
            // pbConnecting
            // 
            this.pbConnecting.Image = global::Utilify.Framework.Resources.connecting;
            this.pbConnecting.Location = new System.Drawing.Point(140, 50);
            this.pbConnecting.Name = "pbConnecting";
            this.pbConnecting.Size = new System.Drawing.Size(75, 68);
            this.pbConnecting.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbConnecting.TabIndex = 0;
            this.pbConnecting.TabStop = false;
            // 
            // formPanel
            // 
            this.formPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.formPanel.BackColor = System.Drawing.Color.Transparent;
            this.formPanel.Controls.Add(this.lnkErrorDetail);
            this.formPanel.Controls.Add(this.lbAdvanced);
            this.formPanel.Controls.Add(this.lbProto);
            this.formPanel.Controls.Add(this.lbCredType);
            this.formPanel.Controls.Add(this.cbAuthMode);
            this.formPanel.Controls.Add(this.lbMessage);
            this.formPanel.Controls.Add(this.lbHost);
            this.formPanel.Controls.Add(this.tbPort);
            this.formPanel.Controls.Add(this.lbPort);
            this.formPanel.Controls.Add(this.tbHostname);
            this.formPanel.Controls.Add(this.cbProtocol);
            this.formPanel.Controls.Add(this.usernameAuthPanel);
            this.formPanel.Controls.Add(this.noCredsPanel);
            this.formPanel.Controls.Add(this.windowsAuthPanel);
            this.formPanel.Location = new System.Drawing.Point(1, 0);
            this.formPanel.Name = "formPanel";
            this.formPanel.Size = new System.Drawing.Size(350, 199);
            this.formPanel.TabIndex = 9;
            // 
            // lnkErrorDetail
            // 
            this.lnkErrorDetail.ActiveLinkColor = System.Drawing.Color.DarkOrange;
            this.lnkErrorDetail.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lnkErrorDetail.Location = new System.Drawing.Point(296, 6);
            this.lnkErrorDetail.Name = "lnkErrorDetail";
            this.lnkErrorDetail.Size = new System.Drawing.Size(48, 35);
            this.lnkErrorDetail.TabIndex = 10;
            this.lnkErrorDetail.TabStop = true;
            this.lnkErrorDetail.Text = "Details...";
            this.lnkErrorDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lnkErrorDetail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkErrorDetail_LinkClicked);
            // 
            // lbAdvanced
            // 
            this.lbAdvanced.ActiveLinkColor = System.Drawing.Color.DarkOrange;
            this.lbAdvanced.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbAdvanced.AutoSize = true;
            this.lbAdvanced.Location = new System.Drawing.Point(279, 177);
            this.lbAdvanced.Name = "lbAdvanced";
            this.lbAdvanced.Size = new System.Drawing.Size(65, 13);
            this.lbAdvanced.TabIndex = 6;
            this.lbAdvanced.TabStop = true;
            this.lbAdvanced.Text = "Advanced...";
            this.lbAdvanced.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbAdvanced_LinkClicked);
            // 
            // usernameAuthPanel
            // 
            this.usernameAuthPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.usernameAuthPanel.BackColor = System.Drawing.Color.Transparent;
            this.usernameAuthPanel.Controls.Add(this.lbUsername);
            this.usernameAuthPanel.Controls.Add(this.txUsername);
            this.usernameAuthPanel.Controls.Add(this.txPassword);
            this.usernameAuthPanel.Controls.Add(this.lbPassword);
            this.usernameAuthPanel.Location = new System.Drawing.Point(8, 128);
            this.usernameAuthPanel.Margin = new System.Windows.Forms.Padding(0);
            this.usernameAuthPanel.Name = "usernameAuthPanel";
            this.usernameAuthPanel.Size = new System.Drawing.Size(336, 46);
            this.usernameAuthPanel.TabIndex = 10;
            // 
            // lbUsername
            // 
            this.lbUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbUsername.AutoSize = true;
            this.lbUsername.BackColor = System.Drawing.Color.Transparent;
            this.lbUsername.Location = new System.Drawing.Point(7, 1);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(55, 13);
            this.lbUsername.TabIndex = 4;
            this.lbUsername.Text = "&Username";
            // 
            // txUsername
            // 
            this.txUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txUsername.BackColor = System.Drawing.SystemColors.Window;
            this.txUsername.Location = new System.Drawing.Point(5, 18);
            this.txUsername.Name = "txUsername";
            this.txUsername.Size = new System.Drawing.Size(160, 20);
            this.txUsername.TabIndex = 0;
            this.txUsername.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbUsername_KeyDown);
            // 
            // txPassword
            // 
            this.txPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txPassword.Location = new System.Drawing.Point(171, 18);
            this.txPassword.Name = "txPassword";
            this.txPassword.Size = new System.Drawing.Size(160, 20);
            this.txPassword.TabIndex = 1;
            this.txPassword.UseSystemPasswordChar = true;
            this.txPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbPassword_KeyDown);
            // 
            // lbPassword
            // 
            this.lbPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPassword.AutoSize = true;
            this.lbPassword.BackColor = System.Drawing.Color.Transparent;
            this.lbPassword.Location = new System.Drawing.Point(172, 1);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(53, 13);
            this.lbPassword.TabIndex = 5;
            this.lbPassword.Text = "&Password";
            // 
            // windowsAuthPanel
            // 
            this.windowsAuthPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.windowsAuthPanel.BackColor = System.Drawing.Color.Transparent;
            this.windowsAuthPanel.Controls.Add(this.lbWindowsUsername);
            this.windowsAuthPanel.Controls.Add(this.lbWindowsAuth);
            this.windowsAuthPanel.Location = new System.Drawing.Point(8, 128);
            this.windowsAuthPanel.Margin = new System.Windows.Forms.Padding(0);
            this.windowsAuthPanel.Name = "windowsAuthPanel";
            this.windowsAuthPanel.Size = new System.Drawing.Size(336, 46);
            this.windowsAuthPanel.TabIndex = 11;
            // 
            // lbWindowsUsername
            // 
            this.lbWindowsUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbWindowsUsername.AutoEllipsis = true;
            this.lbWindowsUsername.AutoSize = true;
            this.lbWindowsUsername.BackColor = System.Drawing.Color.Transparent;
            this.lbWindowsUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWindowsUsername.Location = new System.Drawing.Point(26, 24);
            this.lbWindowsUsername.Name = "lbWindowsUsername";
            this.lbWindowsUsername.Size = new System.Drawing.Size(43, 13);
            this.lbWindowsUsername.TabIndex = 6;
            this.lbWindowsUsername.Text = "xxxxxx";
            // 
            // lbWindowsAuth
            // 
            this.lbWindowsAuth.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbWindowsAuth.AutoSize = true;
            this.lbWindowsAuth.BackColor = System.Drawing.Color.Transparent;
            this.lbWindowsAuth.Location = new System.Drawing.Point(9, 6);
            this.lbWindowsAuth.Name = "lbWindowsAuth";
            this.lbWindowsAuth.Size = new System.Drawing.Size(121, 13);
            this.lbWindowsAuth.TabIndex = 5;
            this.lbWindowsAuth.Text = "Using Windows Identity:";
            // 
            // lbProto
            // 
            this.lbProto.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbProto.AutoSize = true;
            this.lbProto.BackColor = System.Drawing.Color.Transparent;
            this.lbProto.Location = new System.Drawing.Point(14, 85);
            this.lbProto.Name = "lbProto";
            this.lbProto.Size = new System.Drawing.Size(46, 13);
            this.lbProto.TabIndex = 10;
            this.lbProto.Text = "P&rotocol";
            // 
            // cbProtocol
            // 
            this.cbProtocol.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbProtocol.BackColor = System.Drawing.SystemColors.Window;
            this.cbProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProtocol.FormattingEnabled = true;
            this.cbProtocol.Location = new System.Drawing.Point(13, 101);
            this.cbProtocol.Name = "cbProtocol";
            this.cbProtocol.Size = new System.Drawing.Size(82, 21);
            this.cbProtocol.TabIndex = 1;
            // 
            // lbCredType
            // 
            this.lbCredType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbCredType.AutoSize = true;
            this.lbCredType.BackColor = System.Drawing.Color.Transparent;
            this.lbCredType.Location = new System.Drawing.Point(15, 42);
            this.lbCredType.Name = "lbCredType";
            this.lbCredType.Size = new System.Drawing.Size(104, 13);
            this.lbCredType.TabIndex = 12;
            this.lbCredType.Text = "&Authentication mode";
            // 
            // cbAuthMode
            // 
            this.cbAuthMode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbAuthMode.BackColor = System.Drawing.SystemColors.Window;
            this.cbAuthMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAuthMode.FormattingEnabled = true;
            this.cbAuthMode.Location = new System.Drawing.Point(13, 58);
            this.cbAuthMode.Name = "cbAuthMode";
            this.cbAuthMode.Size = new System.Drawing.Size(326, 21);
            this.cbAuthMode.TabIndex = 0;
            this.cbAuthMode.SelectedIndexChanged += new System.EventHandler(this.cbAuthMode_SelectedIndexChanged);
            // 
            // lbMessage
            // 
            this.lbMessage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbMessage.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbMessage.ForeColor = System.Drawing.Color.Red;
            this.lbMessage.Location = new System.Drawing.Point(13, 6);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(279, 35);
            this.lbMessage.TabIndex = 6;
            this.lbMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbHost
            // 
            this.lbHost.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbHost.AutoSize = true;
            this.lbHost.BackColor = System.Drawing.Color.Transparent;
            this.lbHost.Location = new System.Drawing.Point(103, 85);
            this.lbHost.Name = "lbHost";
            this.lbHost.Size = new System.Drawing.Size(117, 13);
            this.lbHost.TabIndex = 2;
            this.lbHost.Text = "&Hostname / IP Address";
            // 
            // tbPort
            // 
            this.tbPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbPort.BackColor = System.Drawing.SystemColors.Window;
            this.tbPort.Location = new System.Drawing.Point(292, 101);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(47, 20);
            this.tbPort.TabIndex = 3;
            this.tbPort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbPort_KeyDown);
            // 
            // lbPort
            // 
            this.lbPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbPort.AutoSize = true;
            this.lbPort.BackColor = System.Drawing.Color.Transparent;
            this.lbPort.Location = new System.Drawing.Point(295, 85);
            this.lbPort.Name = "lbPort";
            this.lbPort.Size = new System.Drawing.Size(26, 13);
            this.lbPort.TabIndex = 3;
            this.lbPort.Text = "P&ort";
            // 
            // tbHostname
            // 
            this.tbHostname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbHostname.BackColor = System.Drawing.SystemColors.Window;
            this.tbHostname.Location = new System.Drawing.Point(103, 101);
            this.tbHostname.Name = "tbHostname";
            this.tbHostname.Size = new System.Drawing.Size(182, 20);
            this.tbHostname.TabIndex = 2;
            this.tbHostname.TextChanged += new System.EventHandler(this.tbHostname_TextChanged);
            this.tbHostname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbHostname_KeyDown);
            // 
            // noCredsPanel
            // 
            this.noCredsPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.noCredsPanel.BackColor = System.Drawing.Color.Transparent;
            this.noCredsPanel.Controls.Add(this.lbNoCreds);
            this.noCredsPanel.Location = new System.Drawing.Point(8, 127);
            this.noCredsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.noCredsPanel.Name = "noCredsPanel";
            this.noCredsPanel.Size = new System.Drawing.Size(336, 46);
            this.noCredsPanel.TabIndex = 12;
            // 
            // lbNoCreds
            // 
            this.lbNoCreds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbNoCreds.AutoEllipsis = true;
            this.lbNoCreds.AutoSize = true;
            this.lbNoCreds.BackColor = System.Drawing.Color.Transparent;
            this.lbNoCreds.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNoCreds.Location = new System.Drawing.Point(47, 17);
            this.lbNoCreds.Name = "lbNoCreds";
            this.lbNoCreds.Size = new System.Drawing.Size(237, 13);
            this.lbNoCreds.TabIndex = 6;
            this.lbNoCreds.Text = "No credentials will be sent to the server.";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(180, 201);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnConnect.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnConnect.Location = new System.Drawing.Point(99, 201);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 7;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // advancedSettingsPanel
            // 
            this.advancedSettingsPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.advancedSettingsPanel.BackColor = System.Drawing.Color.Transparent;
            this.advancedSettingsPanel.Controls.Add(this.lbDnsIdentity);
            this.advancedSettingsPanel.Controls.Add(this.chkValidateDnsIdentity);
            this.advancedSettingsPanel.Controls.Add(this.txDnsIdentity);
            this.advancedSettingsPanel.Location = new System.Drawing.Point(1, 8);
            this.advancedSettingsPanel.Name = "advancedSettingsPanel";
            this.advancedSettingsPanel.Size = new System.Drawing.Size(350, 186);
            this.advancedSettingsPanel.TabIndex = 16;
            this.advancedSettingsPanel.Visible = false;
            // 
            // lbDnsIdentity
            // 
            this.lbDnsIdentity.AutoSize = true;
            this.lbDnsIdentity.Location = new System.Drawing.Point(15, 33);
            this.lbDnsIdentity.Name = "lbDnsIdentity";
            this.lbDnsIdentity.Size = new System.Drawing.Size(233, 13);
            this.lbDnsIdentity.TabIndex = 3;
            this.lbDnsIdentity.Text = "Service identity (if different from the host name) :";
            // 
            // chkValidateDnsIdentity
            // 
            this.chkValidateDnsIdentity.AutoSize = true;
            this.chkValidateDnsIdentity.Location = new System.Drawing.Point(18, 76);
            this.chkValidateDnsIdentity.Name = "chkValidateDnsIdentity";
            this.chkValidateDnsIdentity.Size = new System.Drawing.Size(137, 17);
            this.chkValidateDnsIdentity.TabIndex = 2;
            this.chkValidateDnsIdentity.Text = "Validate service identity";
            this.chkValidateDnsIdentity.UseVisualStyleBackColor = true;
            // 
            // txDnsIdentity
            // 
            this.txDnsIdentity.Location = new System.Drawing.Point(15, 50);
            this.txDnsIdentity.Name = "txDnsIdentity";
            this.txDnsIdentity.Size = new System.Drawing.Size(326, 20);
            this.txDnsIdentity.TabIndex = 0;
            this.txDnsIdentity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txDnsIdentity_KeyPress);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBack.Location = new System.Drawing.Point(265, 201);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 9;
            this.btnBack.Text = "<< Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // MainPanel
            // 
            this.MainPanel.BackColor = System.Drawing.Color.Transparent;
            this.MainPanel.Controls.Add(this.btnConnect);
            this.MainPanel.Controls.Add(this.btnCancel);
            this.MainPanel.Controls.Add(this.btnBack);
            this.MainPanel.Controls.Add(this.formPanel);
            this.MainPanel.Controls.Add(this.connectingPanel);
            this.MainPanel.Controls.Add(this.advancedSettingsPanel);
            this.MainPanel.Location = new System.Drawing.Point(2, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(354, 234);
            this.MainPanel.TabIndex = 17;
            // 
            // LoginView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.MainPanel);
            this.Name = "LoginView";
            this.Size = new System.Drawing.Size(356, 236);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginView_KeyDown);
            this.connectingPanel.ResumeLayout(false);
            this.connectingPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbConnecting)).EndInit();
            this.formPanel.ResumeLayout(false);
            this.formPanel.PerformLayout();
            this.usernameAuthPanel.ResumeLayout(false);
            this.usernameAuthPanel.PerformLayout();
            this.windowsAuthPanel.ResumeLayout(false);
            this.windowsAuthPanel.PerformLayout();
            this.noCredsPanel.ResumeLayout(false);
            this.noCredsPanel.PerformLayout();
            this.advancedSettingsPanel.ResumeLayout(false);
            this.advancedSettingsPanel.PerformLayout();
            this.MainPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel connectingPanel;
        private System.Windows.Forms.PictureBox pbConnecting;
        private System.Windows.Forms.Label lblConnecting;
        private System.Windows.Forms.Label lbHost;
        private System.Windows.Forms.TextBox txUsername;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbPort;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.TextBox txPassword;
        private System.Windows.Forms.TextBox tbHostname;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.Panel usernameAuthPanel;
        private System.Windows.Forms.Label lbCredType;
        private System.Windows.Forms.ComboBox cbAuthMode;
        private System.Windows.Forms.Panel windowsAuthPanel;
        private System.Windows.Forms.Label lbWindowsUsername;
        private System.Windows.Forms.Label lbWindowsAuth;
        private System.Windows.Forms.ComboBox cbProtocol;
        private System.Windows.Forms.Label lbProto;
        private System.Windows.Forms.Panel noCredsPanel;
        private System.Windows.Forms.Label lbNoCreds;
        private System.Windows.Forms.LinkLabel lnkErrorDetail;
        private System.Windows.Forms.LinkLabel lbAdvanced;
        private System.Windows.Forms.Panel advancedSettingsPanel;
        private System.Windows.Forms.CheckBox chkValidateDnsIdentity;
        private System.Windows.Forms.TextBox txDnsIdentity;
        private System.Windows.Forms.Label lbDnsIdentity;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel formPanel;

    }
}
