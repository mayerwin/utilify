using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
#if !MONO
    [DataContract]
#endif
    public enum Architecture
    {
#if !MONO
        [EnumMember]
#endif
        None = 0,
#if !MONO
        [EnumMember]
#endif
        Amd64 = 1,
#if !MONO
        [EnumMember]
#endif
        IA64 = 2,
#if !MONO
        [EnumMember]
#endif
        MSIL = 3,
#if !MONO
        [EnumMember]
#endif
        X86 = 4
    }

#if !MONO
    [DataContract]
#endif
    public enum FileSystemType
    {
#if !MONO
        [EnumMember]
#endif
        Unknown = 0,
#if !MONO
        [EnumMember]
#endif
        Fat16 = 1,
#if !MONO
        [EnumMember]
#endif
        Fat32 = 2,
#if !MONO
        [EnumMember]
#endif
        Ntfs = 3,
#if !MONO
        [EnumMember]
#endif
        Ext2 = 4,
#if !MONO
        [EnumMember]
#endif
        Ext3 = 5
    }
}