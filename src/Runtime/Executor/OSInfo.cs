using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class OSInfo
    {
        //todoLater: we could probably simplify this by defining out own enum for OS, and have a string for version.
        private string name;
        private string version;

        private OSInfo() { }

        /// <summary>
        /// Creates and instance of OS
        /// </summary>
        /// <param name="name"></param>
        /// <param name="version"></param>
        public OSInfo(string name, string version) : this()
        {
            this.Name = name;
            this.Version = version;
        }

        /// <summary>
        /// Gets the Operating System Name.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Name
        {
            get { return name; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("name");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("OS name cannot be empty", "name");
                name = value;
            }
        }

        /// <summary>
        /// Gets the Operating System Version.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Version
        {
            get { return version; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("version");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("OS version cannot be empty", "version");
                version = value;
            }
        }

        /// <summary>
        /// Gets the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1}", name, version);
        }
    }
}
