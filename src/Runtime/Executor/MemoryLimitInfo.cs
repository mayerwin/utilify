using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class MemoryLimitInfo
    {
        private long sizeUsageLimit; //(in bytes)

        private MemoryLimitInfo() { }

        /// <summary>
        /// Creates an instance of MemoryLimitInfo.
        /// </summary>
        /// <param name="sizeUsageLimit"></param>
        public MemoryLimitInfo(long sizeUsageLimit) : this()
        {
            this.SizeUsageLimit = sizeUsageLimit;
        }

        /// <summary>
        /// Gets the current size limit of the memory usable in bytes.
        /// </summary>
        /// <remarks>
        /// Can we use negative values on limits to represent 'no limit'?
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long SizeUsageLimit
        {
            get { return sizeUsageLimit; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Size limit cannot be less than zero", "sizeUsageLimit");
                sizeUsageLimit = value;
            }
        }

        /// <summary>
        /// Get the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} bytes Usable/Limit", sizeUsageLimit);
        }
    }
}
