using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class DiskLimitInfo
    {
        private string root; // OS's name/id for the root used for linking by performance and limit infos
        private long sizeUsageLimit; //(in bytes)

        private DiskLimitInfo(){}

        /// <summary>
        /// Creates an instance of DiskLimitInfo
        /// </summary>
        /// <param name="root"></param>
        /// <param name="sizeUsageLimit"></param>
        public DiskLimitInfo(string root, long sizeUsageLimit) : this()
        {
            this.Root = root;
            this.SizeUsageLimit = sizeUsageLimit;
        }

        /// <summary>
        /// Gets the Root for this disk.
        /// </summary>
        /// <remarks>
        /// This field uniquely identifies each disk on a machine.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Root
        {
            get { return root; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("root");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Disk root cannot be empty", "root");
                root = value;
            }
        }

        /// <summary>
        /// Gets the size limit (in bytes) of the space allowed to be used on this disk.
        /// </summary>
        public long SizeUsageLimit
        {
            get { return sizeUsageLimit; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Limit size cannot be less than zero", "sizeUsageLimit");
                sizeUsageLimit = value;
            }
        }

        /// <summary>
        /// Gets the string representation for this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} bytes Usable/Limit", sizeUsageLimit);
        }
    }
}
