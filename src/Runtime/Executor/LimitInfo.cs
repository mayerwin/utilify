using System;
using System.Text;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class LimitInfo
    {
        private string systemId;
        private ProcessorLimitInfo[] processors;
        private DiskLimitInfo[] disks;
        private MemoryLimitInfo memory;

        private LimitInfo() { }

        /// <summary>
        /// Creates an instance of the LimitInfo class.
        /// </summary>
        /// <param name="systemId"></param>
        /// <param name="processors"></param>
        /// <param name="disks"></param>
        /// <param name="memory"></param>
        public LimitInfo(string systemId, ProcessorLimitInfo[] processors,
            DiskLimitInfo[] disks, MemoryLimitInfo memory) : this()
        {
            this.SystemId = systemId;
            this.Processors = processors;
            this.Disks = disks;
            this.Memory = memory;
        }

        /// <summary>
        /// Gets the Id of the executor this performance info belongs to
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string SystemId
        {
            get { return systemId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("systemId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("SystemId cannot be empty", "systemId");
                systemId = value;
            }
        }


        /// <summary>
        /// Gets the processor limit information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public ProcessorLimitInfo[] Processors
        {
            get { return processors; }
            private set
            {
                if (value == null)
                    processors = new ProcessorLimitInfo[0];
                else
                    processors = value;
            }
        }

        /// <summary>
        /// Gets the hard disk limit information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DiskLimitInfo[] Disks
        {
            get { return disks; }
            private set
            {
                if (value == null)
                    disks = new DiskLimitInfo[0];
                else
                    disks = value;
            }
        }

        /// <summary>
        /// Gets the memory limit information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public MemoryLimitInfo Memory
        {
            get { return memory; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("memory");
                memory = value;
            }
        }

        /// <summary>
        /// Gets the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("LimitInfo : {0} ", systemId).AppendLine();

            if (processors != null)
            {
                sb.AppendFormat("- Processors : {0}", processors.Length).AppendLine();
                for (int i = 0; i < processors.Length; i++)
                    sb.AppendFormat("- {0} : {1}", i, processors[i]).AppendLine();
            }
            else
            {
                sb.AppendLine("- No processors.");
            }

            if (disks != null)
            {
                sb.AppendFormat("- Disks : {0}", disks.Length).AppendLine();
                for (int i = 0; i < disks.Length; i++)
                    sb.AppendFormat("- {0} : {1}", i, disks[i]).AppendLine();
            }
            else
            {
                sb.AppendLine("- No disks.");
            }

            sb.AppendFormat("- Memory : {0}", memory).AppendLine();

            return sb.ToString();
        }
    }
}
