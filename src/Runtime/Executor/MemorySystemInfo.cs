using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class MemorySystemInfo
    {
        private long sizeTotal; //(in bytes)

        private MemorySystemInfo() { }

        /// <summary>
        /// Creates and instance of MemorySystemInfo
        /// </summary>
        /// <param name="sizeTotal"></param>
        public MemorySystemInfo(long sizeTotal) : this()
        {
            this.SizeTotal = sizeTotal;
        }

        /// <summary>
        /// Gets the total size of the memory module in bytes.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long SizeTotal
        {
            get { return sizeTotal; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Size cannot be less than zero", "sizeTotal");
                sizeTotal = value;
            }
        }

        /// <summary>
        /// Gets the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} bytes Total", sizeTotal);
        }
    }
}
