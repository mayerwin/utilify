using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    /// <summary>
    /// Represents a job that is submitted for remote execution
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class JobInfo
    {
        private string jobId; //used when this object is sent from the manager to the executor

        private string applicationId;
        private JobType jobType;
        private ClientPlatform clientPlatform;

        /// <summary>
        /// Creates an instance of the JobInfo class with the specified parameters
        /// </summary>
        /// <param name="jobId">id of the job</param>
        /// <param name="applicationId">id of the associated application</param>
        /// <param name="jobType">type of the job which is one of the <see cref="Utilify.Framework.JobType">JobType</see> values</param>
        /// <param name="clientPlatform">the platform of the job which is one of the <see cref="Utilify.Framework.ClientPlatform">ClientPlatform</see> values</param>
        /// <exception cref="System.ArgumentException">applicationId is empty</exception>
        public JobInfo(string jobId, string applicationId, JobType jobType, ClientPlatform clientPlatform)
        {
            this.JobId = jobId;
            this.ApplicationId = applicationId;
            this.JobType = jobType;
            this.ClientPlatform = clientPlatform;
        }

        /// <summary>
        /// Gets the job id
        /// </summary>
#if !MONO
        [DataMember(IsRequired = false)]
#endif
        public string JobId
        {
            get { return jobId; }
            private set { jobId = value ?? string.Empty; }
        }

        /// <summary>
        /// Gets the id of the application associated with this job
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationId is a null reference</exception>
        /// <exception cref="System.ArgumentException">applicationId is empty</exception>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ApplicationId
        {
            get { return applicationId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("applicationId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application id cannot be empty", "applicationId");

                applicationId = value;
            }
        }

        /// <summary>
        /// Gets the type of the job
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public JobType JobType
        {
            get { return jobType; }
            private set 
            {
                jobType = value; 
            }
        }

        /// <summary>
        /// Identifies the platform the job was submitted from
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public ClientPlatform ClientPlatform
        {
            get { return clientPlatform; }
            private set
            {
                clientPlatform = value;
            }
        }
    }
}