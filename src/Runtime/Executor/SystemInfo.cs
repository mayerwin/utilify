using System;
using System.Text;

#if !MONO
using System.Runtime.Serialization;
#endif

//todo: all *Info classes, contracts and interfaces to be placed into Platform.Contract
namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class SystemInfo
    {
        /// <summary>
        /// Default ping interval for executors
        /// </summary>
        public const long DefaultPingInterval = 10000;

        private string id;
        private ProcessorSystemInfo[] processors;
        private DiskSystemInfo[] disks;
        private MemorySystemInfo memory;

        private OSInfo operatingSystem;

        // NOTE: is the URI even needed?
        private string uri;

        // NOTE: should all these properties below be in a seperate class and/or db table?
        private bool dedicated;
        private long pingInterval = DefaultPingInterval; //this tells the manager when to expect the next ping
        private DateTime lastPingTime; //this is used only on the manager side to determine when the executor has last pinged.

        private SystemInfo() { }

        /// <summary>
        /// Creates an instance of the SystemInfo class
        /// </summary>
        /// <param name="id"></param>
        /// <param name="processors"></param>
        /// <param name="disks"></param>
        /// <param name="memory"></param>
        /// <param name="operatingSystem"></param>
        /// <param name="uri"></param>
        /// <param name="dedicated"></param>
        /// <param name="pingInterval"></param>
        public SystemInfo(string id, ProcessorSystemInfo[] processors, DiskSystemInfo[] disks, 
            MemorySystemInfo memory, OSInfo operatingSystem, string uri, bool dedicated, long pingInterval) : this()
        {
            this.Id = id;
            this.Processors = processors;
            this.Disks = disks;
            this.Memory = memory;
            this.OS = operatingSystem;
            this.Uri = uri;
            this.Dedicated = dedicated;

            //set the last ping time to min-Utc - by default
            this.LastPingTime = Helper.MinUtcDate;
        }

        /// <summary>
        /// Gets the id of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Id
        {
            get { return id; }
            private set { this.id = value ?? String.Empty; } 
        }

        /// <summary>
        /// Gets the processor information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public ProcessorSystemInfo[] Processors
        {
            get { return processors; }
            private set
            {
                if (value == null)
                    processors = new ProcessorSystemInfo[0];
                else
                    processors = value;
            }
        }

        /// <summary>
        /// Gets the hard disk information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DiskSystemInfo[] Disks
        {
            get { return disks; }
            private set
            {
                if (value == null)
                    disks = new DiskSystemInfo[0];
                else
                    disks = value;
            }
        }

        /// <summary>
        /// Gets the memory information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public MemorySystemInfo Memory
        {
            get { return memory; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("memory");
                memory = value;
            }
        }

        /// <summary>
        /// Gets the operation system information of the system
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public OSInfo OS
        {
            get { return operatingSystem; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("operatingSystem");
                operatingSystem = value;
            }
        }

        /// <summary>
        /// Gets the uri of the system
        /// </summary>
        // Do we need this?
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Uri
        {
            get { return uri; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("uri");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Uri cannot be empty", "uri");
                uri = value;
            }
        }

        /// <summary>
        /// Gets a value indicating if this system is a dedicated node
        /// </summary>//todoDoc: provide more info / links to show what is meant by dedicated
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public bool Dedicated
        {
            get { return dedicated; }
            private set
            {
                dedicated = value;
            }
        }

        /// <summary>
        /// Gets the interval (in milliseconds) between consecutive 'ping's of this Executor. This value tells the Manager when to expect the next 'ping' / contact. 
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long PingInterval
        {
            get { return pingInterval; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Ping interval cannot be less than 0");
                pingInterval = value;
            }
        }

        /// <summary>
        /// Gets the date when this Executor last contacted the manager.
        /// </summary>
        /// <remarks>
        /// Validates the ping time to make sure its a valid date and not a time in the future.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime LastPingTime
        {
            get { return lastPingTime; }
            set //was internal: needed to change, to avoid InternalsVisibleTo attribs.
            {
                DateTime temp = Helper.MakeUtc(value);
                if (temp > DateTime.UtcNow.AddMinutes(5)) //MAGIC - for some leniency in time-differences
                    throw new ArgumentOutOfRangeException("lastPingTime",
                        "Last ping time cannot be set to a time in the future. Current time UTC: " + DateTime.UtcNow + ", trying to set to: " + temp);
                else if (temp > DateTime.UtcNow)
                    temp = DateTime.UtcNow; //reset it to current time
                lastPingTime = temp;
            }
        }

        /// <summary>
        /// Gets the string representation of this class.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SystemInfo : {0} ", id).AppendLine();

            if (processors != null)
            {
                sb.AppendFormat("- Processors : {0}", processors.Length).AppendLine();
                for(int i = 0; i < processors.Length; i++)
                    sb.AppendFormat("- {0} : {1}", i, processors[i]).AppendLine();
            }
            else
            {
                sb.AppendLine("- No processors.");
            }

            if (disks != null)
            {
                sb.AppendFormat("- Disks : {0}", disks.Length).AppendLine();
                for (int i = 0; i < disks.Length; i++)
                    sb.AppendFormat("- {0} : {1}", i, disks[i]).AppendLine();
            }
            else
            {
                sb.AppendLine("- No disks.");
            }

            sb.AppendFormat("- Memory : {0}", memory).AppendLine();
            sb.AppendFormat("- OS : {0}", operatingSystem).AppendLine();
            sb.AppendFormat("- Uri : {0}", uri).AppendLine();
            sb.AppendFormat("- Dedicated : {0}", dedicated).AppendLine();
            sb.AppendFormat("- Last Ping Time : {0}", lastPingTime).AppendLine();
            sb.AppendFormat("- Ping Interval : {0}", pingInterval).AppendLine();

            return sb.ToString();
        }
    }
}
