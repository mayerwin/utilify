﻿using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Security
{

    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class GroupInfo
    {
        private int id;
        private string name;

        public GroupInfo(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public int Id
        {
            get
            {
                return id;
            }
            private set
            {
                this.id = value; //todo: finish validation etc.
            }
        }
    }
}
