﻿using Utilify.Platform.Contract;

#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform
{
#if !MONO
    [ServiceContract(Namespace = Namespaces.Manager)]
    [DataContractFormat(Style = OperationFormatStyle.Document)]
    //to use Xml Serializer : [XmlSerializerFormat(Style = OperationFormatStyle.Document, Use = OperationFormatUse.Literal)]
#endif
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1, EmitConformanceClaims = true, Namespace = Namespaces.ManagerCompat)]
    public interface IWorkloadManager : IManagerService
    {
        //todoLater: see if we can refactor this and the ApplicationManager to share code.
#if !MONO
        [OperationContract]
        //[FaultContract(typeof(EntityNotFoundFault))]
        //[FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        ApplicationSubmissionInfo[] GetApplications();

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        JobInfo[] GetJobs(string applicationId);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void AbortApplications(string[] applicationIds);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void AbortJobs(string[] jobIds);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void PauseApplications(string[] applicationIds);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        ApplicationStatusInfo[] GetApplicationStatus(string[] applicationIds);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        JobStatusInfo[] GetJobStatus(string[] jobIds);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        JobStatusInfo[] GetJobStatusForApplication(string applicationId);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void ResumeApplications(string[] applicationIds);

#if !MONO
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void ResetJobs(string[] jobIds);
    }
}
