﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Utilify.Platform
{
    /// <summary>
    /// The exception that is thrown when the current process encounters an unhandled exception, and the process needs to be forcibly terminated.
    /// </summary>
    [Serializable]
    public class ApplicationTerminatedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationTerminatedException">ApplicationTerminatedException</see> class.
        /// </summary>
        public ApplicationTerminatedException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationTerminatedException">ApplicationTerminatedException</see> class 
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public ApplicationTerminatedException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationInitializationException">ApplicationTerminatedException</see> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public ApplicationTerminatedException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="ApplicationTerminatedException">ApplicationTerminatedException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected ApplicationTerminatedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
