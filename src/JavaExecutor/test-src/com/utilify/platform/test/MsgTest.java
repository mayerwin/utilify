package com.utilify.platform.test;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import com.utilify.platform.executor.ExecuteJobMessageData;
import com.utilify.platform.executor.MessageDataHelper;


public class MsgTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello World!");
		try {
	    	File f = new File("C://javamsg.data");
	    	FileInputStream fis = new FileInputStream(f);
	    	// Could just pass a stream here instead of processing the data twice...
	    	byte[] data = MessageDataHelper.readNextBytes(fis);
	    	ExecuteJobMessageData msgData = new ExecuteJobMessageData(data);
	    	System.out.println("Printing:\n"+msgData.toString());
	    	ByteArrayInputStream bis = new ByteArrayInputStream(msgData.getInitialJobInstance());
	    	ObjectInputStream ois = new ObjectInputStream(bis);
	    	Abcd job = (Abcd) ois.readObject();
	    	System.out.println("Job says:"+ job.GetText());
		} catch (Exception e) {
			// some error
			e.printStackTrace();
		}
		System.out.println("Finished.");
	}

}
