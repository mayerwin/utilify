package com.utilify.platform.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;

import com.utilify.framework.DependencyType;
import com.utilify.framework.ExecutionContext;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.client.Dependent;
import com.utilify.framework.client.Executable;

@SuppressWarnings("serial")
public class AdditionWithInputJob implements Executable, Dependent {
	
     private int valueA;
     private int valueB;
     private int result;
     private int inputValue = 0;
     private String inputFilename = "addition.input";

     public AdditionWithInputJob(int valueA, int valueB) {
         this.valueA = valueA;
         this.valueB = valueB;
     }

     public void execute(ExecutionContext context) throws IOException {
         // Try to Add extra number from input file.  	 
         String inputPath = context.getWorkingDirectory() + File.separator + inputFilename;
         File inputFile = new File(inputPath);
         if (inputFile.exists())
         {
             //Try read extra number to add from file
        	 BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        	 String inputValueString = reader.readLine();
        	 inputValue = Integer.parseInt(inputValueString);
        	 reader.close();
         }
         // Do Addition
         this.result = this.valueA + this.valueB + this.inputValue;
         System.out.println("Adding "+valueA+" and "+valueB+" and "+inputValue+".");
     }

     public int getValueA() {
         return this.valueA;
     }

     public int getValueB() {
         return this.valueB;
     }

     public int GetInputValue() {
         return this.inputValue;
     }

     public int getResult() {
         return this.result;
     }
     
	public DependencyInfo[] getDependencies() {
        DependencyInfo info = null;
		try {
			info = new DependencyInfo("addition.input", "testData/addition.input", DependencyType.DATA_FILE);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        return new DependencyInfo[] { info };
	}
 }