package com.utilify.platform.executor;

import java.io.Serializable;

public class JobExecutionInfo implements Serializable {

	private static final long serialVersionUID = -3987306474367438740L;

	private String jobId;
	private String applicationId;
	private byte[] jobInstance;
	private String jobDirectory; //the base of the job
	
	private JobExecutionInfo() {
	}

	public JobExecutionInfo(String jobId, String applicationId,
			byte[] jobInstance, String jobDirectory) {

		setJobId(jobId);
		setApplicationId(applicationId);
		setJobInstance(jobInstance);
		setJobDirectory(jobDirectory);
	}

	public String getJobId() {
		return jobId;
	}

	private void setJobId(String value) {
		jobId = value;
	}

	public String getApplicationId() {
		return applicationId;
	}

	private void setApplicationId(String value) {
		applicationId = value;
	}

	public byte[] getJobInstance() {
		return jobInstance;
	}

	private void setJobInstance(byte[] value) {
		jobInstance = value;
	}

	public String getJobDirectory() {
		return jobDirectory;
	}

	private void setJobDirectory(String value) {
		jobDirectory = value;
	}
}