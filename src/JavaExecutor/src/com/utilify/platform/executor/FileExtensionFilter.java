package com.utilify.platform.executor;

import java.io.File;
import java.io.FilenameFilter;

public class FileExtensionFilter implements FilenameFilter {

	private String extension = "";
	
	private FileExtensionFilter(){}
	
	public FileExtensionFilter(String extension){
		if (extension == null)
			throw new IllegalArgumentException("File extension cannot be null");
		this.extension = extension;
	}
	
	public boolean accept(File arg0, String arg1) {
		return (arg1 != null && arg1.endsWith(extension));
	}

}
