package com.utilify.platform.executor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class AbortJobMessageData implements MessageData {

	private String jobId;
	
	public AbortJobMessageData(String jobId) {
		this.jobId = jobId;
	}

	public AbortJobMessageData(byte[] bytes) {
    	try {
			ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
			initialize(stream);
	        stream.close();
    	} catch (Exception e) {
			// some error.
    		e.printStackTrace();
    	}
	}

	public AbortJobMessageData(InputStream stream) {
		initialize(stream);
	}

	private void initialize(InputStream stream) {
		try {
			this.jobId = MessageDataHelper.readNextLine(stream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getJobId() {
		return jobId;
	}

	public boolean isValid() {		
		return (jobId != null && jobId.trim().length() > 0);
	}
}
