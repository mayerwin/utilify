﻿
#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform
{

#if !MONO
    /// <summary>
    /// Specifies the contract for the application manager service.
    /// The application manager service is responsible for handling application (and job) submission requests,
    /// and queue it for scheduling. The service also handles status query requests, and managing the lifetime of an application
    /// and its jobs and dependencies.
    /// </summary>
    [ServiceContract(Namespace = Namespaces.Manager)]
    [DataContractFormat(Style = OperationFormatStyle.Document)]
    //to use Xml Serializer : [XmlSerializerFormat(Style = OperationFormatStyle.Document, Use = OperationFormatUse.Literal)]
#endif
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1, EmitConformanceClaims = true, Namespace = Namespaces.ManagerCompat)]
    public interface IApplicationManager : IManagerService
    {

#if !MONO
        /// <summary>
        /// Aborts the application with the specified id.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the application id is null or empty</item>
        ///     <item><see cref="EntityNotFoundFault" /> when the application id is not found</item>
        ///     <item><see cref="InvalidOperationFault" /> when the application cannot be aborted due to its current state</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="applicationId">The application id.</param>
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void AbortApplication(string applicationId);

#if !MONO
        /// <summary>
        /// Aborts the job.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the job id is null or empty</item>
        ///     <item><see cref="EntityNotFoundFault" /> when the job id is not found</item>
        ///     <item><see cref="InvalidOperationFault" /> when the job cannot be aborted due to its current state</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="jobId">The job id.</param>
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void AbortJob(string jobId);

#if !MONO
        /// <summary>
        /// Gets the unresolved dependencies.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the query argument is null or invalid</item>
        ///     <item><see cref="EntityNotFoundFault" /> when the application or job id is not found</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="query">The query to filter for an application or job.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        DependencyInfo[] GetUnresolvedDependencies(DependencyQueryInfo query);

#if !MONO
        /// <summary>
        /// Pauses the application.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the application id is null or empty</item>
        ///     <item><see cref="EntityNotFoundFault" /> when the application id is not found</item>
        ///     <item><see cref="InvalidOperationFault" /> when the application cannot be paused due to its current state</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="applicationId">The application id.</param>
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void PauseApplication(string applicationId);

#if !MONO
        /// <summary>
        /// Gets the application status.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the application id is null or empty</item>
        ///     <item><see cref="EntityNotFoundFault" /> when the application id is not found</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="applicationId">The application id.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        ApplicationStatusInfo GetApplicationStatus(string applicationId);

#if !MONO
        /// <summary>
        /// Gets the job status.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the job id(s) is/are null or empty</item>
        ///     <item><see cref="EntityNotFoundFault" /> when the job id(s) is/are not found</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="jobIds">The job ids.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        JobStatusInfo[] GetJobStatus(string[] jobIds);

#if !MONO
        /// <summary>
        /// Resumes the application.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the application id is null or empty</item>
        ///     <item><see cref="EntityNotFoundFault" /> when the application id is not found</item>
        ///     <item><see cref="InvalidOperationFault" /> when the application cannot be resumed due to its current state</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="applicationId">The application id.</param>
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void ResumeApplication(string applicationId);

#if !MONO
        /// <summary>
        /// Sends the content for a dependency.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the dependency id is null or empty</item>
        ///     <item><see cref="EntityNotFoundFault" /> when the dependency id is not found</item>
        ///     <item><see cref="InvalidOperationFault" /> when the dependency content was already sent earlier</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="dependency">The dependency content.</param>
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        void SendDependency(DependencyContent dependency);

#if !MONO
        /// <summary>
        /// Submits a new application.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="ArgumentFault" /> when the appInfo is null or contains invalid data</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="appInfo">The application submission information.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        string SubmitApplication(ApplicationSubmissionInfo appInfo);

#if !MONO
        /// <summary>
        /// Submits the jobs for an application.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when the parent application for the jobs is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the jobInfo is null or contains invalid data</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="jobInfo">The job info.</param>
        /// <returns></returns>
        [OperationContract] //todo: shouldn't this cause an invalid operation exception if the app is already paused, aborted etc?
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        string[] SubmitJobs(JobSubmissionInfo[] jobInfo);

#if !MONO
        /// <summary>
        /// Gets the results produced (if any) by the execution of the job.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when the job is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the jobId is null or empty</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="jobId">The job id.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        ResultStatusInfo[] GetJobResults(string jobId);

#if !MONO
        /// <summary>
        /// Gets the completed job instance.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when the job is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the jobId is null or empty</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="jobId">The job id.</param>
        /// <returns>The job completion information.</returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        JobCompletionInfo GetCompletedJob(string jobId);

#if !MONO
        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when the result is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the resultId is null or empty</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="resultId">The result id.</param>
        /// <returns>The result content</returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        //[WebMethod]
        ResultContent GetResult(string resultId);

    }
}
