﻿using System.ServiceModel;

namespace Utilify.Platform
{
    /// <summary>
    /// Specifies the contract for a simple data transfer service.
    /// The data transfer service is used to stream files to and from the Manager.
    /// </summary>
    /* 
     * Important configuration options required for this service:
     * 
     * maxReceivedMessageSize = set this to max long value
     * messageEncoding = Mtom / Binary 
     * transferMode = Streamed
     * maxBufferSize = 1MB
     * receiveTimeout= some largish value (2 hours?)
     * sendTimeout= some largish value (2 hours?)
     * 
     * Supported bindings: NetTcp (can use Binary encoding), BasicHttp, WsHttp
     * 
     * Ideally, the service should be configured as a singleton as hosted using an existing object,
     * which is configured with a DataDirectory.
     * 
     * Since this is a streamed service, we need to configure it as a separate endpoint 
     * 
     * IF used in IIS, set httpRuntime maxRequestLength to a high value.
     * 
     * TODO: Implement reliable file transfers:
     * http://blogs.msdn.com/yassers/archive/2006/01/21/515887.aspx
     * Either using WS-RM (and a chunking channel) OR
     * Keeping track of bytes read/write so far and persisting requests to read/write for a while,
     * so that a subsequent request to resume/recover from a crashed file transfer can use that info and 
     * continue the transfer.
     */
#if !MONO
    [ServiceContract(Namespace = Namespaces.Manager)]
    [DataContractFormat(Style = OperationFormatStyle.Document)]
#endif
    [StreamedBehavior]
    public interface IDataTransferService : IManagerService
    {
#if !MONO
        /// <summary>
        /// Uploads the specified file into the data store
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when (part of) the destination path is not found and CreatePath is set to false</item>
        ///     <item><see cref="ArgumentFault" /> when the info is null or invalid</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="info">The info.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        DataObjectInfo Put(DataTransferInfo info);

#if !MONO
        /// <summary>
        /// Gets the specified file from the data store.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when (part of) the path is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the info is null or invalid</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="info">The info.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        DataTransferInfo Get(DataObjectInfo info); //need to have a MessageContract type for both params and return values - or else it wont work.

#if !MONO
        /// <summary>
        /// Lists the objects in the specified container name.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when (part of) the path is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the containerName is null or empty</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="containerName">Name of the container.</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        string[] List(string containerName);

#if !MONO
        /// <summary>
        /// Creates the specified container in the data store.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when (part of) the path is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the containerName is null or empty</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="containerName">Path and name of the container to create.</param>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        void MkDir(string containerName);

#if !MONO
        /// <summary>
        /// Removes (deletes) the specified container.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when (part of) the container path is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the containerName is null or empty</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="containerName">Path and name of the container to remove.</param>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        void RmDir(string containerName);

#if !MONO
        /// <summary>
        /// Deletes the specified object. If the object is a container, use <see cref="RmDir" /> instead.
        /// </summary>
        /// <remarks>
        /// A call to this operation can cause the following faults: <br/>
        /// <list type="bullet">
        ///     <item><see cref="EntityNotFoundFault" /> when (part of) the path is not found</item>
        ///     <item><see cref="ArgumentFault" /> when the objectName is null or empty</item>
        ///     <item><see cref="AuthenticationFault" /> when the request is unauthenticated</item>
        ///     <item><see cref="AuthorizationFault" /> when the request is unauthorised</item>
        ///     <item><see cref="ServerFault" /> when an unexpected exception occurs in processing this request</item>
        /// </list>
        /// </remarks>
        /// <param name="objectName">Path and name of the object.</param>
        [OperationContract]
        [FaultContract(typeof(EntityNotFoundFault))]
        [FaultContract(typeof(ArgumentFault))]
        [FaultContract(typeof(ServerFault))]
        [FaultContract(typeof(AuthenticationFault))]
        [FaultContract(typeof(AuthorizationFault))]
#endif
        void Delete(string objectName);

    }
}
