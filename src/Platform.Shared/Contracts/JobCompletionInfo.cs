using System;
using System.Text;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents a job that has completed execution
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class JobCompletionInfo
    {
        private string jobId;
        private string applicationId;
        private byte[] jobInstance;
        private byte[] jobException;
        private string executionLog;
        private string executionError;

        private string executionHost;

        private DateTime submittedTime;
        private DateTime scheduledTime;
        private DateTime startTime;
        private DateTime finishTime;

        private long cpuTime;
        private int mappedCount;        

        // required for serialization on the executor
        private JobCompletionInfo() { }

        //internal use only.
        /// <summary>
        /// Creates an instance of the JobCompletionInfo class with the specified parameters.
        /// This constructor is for internal use only.
        /// </summary>
        /// <param name="jobId">id of the job</param>
        /// <param name="applicationId">id of the associated application</param>
        /// <param name="jobInstance">serialized instance of the job object</param>
        /// <param name="jobException">serialized instance of the job execution exception</param>
        /// <param name="executionLog">the execution log of the job</param>
        /// <param name="executionError">the execution errors that may have occured during job execution</param>
        /// <param name="cpuTime">The cpu time.</param>
        /// <exception cref="System.ArgumentNullException">applicationId or jobInstance is a null reference</exception>
        /// <exception cref="System.ArgumentException">
        /// - applicationId is empty Or, <br/>
        /// - jobInstance is empty
        /// </exception>
        public JobCompletionInfo(string jobId, string applicationId, byte[] jobInstance, byte[] jobException,
            string executionLog, string executionError, long cpuTime) 
            : this(jobId, applicationId, jobInstance, jobException, executionLog, executionError, string.Empty, 
            DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, 
            cpuTime, 0)
        {
            //mapped count, scheduled/start/finish times are irrelevant on the exeuctor, where this ctor is used.
            //we just set them to their default values:
            //the manager takes care of updating them when appropriate
        }

        /// <summary>
        /// This constructor is for internal use only.
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="applicationId"></param>
        /// <param name="jobInstance"></param>
        /// <param name="jobException"></param>
        /// <param name="executionLog"></param>
        /// <param name="executionError"></param>
        /// <param name="executionHost"></param>
        /// <param name="submittedTime"></param>
        /// <param name="scheduledTime"></param>
        /// <param name="startTime"></param>
        /// <param name="finishTime"></param>
        /// <param name="cpuTime"></param>
        /// <param name="mappedCount"></param>
        public JobCompletionInfo(string jobId, string applicationId, byte[] jobInstance, byte[] jobException,
            string executionLog, string executionError, string executionHost,
            DateTime submittedTime, DateTime scheduledTime, DateTime startTime, DateTime finishTime, 
            long cpuTime, int mappedCount)
        {
            this.JobId = jobId;
            this.ApplicationId = applicationId;
            this.JobInstance = jobInstance;
            this.JobException = jobException;
            this.ExecutionLog = executionLog;
            this.ExecutionError = executionError;
            this.ExecutionHost = executionHost;
            this.SubmittedTime = submittedTime;
            this.ScheduledTime = scheduledTime;
            this.StartTime = startTime;
            this.FinishTime = finishTime;
            this.CpuTime = cpuTime;
            this.MappedCount = mappedCount;
        }

        /// <summary>
        /// Gets the job id
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string JobId //was internal: changed to avoid InternalsVisibleTo attrib
        {
            get { return jobId; }
            private set 
            {
                if (value == null)
                    throw new ArgumentNullException("jobId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Job Id cannot be empty", "jobId");

                jobId = value;
            }
        }

        /// <summary>
        /// Gets the execution host.
        /// </summary>
        /// <value>The execution host.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ExecutionHost
        {
            get { return executionHost; }
            private set 
            {
                //execution host can be null/empty : if this job failed before ever getting to an executor
                executionHost = value; 
            }
        }

        /// <summary>
        /// Gets the id of the application associated with this job
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationId is a null reference</exception>
        /// <exception cref="System.ArgumentException">applicationId is empty</exception>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ApplicationId
        {
            get { return applicationId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("applicationId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application id cannot be empty", "applicationId");

                applicationId = value;
            }
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        private byte[] JobInstance
        {
            get { return jobInstance; }
            set 
            {
                if (value == null)
                    throw new ArgumentNullException("jobInstance");
                if (value.Length == 0)
                    throw new ArgumentException("The serialised job instance cannot be empty", "jobInstance");
                jobInstance = value; 
            }
        }

        /// <summary>
        /// Gets the serialised instance of the job
        /// </summary>
        /// <returns>serialised instance of the job</returns>
        public byte[] GetJobInstance()
        {
            return JobInstance;
        }

#if !MONO
        [DataMember(IsRequired = false)]
#endif
        private byte[] JobException
        {
            get { return jobException; }
            set { jobException = value; }
        }

        /// <summary>
        /// Gets the exception that occured during job execution
        /// </summary>
        /// <returns></returns>
        public byte[] GetJobException()
        {
            return JobException;
        }

        /// <summary>
        /// Gets the execution log
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ExecutionLog
        {
            get { return executionLog; }
            private set
            {
                if (value == null)
                    value = string.Empty;
                executionLog = value;
            }
        }

        /// <summary>
        /// Gets the execution errors, if any.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ExecutionError
        {
            get { return executionError; }
            private set
            {
                if (value == null)
                    value = string.Empty;
                executionError = value;
            }
        }

        /// <summary>
        /// Gets the start time.
        /// </summary>
        /// <value>The start time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime StartTime
        {
            get { return startTime; }
            private set { startTime = Helper.MakeUtc(value); } //on the executor this won't be set.
        }

        /// <summary>
        /// Gets the finish time.
        /// </summary>
        /// <value>The finish time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime FinishTime
        {
            get { return finishTime; }
            private set { finishTime = Helper.MakeUtc(value); } //on the executor this won't be set.
        }

        /// <summary>
        /// Gets the scheduled time.
        /// </summary>
        /// <value>The scheduled time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime ScheduledTime
        {
            get { return scheduledTime; }
            private set { scheduledTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the submitted time.
        /// </summary>
        /// <value>The submitted time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DateTime SubmittedTime
        {
            get { return submittedTime; }
            private set { submittedTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the cpu time.
        /// </summary>
        /// <value>The cpu time.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long CpuTime
        {
            get { return cpuTime; }
            private set 
            {
                if (value < 0)
                    throw new ArgumentException("Value cannot be negative: cpuTime");

                cpuTime = value;
            }
        }

        /// <summary>
        /// Gets the mapped count (the number of times the job was scheduled before it got executed).
        /// </summary>
        /// <value>The mapped count.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public int MappedCount
        {
            get { return mappedCount; }
            private set 
            {
                if (value < 0)
                    throw new ArgumentException("Value cannot be negative: mappedCount");
                mappedCount = value;
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            JobCompletionInfo other = obj as JobCompletionInfo;
            if (other == null)
                return false;

            if (object.ReferenceEquals(other, this))
                return true;

            return (other.JobId == this.JobId) &&
                (other.ApplicationId == this.ApplicationId) &&
                (other.ExecutionHost == this.ExecutionHost) &&
                (other.ExecutionLog == this.ExecutionLog) &&
                (other.ExecutionError == this.ExecutionError) &&
                (Helper.AreEqual(other.FinishTime, this.FinishTime)) &&
                (Helper.AreEqual(other.StartTime, this.StartTime)) &&
                (Helper.AreEqual(other.ScheduledTime, this.ScheduledTime)) &&
                (other.CpuTime == this.CpuTime) && 
                (other.MappedCount == this.MappedCount);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.applicationId.GetHashCode() ^ this.jobId.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string fullDateTimeFormat = Constants.FullDateTimeFormat;

            var jobInstanceLength = this.JobInstance == null ? 0 : this.JobInstance.Length;
            var jobExceptionLength = this.JobException == null ? 0 : this.JobException.Length;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("JobId : {0}", this.JobId).AppendLine();
            sb.AppendFormat("AppId : {0}", this.ApplicationId).AppendLine();
            sb.AppendFormat("Exctor : {0}", this.ExecutionHost).AppendLine();
            sb.AppendFormat("Log : {0}", this.ExecutionLog).AppendLine();
            sb.AppendFormat("Error : {0}", this.ExecutionError).AppendLine();
            sb.AppendFormat("Instance length : {0}", jobInstanceLength).AppendLine();
            sb.AppendFormat("Exception Instance length : {0}", jobExceptionLength).AppendLine();
            sb.AppendFormat("Submitted : {0}", this.SubmittedTime.ToString(fullDateTimeFormat)).AppendLine();
            sb.AppendFormat("Scheduled : {0}", this.ScheduledTime.ToString(fullDateTimeFormat)).AppendLine();
            sb.AppendFormat("Start : {0}", this.StartTime.ToString(fullDateTimeFormat)).AppendLine();
            sb.AppendFormat("Finish : {0}", this.FinishTime.ToString(fullDateTimeFormat)).AppendLine();
            sb.AppendFormat("Cpu time : {0}", Helper.GetFormattedTimeSpan(this.CpuTime)).AppendLine();
            return sb.ToString();
        }
    }
}
