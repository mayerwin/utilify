using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents the file data of a dependency
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class DependencyContent
    {
        private byte[] content;
        private string dependencyId;

        private DependencyContent() {}

        /// <summary>
        /// Creates an instance of the DependencyContent class with the specified parameters
        /// </summary>
        /// <param name="dependencyId">id of the dependency</param>
        /// <param name="content">dependency file contents</param>
        /// <exception cref="System.ArgumentNullException">dependencyId or content is a null reference</exception>
        /// <exception cref="System.ArgumentException">dependencyId or content is empty</exception>
        public DependencyContent(string dependencyId, byte[] content) : this()
        {
            this.DependencyId = dependencyId;
            this.Content = content;
        }

        /// <summary>
        /// Gets the binary serialized contents of the dependency
        /// </summary>
        /// <returns>a byte[] representing the serialized file data</returns>
        public byte[] GetContent()
        {
             return content; 
        }

        /// <summary>
        /// Gets the dependency id
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string DependencyId
        {
            get { return dependencyId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("dependencyId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("The dependency id cannot be empty", "dependencyId");
                dependencyId = value;
            }
        }

#if !MONO
        [DataMember(IsRequired = true)]
#endif
        private byte[] Content
        {
            get { return content; }
            set 
            {
                if (value == null)
                    throw new ArgumentNullException("content");
                if (value.Length == 0)
                    throw new ArgumentException("Content cannot be empty","content");
                content = value;
            }
        }
    }
}