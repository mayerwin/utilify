using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents information about a job execution result.
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class ResultInfo
    {
        private string id;
        private string fileName;
        private ResultRetrievalMode retrievalMode;

        private ResultInfo() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultInfo"/> class.
        /// </summary>
        /// <param name="fileNameOrUri">The file name or URI.</param>
        /// <param name="retrievalMode">The retrieval mode.</param>
        public ResultInfo(string fileNameOrUri, ResultRetrievalMode retrievalMode) : this()
        {
            this.Id = string.Empty; //to ensure it is not a null string
            this.FileName = fileNameOrUri;
            this.RetrievalMode = retrievalMode;
        }

        /// <summary>
        /// This constructor is for internal use only.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="fileNameOrUri">The file name or URI.</param>
        /// <param name="retrievalMode">The retrieval mode.</param>
        public ResultInfo(string id, string fileNameOrUri, ResultRetrievalMode retrievalMode)
            : this(fileNameOrUri, retrievalMode)
        {
            //the Id property setter is used in deserialization as well.
            //but if this ctor is used, the Id needs to be set correctly.
            if (id == null)
                throw new ArgumentNullException("id");
            if (id.Trim().Length == 0)
                throw new ArgumentException("Result id cannot be empty", "id");

            this.Id = id;
        }

        /// <summary>
        /// Gets the id of the result
        /// </summary>
#if !MONO
        [DataMember(IsRequired = false)]
#endif
        public string Id //was internal, had to change to public -  to avoid InternalsVisibleTo
        {
            get { return id; }
            private set { id = value ?? string.Empty; }
        }

        /// <summary>
        /// Gets or sets the name of the result file.
        /// </summary>
        /// <value>The name of the file.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string FileName
        {
            get { return fileName; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("fileName");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("File name or url cannot be empty", "fileName");
                fileName = value;
            }
        }

        /// <summary>
        /// Gets or sets the retrieval mode.
        /// </summary>
        /// <value>The retrieval mode.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public ResultRetrievalMode RetrievalMode
        {
            get { return retrievalMode; }
            private set { retrievalMode = value; }
        }

        /// <summary>
        /// A ResultInfo instance is considered to be 'equal' to another ResultInfo instance
        /// if filenames are the same and the retrieval mode is the same.
        /// See <see cref="M:System.Object.Equals">System.Object.Equals()</see>
        /// </summary>
        /// <param name="other">the ResultInfo object to compare this object to</param>
        /// <returns>true, if the specified object is equal to this object, false otherwise</returns>
        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (other.GetType() != this.GetType())
                return false;

            bool areEqual = false;
            ResultInfo otherInfo = other as ResultInfo;
            areEqual = (otherInfo.FileName == this.FileName) &&
                (otherInfo.RetrievalMode == this.RetrievalMode);

            return areEqual;
        }

        private int hashCode = 0;
        /// <summary>
        /// See <see cref="M:System.Object.GetHashCode">System.Object.GetHashCode</see>
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (hashCode == 0)
            {
                hashCode = 420 ^ retrievalMode.GetHashCode();
                if (fileName != null)
                    hashCode = fileName.GetHashCode() ^ hashCode;
            }
            return hashCode;
        } //this object is not really intended to be used as a key in a hashtable, but its good to override GetHashCode when overriding Equals

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("ResultInfo : {0}, {1}.", this.FileName, this.RetrievalMode);
        }
    }
}