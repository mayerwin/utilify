﻿using System;
using System.IO;

namespace Utilify.Platform
{
    /// <summary>
    /// Contains the data for the progress changed event of the <see cref="SequentialReadOnlyStream"/>
    /// </summary>
    public class ProgressChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the bytes read from the stream so far.
        /// </summary>
        /// <value>The bytes read.</value>
        public long BytesRead { get; private set; }
        /// <summary>
        /// Gets the length of the data stream.
        /// </summary>
        /// <value>The length.</value>
        public long Length { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressChangedEventArgs"/> class.
        /// </summary>
        /// <param name="bytesRead">The bytes read.</param>
        /// <param name="length">The length.</param>
        public ProgressChangedEventArgs(long bytesRead, long length)
        {
            this.BytesRead = bytesRead;
            this.Length = length;
        }
    }
    
    //can be used in file uploads etc
    /// <summary>
    /// Represents a stream that can only be read from sequentially.
    /// Writes to this stream are not supported, and operations such as seeking, positioning the read/write pointer are not supported.
    /// 
    /// An instance of this class is generally used to encapsulate an incoming data stream (such as a data from a file or network).
    /// </summary>
    public class SequentialReadOnlyStream : Stream
    {
        /// <summary>
        /// Occurs when the read progress is changed.
        /// </summary>
        public event EventHandler<ProgressChangedEventArgs> ProgressChanged;

        private readonly Stream innerStream;
        private readonly long length;
        private long bytesRead;

        /// <summary>
        /// Initializes a new instance of the <see cref="SequentialReadOnlyStream"/> class.
        /// </summary>
        /// <param name="stream">The stream to encapsulats.</param>
        public SequentialReadOnlyStream(Stream stream)
        {
            try
            {
                this.length = stream.Length;
            }
            catch (NotSupportedException)
            {
                throw new ArgumentException("ReadOnlyStream only supports streams whose length can be determined.", "stream");
            }
            this.innerStream = stream;
            bytesRead = 0;

            OnProgressChanged();
        }

        private void OnProgressChanged()
        {
            ProgressChangedEventArgs pa = new ProgressChangedEventArgs(bytesRead, length);
            EventHelper.RaiseEvent<ProgressChangedEventArgs>(ProgressChanged, this, pa);
        }

        /// <summary>
        /// Gets the progress of reading from the stream.
        /// </summary>
        /// <returns></returns>
        public double GetProgress()
        {
            return ((double)bytesRead) / length;
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports reading.
        /// This stream supports reading, if the underlying stream supports it.
        /// </summary>
        /// <value></value>
        /// <returns>true, if the underlying stream supports reading; false, otherwise.
        /// </returns>
        public override bool CanRead
        {
            get { return this.innerStream.CanRead; }
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports seeking.
        /// This stream does not support seeking.
        /// </summary>
        /// <value></value>
        /// <returns>false, always.
        /// </returns>
        public override bool CanSeek
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports writing.
        /// This stream does not support writing.
        /// </summary>
        /// <value></value>
        /// <returns>false, always.
        /// </returns>
        public override bool CanWrite
        {
            get { return false; }
        }

        /// <summary>
        /// This stream does not support the Flush operation.
        /// </summary>
        /// <exception cref="T:System.NotSupportedException">Always</exception>
        public override void Flush() 
        {
            throw new NotSupportedException("The method or operation is not supported.");
        }

        /// <summary>
        /// Gets the length in bytes of the stream.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// A long value representing the length of the stream in bytes.
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        /// A class derived from Stream does not support seeking.
        /// </exception>
        /// <exception cref="T:System.ObjectDisposedException">
        /// Methods were called after the stream was closed.
        /// </exception>
        public override long Length
        {
            get
            {
                return this.length;
            }
        }

        /// <summary>
        /// Gets the position within the current stream.
        /// Even though the contract for a <see cref="Stream"/> specifies the the position can be set, this stream does not support setting the position.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The current position within the stream.
        /// </returns>
        /// <exception cref="T:System.IO.IOException">
        /// An I/O error occurs.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The stream does not support seeking.
        /// </exception>
        /// <exception cref="T:System.ObjectDisposedException">
        /// Methods were called after the stream was closed.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">If the calling code tries to set the position value</exception>
        public override long Position
        {
            get { return bytesRead; }
            set { throw new NotSupportedException("The method or operation is not supported."); }
        }

        /// <summary>
        /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
        /// </summary>
        /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset"/> and (<paramref name="offset"/> + <paramref name="count"/> - 1) replaced by the bytes read from the current source.</param>
        /// <param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin storing the data read from the current stream.</param>
        /// <param name="count">The maximum number of bytes to be read from the current stream.</param>
        /// <returns>
        /// The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.
        /// </returns>
        /// <exception cref="T:System.ArgumentException">
        /// The sum of <paramref name="offset"/> and <paramref name="count"/> is larger than the buffer length.
        /// </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="buffer"/> is null.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="offset"/> or <paramref name="count"/> is negative.
        /// </exception>
        /// <exception cref="T:System.IO.IOException">
        /// An I/O error occurs.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The stream does not support reading.
        /// </exception>
        /// <exception cref="T:System.ObjectDisposedException">
        /// Methods were called after the stream was closed.
        /// </exception>
        public override int Read(byte[] buffer, int offset, int count)
        {
            int result = innerStream.Read(buffer, offset, count);
            bytesRead += result;

            OnProgressChanged();

            return result;
        }

        /// <summary>
        /// This stream does not support seeking, and calling this operation causes a <see cref="System.NotSupportedException"/>.
        /// </summary>
        /// <param name="offset">A byte offset relative to the <paramref name="origin"/> parameter.</param>
        /// <param name="origin">A value of type <see cref="T:System.IO.SeekOrigin"/> indicating the reference point used to obtain the new position.</param>
        /// <returns>
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        /// The stream does not support seeking.
        /// </exception>
        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException("The method or operation is not supported.");
        }

        /// <summary>
        /// This stream does not support settinsg the length.
        /// </summary>
        /// <param name="value">The desired length of the current stream in bytes.</param>
        /// <exception cref="T:System.NotSupportedException">
        /// The stream does not support both writing and seeking.
        /// </exception>
        public override void SetLength(long value)
        {
            throw new NotSupportedException("The method or operation is not supported.");
        }

        /// <summary>
        /// This stream does not support writing.
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies <paramref name="count"/> bytes from <paramref name="buffer"/> to the current stream.</param>
        /// <param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin copying bytes to the current stream.</param>
        /// <param name="count">The number of bytes to be written to the current stream.</param>
        /// <exception cref="T:System.NotSupportedException">
        /// The stream does not support writing.
        /// </exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException("The method or operation is not supported.");
        }
    }

}
