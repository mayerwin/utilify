﻿using System;
using System.ComponentModel;
using System.Text;
//using Utilify.Platform.ComponentModel;
using Utilify.Platform.Configuration;

namespace Utilify.Platform
{
    //todoDoc: this is part of the public API

    //[TypeDescriptionProvider(typeof(DynamicTypeDescriptionProvider))]
    /// <summary>
    /// Represents the settings used to connect to a service running on the Manager.
    /// </summary>
    [Description("Connection settings used to connect to the Manager")]
    //[TypeConverter(typeof(PropertySorterConverter))]
    public class ConnectionSettings : IConfigurationSettings<ClientElement>
    {
        private static readonly ConnectionSettings defaultSettings;

        private bool isDefault = false;

        static ConnectionSettings()
        {
            defaultSettings = new ConnectionSettings();
            defaultSettings.IsDefault = true;
        }

        public static int DefaultPort
        {
            get
            {
                return ClientElement.DefaultPort;
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is a default settings instance.
        /// This property is for internal use only.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is a default instance; otherwise, <c>false</c>.
        /// </value>
        public bool IsDefault
        {
            get { return isDefault; }
            private set { isDefault = value; }
        }

        /// <summary>
        /// Gets the default connection settings instance.
        /// This property is for internal use only.
        /// </summary>
        /// <value>The default instance.</value>
        public static ConnectionSettings Default
        {
            get { return defaultSettings; }
        }

        //this needs to be public, since we use it in as a generic type parameter elsewhere
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionSettings"/> class.
        /// </summary>
        public ConnectionSettings()
            : this(ClientElement.DefaultHost,
            ClientElement.DefaultPort, ClientElement.DefaultIsInteropClient)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionSettings"/> class.
        /// </summary>
        /// <param name="host">The host name where the service is running.</param>
        public ConnectionSettings(string host)
            : this(host, ClientElement.DefaultPort, ClientElement.DefaultIsInteropClient)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionSettings"/> class.
        /// </summary>
        /// <param name="host">The host name where the service is running.</param>
        /// <param name="port">The service port.</param>
        public ConnectionSettings(string host, int port)
            : this(host, port, ClientElement.DefaultIsInteropClient)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionSettings"/> class.
        /// </summary>
        /// <param name="host">The host name where the service is running.</param>
        /// <param name="port">The service port.</param>
        /// <param name="isInteropClient">if set to <c>true</c> connect to an interop end point (such as a http/https endpoint).</param>
        public ConnectionSettings(string host, int port, bool isInteropClient)
        {
            this.Host = host;
            this.Port = port;
        }

        private string host = ClientElement.DefaultHost;
        /// <summary>
        /// Gets or sets the host name of the service.
        /// </summary>
        /// <value>The host.</value>
        [Category("Connection settings")]
        [Description("Host name of the Manager to connect to")]
        //[PropertySortOrderAttribute(0)]
        public string Host
        {
            get { return host; }
            set 
            {
                if (value == null || value.Trim().Length == 0)
                    throw new ArgumentException("Host name cannot be null or empty");

                if (Uri.CheckHostName(value) == UriHostNameType.Unknown
                        || Uri.CheckHostName(value) == UriHostNameType.Basic)
                    throw new ArgumentException("Host name is invalid");

                host = value;
            }
        }

        private int port = ClientElement.DefaultPort;
        /// <summary>
        /// Gets or sets the service port.
        /// </summary>
        /// <value>The port.</value>
        [Category("Connection settings")]
        [Description("Port number of the Manager to connect to")]
        //[PropertySortOrderAttribute(1)]
        public int Port
        {
            get { return port; }
            set 
            {
                //if (value < 1024 || value > 65536)
                //    throw new ArgumentOutOfRangeException("Port number has to be between 1024 and 65536");
                port = value;
            }
        }

        private bool isInteropClient = ClientElement.DefaultIsInteropClient;
        /// <summary>
        /// Gets or sets a value indicating whether this instance represents a connection to an interop endpoint.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if connecting to an interop endpoint; otherwise, <c>false</c>.
        /// </value>
        [Category("Connection settings")]
        [Description("Specifies whether this client connections an interoperable endpoint (such as http/https) ")]
        //[PropertySortOrderAttribute(1)]
        public bool IsInteropClient
        {
            get { return isInteropClient; }
            set { isInteropClient = value; }
        }

        private CredentialType credentialType = SecurityElement.DefaultCredentialType;
        /// <summary>
        /// Gets or sets the type of the credential.
        /// </summary>
        /// <value>The type of the credential.</value>
        [Category("Connection settings")]
        [Description("The type of credential used to authenticate to the Manager")]
        //[PropertySortOrderAttribute(3)]
        public CredentialType CredentialType
        {
            get { return credentialType; }
            set { credentialType = value; }
        }

        private UsernameCredential usernameCredential = null; //initialise on first use
        /// <summary>
        /// Gets or sets the username credential.
        /// </summary>
        /// <value>The username credential.</value>
        [Category("Connection settings")]
        [Description("The username credential used to authenticate to the Manager")]
        //[BrowsableIf("CredentialType", BrowsableIfAttribute.CompareType.Equals, CredentialType.Username)]
        public UsernameCredential UsernameCredential
        {
            get 
            {
                if (usernameCredential == null)
                    usernameCredential = new UsernameCredential(); //default
                return usernameCredential;
            }
            set { usernameCredential = value; }
        }

        private ServiceCertificate serviceCertificate = null; //initialise on first use
        /// <summary>
        /// Gets or sets the service certificate.
        /// </summary>
        /// <value>The service certificate.</value>
        [Category("Connection settings")]
        [Description("The service certificate details expected from the Manager")]
        //[BrowsableIf("CredentialType", BrowsableIfAttribute.CompareType.Equals, CredentialType.Username)]
        public ServiceCertificate ServiceCertificate
        {
            get 
            {
                if (serviceCertificate == null)
                    serviceCertificate = new ServiceCertificate(this.Host, false);
                return serviceCertificate;
            }
            set { serviceCertificate = value; }
        }

        #region IConfigurationSettings<ClientElement> Members

        /// <summary>
        /// Gets the validation errors.
        /// </summary>
        /// <returns></returns>
        string IConfigurationSettings<ClientElement>.GetValidationErrors()
        {
            StringBuilder errors = new StringBuilder();
            //first check if the host, port, protocol settings are correct:
            if (this.Host == null || this.Host.Trim().Length == 0)
                errors.AppendLine("Host cannot be null or empty");
            //if (this.Port < 1024 || this.Port > 65536)
            //   errors.AppendLine("Port cannot be less than or equal to 1024 or greater than 65536");
            if (this.CredentialType == CredentialType.Username)
            {
                if (this.UsernameCredential == null)
                    errors.AppendLine("User name credential cannot be null");
                else
                {
                    string unError = this.UsernameCredential.GetValidationErrors();
                    if (!string.IsNullOrEmpty(unError))
                        errors.AppendLine(unError);
                }
                if (this.ServiceCertificate != null)
                {
                    string scError = this.ServiceCertificate.GetValidationErrors();
                    if (!string.IsNullOrEmpty(scError))
                        errors.AppendLine(scError);
                }
            }
            return errors.ToString();
        }

        /// <summary>
        /// Updates the configuration element.
        /// </summary>
        /// <param name="element">The element whose values need to be updated.</param>
        void IConfigurationSettings<ClientElement>.UpdateConfigurationElement(ref ClientElement element)
        {
            if (element == null)
                element = new ClientElement(); //just create a new default element

            element.ServiceHost = this.Host;
            element.ServicePort = this.Port;
            element.IsInteropClient = this.IsInteropClient;

            if (element.Security == null)
                element.Security = new SecurityElement();

            //no need to worry about nulls: since all config elements seem to be always available (the .NET config framework initialises them)
            if (this.CredentialType != CredentialType.None)
                element.Security.CredentialType = this.CredentialType;

            switch (this.CredentialType)
            {
                case CredentialType.Username:
                    //since the objects are already instantiated, just set its properties

                    if (element.Security.UserName == null)
                        element.Security.UserName = new UserNameElement();

                    element.Security.UserName.Name = this.UsernameCredential.Username;
                    element.Security.UserName.Password = this.UsernameCredential.Password;

                    if (element.Security.ServiceCertificate == null)
                        element.Security.ServiceCertificate = new ServiceCertificateElement();

                    element.Security.ServiceCertificate.DnsIdentity = this.ServiceCertificate.DnsIdentity;
                    element.Security.ServiceCertificate.Validate = this.ServiceCertificate.Validate;

                    break;
                case CredentialType.Windows:
                    //set elements to null to prevent saving un-necessary config to file
                    element.Security.UserName = null;

                    if (element.Security.ServiceCertificate == null)
                        element.Security.ServiceCertificate = new ServiceCertificateElement();

                    element.Security.ServiceCertificate.DnsIdentity = this.ServiceCertificate.DnsIdentity;
                    element.Security.ServiceCertificate.Validate = this.ServiceCertificate.Validate;

                    break;
                //case CredentialType.Certificate:
                //    settingsElement.Security.ClientCertificate.Store = this.ClientCertificate.Store;
                //    settingsElement.Security.ClientCertificate.SubjectName = this.ClientCertificate.SubjectName;
                //    break;
                case CredentialType.None:
                    //set elements to null to prevent saving un-necessary config to file
                    element.Security = null;
                    break;
            }
        }

        /// <summary>
        /// Loads the configuration.
        /// </summary>
        /// <param name="element">The element to load configuration from.</param>
        /// <returns></returns>
        IConfigurationSettings<ClientElement> IConfigurationSettings<ClientElement>.LoadConfiguration(ClientElement element)
        {
            if (element == null)
                throw new ArgumentNullException("Invalid configuration element. Cannot load ConnectionSettings from " + typeof(ClientElement).FullName);

            ClientElement settingsElement = element; //todoFix section.ConnectionSettings;

            this.Host = settingsElement.ServiceHost;
            this.ServiceCertificate.DnsIdentity = this.Host; // set it to the same value initially.
            this.Port = settingsElement.ServicePort;
            this.IsInteropClient = settingsElement.IsInteropClient;

            //if the security element is present:
            if (settingsElement.Security.ElementInformation.IsPresent)
            {
                this.CredentialType = settingsElement.Security.CredentialType;
                switch (this.CredentialType)
                {
                    case CredentialType.Username:
                        //since the object is already instantiated, just set its properties
                        this.UsernameCredential.Username = settingsElement.Security.UserName.Name;
                        this.UsernameCredential.Password = settingsElement.Security.UserName.Password;

                        break;
                    //case CredentialType.Certificate:
                    //    this.ClientCertificate.Store = element.Security.ClientCertificate.Store;
                    //    this.ClientCertificate.SubjectName = element.Security.ClientCertificate.SubjectName;
                    //    break;
                }

                //get the DnsIdentity even if we don't validate it
                if (settingsElement.Security.ServiceCertificate.ElementInformation.IsPresent)
                {
                    this.ServiceCertificate.DnsIdentity = settingsElement.Security.ServiceCertificate.DnsIdentity;
                    this.ServiceCertificate.Validate = settingsElement.Security.ServiceCertificate.Validate;
                }
                else
                {
                    this.ServiceCertificate.DnsIdentity = this.Host; //set it to default to the same value as Host
                    this.ServiceCertificate.Validate = false;
                }
            }
            return this;
        }

        #endregion
    }

    //[TypeConverter(typeof(PropertySorterConverter))]
    /// <summary>
    /// Represents a username credential used when connecting to a service.
    /// </summary>
    public class UsernameCredential
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UsernameCredential"/> class.
        /// </summary>
        public UsernameCredential()
        {
        }

        internal UsernameCredential(UserNameElement element) : this()
        {
            //here we directly set the password in the field, 
            //since we could be passed in an empty element: we don't want to do validation here.
            //we don't care. since we would then have an empty credential.
            //it is upto the component that uses the credential to worry about that.
            //but ofcourse, if something explicity sets an empty/null user/password, we blow up.
            if (element != null)
            {
                this.username = element.Name ?? string.Empty;
                this.password = element.Password ?? string.Empty;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UsernameCredential"/> class.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        public UsernameCredential(string username, string password) : this()
        {
            this.Username = username;
            this.Password = password;
        }

        private string username = string.Empty;
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The username.</value>
        [Category("Connection settings")]
        [Description("The username used to authenticate to the Manager")]
        //[PropertySortOrderAttribute(0)]
        public string Username
        {
            get { return username; }
            set 
            {
                if (value == null || value.Trim().Length == 0)
                    throw new ArgumentException("User name cannot be null or empty");

                this.username = value;
            }
        }

        private string password = string.Empty;
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Category("Connection settings")]
        [Description("The password used to authenticate to the Manager")]
        //[PropertySortOrderAttribute(1)]
        [PasswordPropertyText(true)]
        public string Password
        {
            get { return password; }
            set 
            {
                if (value == null || value.Trim().Length == 0)
                    throw new ArgumentException("Password cannot be null or empty");
                
                password = value;
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.Username;
        }

        internal string GetValidationErrors()
        {
            StringBuilder errors = new StringBuilder();
            if (this.Username == null || this.Username.Trim().Length == 0)
                errors.AppendLine("User name cannot be null or empty");
            if (this.Password == null || this.Password.Trim().Length == 0)
                errors.AppendLine("Password cannot be null or empty");
            return errors.ToString();
        }
    }

    //[TypeConverter(typeof(PropertySorterConverter))]
    /// <summary>
    /// Represents the service certificate
    /// </summary>
    public class ServiceCertificate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceCertificate"/> class.
        /// </summary>
        public ServiceCertificate()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceCertificate"/> class.
        /// </summary>
        /// <param name="dnsIdentity">The DNS identity.</param>
        /// <param name="validate">if set to <c>true</c> [validate].</param>
        public ServiceCertificate(string dnsIdentity, bool validate)
        {
            if (dnsIdentity == null || dnsIdentity.Trim().Length == 0)
                throw new ArgumentException("DnsIdentity cannot be null or empty");

            this.dnsIdentity = dnsIdentity;
            this.validate = validate;
        }

        internal ServiceCertificate(ServiceCertificateElement element)
        {
            if (element != null)
            {
                this.dnsIdentity = element.DnsIdentity;
                this.validate = element.Validate;
            }
        }

        private string dnsIdentity = string.Empty;
        /// <summary>
        /// Gets or sets the DNS identity of the service.
        /// </summary>
        /// <value>The DNS identity.</value>
        [Category("Connection settings")]
        [Description("The dns identity expected from the Manager")]
        public string DnsIdentity
        {
            get { return dnsIdentity; }
            set { dnsIdentity = value; }
        }

        private bool validate = false;
        /// <summary>
        /// Gets or sets a value indicating whether to validate the <see cref="ServiceCertificate"/>.
        /// </summary>
        /// <value><c>true</c> if validate; otherwise, <c>false</c>.</value>
        [Category("Connection settings")]
        [Description("Specifies whether to validate the Manager certificate")]
        public bool Validate
        {
            get { return validate; }
            set { validate = value; }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return DnsIdentity;
        }

        internal string GetValidationErrors()
        {
            if (this.DnsIdentity == null || this.DnsIdentity.Trim().Length == 0)
                return "Dns identity cannot be null or empty";
            else
                return string.Empty;
        }
    }
}
