using System;
using System.IO;
using System.Threading;

using Utilify.Platform.Contract;
using Utilify.Platform.Executor.InfoProvider;

namespace Utilify.Platform.Executor
{
    internal class ResourceManager : PlatformServiceBase
    {
        private ConnectionSettings connectionSettings;

        private IResourceMonitor proxy;
        private ISystemInfoProvider systemInfoProvider;
        private IPerformanceInfoProvider performanceInfoProvider;

        private ExecutorConfiguration configuration;

        private const string ExecutorIdFileName = "executor.{0}.id";

        private ResourceManager()
        {
            systemInfoProvider = InfoProviderFactory.GetSystemInfoProvider();
            performanceInfoProvider = InfoProviderFactory.GetPerformanceInfoProvider();
        }

        //used for testing only
        internal ResourceManager(IResourceMonitor proxy) : this()
        {
            if (proxy == null)
                throw new ArgumentNullException("proxy");

            this.proxy = proxy;
        }

        internal ResourceManager(ConnectionSettings connectionSettings) : this()
        {
            if (connectionSettings == null)
                throw new ArgumentNullException("connectionSettings");

            this.connectionSettings = connectionSettings;
        }

        // Issue 044: Discuss. What happens when registration fails
        //todoLater: Move registration + discovery into a seperate module.
        private void Register()
        {
            logger.Debug("Registering Executor...");
            string endpointAddress = "Unknown";
            string managerHostName = connectionSettings.Host;

            this.proxy = ProxyFactory.CreateProxyInstance<IResourceMonitor>(connectionSettings);

#if !MONO
            IManagerProxy wcfProxy = proxy as IManagerProxy;
            if (wcfProxy != null)
            {
                if (wcfProxy.EndPoint != null && wcfProxy.EndPoint.Address != null)
                {
                    endpointAddress = wcfProxy.EndPoint.Address.ToString();
                }
                managerHostName = wcfProxy.EndPoint.ListenUri.DnsSafeHost;
            }
#endif

            // Try to read existing Id from file. If it doesnt exist use null          
            string executorId = GetIdFromFile(managerHostName);
            bool gettingNewId = string.IsNullOrEmpty(executorId);

            SystemInfo sysInfo = null;
            bool success = false;
            while (!success && !IsStopRequested)
            {
                if (sysInfo == null)
                {
                    sysInfo = systemInfoProvider.GetSystemInfo(executorId);
                    logger.Debug("Registering with SystemInfo:\n" + sysInfo);

                    if (sysInfo == null)
                        throw new ApplicationException("Could not get System Information from the local machine");
                }

                try
                {
                    // Register Executor with Manager using existing Id if available. Will return the registered Id.
                    executorId = proxy.Register(sysInfo);
                    // Check if the Id sent is the same as the one returned. If not, then the Manager forgot about us.
                    // Need to save our new/updated Id to file.
                    gettingNewId = true;
                    success = !string.IsNullOrEmpty(executorId);
                }
                catch (Utilify.Platform.CommunicationException e) 
                {
                    //just before going off to sleep - check if we've been asked to stop!
                    if (IsStopRequested)
                        break;

                    int backOffInterval = BackOff.GetNextInterval();
                    logger.Warn("Could not register with Manager {0}. Backing off for {1}. {2}. ",
                        endpointAddress, Helper.GetFormattedTimeSpan(backOffInterval), e.ToString());

                    Thread.Sleep(backOffInterval);
                }
                catch (InternalServerException ix) //insulate exctor from the Manager's exceptions
                {
                    //just before going off to sleep - check if we've been asked to stop!
                    if (IsStopRequested)
                        break;

                    logger.Warn("Could not register with Manager. Server exception: ", ix);

                    Thread.Sleep(BackOff.GetNextInterval()); //back off for a bit
                }
            }

            //make sure we have an executor id, if we were NOT stopped by a StopRequest.
            if (string.IsNullOrEmpty(executorId) && IsStopRequested)
                return;
            else if (string.IsNullOrEmpty(executorId))
                throw new ApplicationException("Could not get executor id. Registration failed.");

            // Create the Configuration and set the Registered Id
            this.configuration = new ExecutorConfiguration(executorId, sysInfo);

            logger.Debug(string.Format("Executor registered with Id: {0}", executorId));

            if (gettingNewId)
                SaveIdToFile(executorId, managerHostName);
            //proxy.UpdateLimits(GetLimitInfo());

            //Set the config object and signal that the registration is complete
            InternalState.ExecutorConfiguration = this.configuration;
            
            //Create and set the JobManager proxy
            IJobManager jmProxy = ProxyFactory.CreateProxyInstance<IJobManager>(connectionSettings);
            InternalState.JobManager = jmProxy;

            //Create and set the DataTransferService proxy
            IDataTransferService dtProxy = ProxyFactory.CreateProxyInstance<IDataTransferService>(connectionSettings);
            InternalState.DataTransferService = dtProxy;

            InternalState.WaitHandle.Set();
        }

        private string executorIdFilePath = null;
        private string ExecutorIdFilePath
        {
            get
            {
                if (executorIdFilePath == null)
                    executorIdFilePath = Path.Combine(IOHelper.GetDataDirectory(true), ExecutorIdFileName);
                return executorIdFilePath;
            }
        }

        private string GetIdFromFile(string managerHostName)
        {
            string executorId = string.Empty;            
            try
            {
                //to make sure we have a string safe to use as a file path hash it:
                string hostnameHash = Hasher.ComputeHash(managerHostName, HashType.MD5);
                string filePath = string.Format(ExecutorIdFilePath, hostnameHash);
                if (File.Exists(filePath))
                {
                    //found it!
                    using (FileStream fs = new FileStream(filePath, FileMode.Open,
                        FileAccess.Read, FileShare.None))
                    {
                        using (StreamReader sr = new StreamReader(fs))
                        {
                            executorId = sr.ReadLine(); //read only the first line
                        }
                    }
                    logger.Debug("Read Executor Id {0} from file {1}.", executorId, filePath);
                }
                else
                {
                    logger.Info("Could not find existing executor Id at {0}, need to get a new Id from Manager", filePath);
                }
            }
            catch (IOException e)
            {
                logger.Warn("Failed to access existing executor Id, need to get a new Id from Manager : " + e.Message);
            }
            return executorId;
        }

        private void SaveIdToFile(string executorId, string managerHostName)
        {
            try
            {
                //to make sure we have a string safe to use as a file path hash it:
                string hostnameHash = Hasher.ComputeHash(managerHostName, HashType.MD5);
                string filePath = string.Format(ExecutorIdFilePath, hostnameHash);
                FileMode mode = FileMode.OpenOrCreate;
                if (File.Exists(filePath))
                    mode = FileMode.Truncate;
                
                using (FileStream fs = new FileStream(filePath, mode, FileAccess.Write, FileShare.None))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine(executorId);
                    }
                }

                logger.Debug("Wrote Executor Id {0} to file {1}.", executorId, filePath);
            }
            catch (IOException e)
            {
                logger.Warn("Failed to write the Executor Id to file.", e.Message);
            }
        }

        protected override void Run() 
        {
            try
            {
                // Need to create the executor configuration here so that we can make it available to
                // the containing class before starting the service.
                // Might be better to register in the Run() but want to gaurantee the executor configuration is set before returning from here
                Register();

                PerformanceInfo perfInfo = null;
                // Start thread to continually ping manager and give status updates with 'dynamic' PerformanceInfo
                while (!IsStopRequested)
                {
                    //logger.Debug("Pinging...");
                    //proxy.Ping(this.name);

                    perfInfo = performanceInfoProvider.GetPerformanceInfo(this.configuration.Id);
                    try
                    {
                        logger.Debug("Updating Performance Info...");
                        this.configuration.UpdatePerformance(perfInfo);
                        proxy.UpdatePerformance(perfInfo);
                        // MAGIC NUMBER
                        Thread.Sleep(4000);
                    }
                    catch (Utilify.Platform.CommunicationException e)
                    {
                        int backOffInterval = BackOff.GetNextInterval();
                        logger.Debug(string.Format("Could not update performance info. Backing off for {0}. {1}", 
                            Helper.GetFormattedTimeSpan(backOffInterval), e));
                        Thread.Sleep(backOffInterval);
                    }
                    catch (InternalServerException ix) //insulate exctor from the Manager's exceptions
                    {
                        logger.Warn("Server exception: ", ix);
                        Thread.Sleep(BackOff.GetNextInterval()); //back off for a bit
                    }
                }
            }
            catch (Exception e)
            {
                OnError(e, true);
            }
        }

        protected internal override string Name
        {
            get { return "ResourceManager"; }
        }
    }
}
