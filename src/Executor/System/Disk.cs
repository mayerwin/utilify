using System;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor
{
    internal class Disk
    {
        private string root; // OperatingSystem's name/id for the root used for linking by performance and limit infos
        private FileSystemType fileSystem;
        private long sizeTotal; //(in bytes)
        private long sizeTotalFree; //(in bytes)
        private long sizeUsageCurrent; //(in bytes)
        private long sizeUsageLimit; //(in bytes)

        private Disk() { }

        public Disk(string root, FileSystemType fileSystem, long sizeTotal, long sizeTotalFree, long sizeUsageCurrent, long sizeUsageLimit)
        {
            this.root = root;
            this.fileSystem = fileSystem;
            this.sizeTotal = sizeTotal;
            this.sizeTotalFree = sizeTotalFree;
            this.sizeUsageCurrent = sizeUsageCurrent;
            this.sizeUsageLimit = sizeUsageLimit;
        }

        public Disk(DiskSystemInfo info)
        {
            this.root = info.Root;
            this.fileSystem = info.FileSystem;
            this.sizeTotal = info.SizeTotal;
        }

        public void UpdatePerformance(DiskPerformanceInfo info)
        {
            if (!info.Root.Equals(this.root))
                throw new ArgumentException("Specified Root does not match the current Root. Cannot update.", "info.Root");

            this.sizeTotalFree = info.SizeTotalFree;
            this.sizeUsageCurrent = info.SizeUsageCurrent;
        }

        public void UpdateLimits(DiskLimitInfo info)
        {
            if (!info.Root.Equals(this.root))
                throw new ArgumentException("Specified Root does not match the current Root. Cannot update.", "info.Root");

            this.sizeUsageLimit = info.SizeUsageLimit;
        }

        public string Root
        {
            get { return root; }
        }

        public FileSystemType FileSystem
        {
            get { return fileSystem; }
        }

        public long SizeTotal
        {
            get { return sizeTotal; }
        }

        public long SizeTotalFree
        {
            get { return sizeTotalFree; }
        }

        public long SizeUsageCurrent
        {
            get { return sizeUsageCurrent; }
        }

        public long SizeUsageLimit
        {
            get { return sizeUsageLimit; }
        }

        public DiskSystemInfo ToDiskSystemInfo()
        {
            return new DiskSystemInfo(this.root, this.fileSystem, this.sizeTotal);
        }

        public DiskPerformanceInfo ToDiskPerformanceInfo()
        {
            return new DiskPerformanceInfo(this.root, this.sizeTotalFree, this.sizeUsageCurrent);
        }

        public DiskLimitInfo ToDiskLimitInfo()
        {
            return new DiskLimitInfo(this.root, this.sizeUsageLimit);
        }

        public override string ToString()
        {
            return string.Format("{0} {1} : {2} bytes Total, {3} bytes Total Free : Currently Using {4} bytes, {5} bytes Usable/Limit",
                fileSystem, root, sizeTotal, sizeTotalFree, sizeUsageCurrent, sizeUsageLimit);
        }

        //kna: changed this to use Enum.Parse in place of this
        //public static FileSystemType ParseFileSystemType(String value)
        //{
        //    FileSystemType type = FileSystemType.Unknown;
        //    value = value.ToUpperInvariant();

        //    switch (value)
        //    {
        //        case "FAT16":
        //            type = FileSystemType.Fat16;
        //            break;
        //        case "FAT32":
        //            type = FileSystemType.Fat32;
        //            break;
        //        case "NTFS":
        //            type = FileSystemType.Ntfs;
        //            break;
        //        case "EXT2":
        //            type = FileSystemType.Ext2;
        //            break;
        //        case "EXT3":
        //            type = FileSystemType.Ext3;
        //            break;
        //        default:
        //            type = FileSystemType.Unknown;
        //            break;
        //    }

        //    return type;
        //}
    }
}
