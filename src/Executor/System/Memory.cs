using System;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Executor
{
    internal class Memory
    {
        private long sizeTotal; //(in bytes)
        private long sizeTotalFree; //(in bytes)
        // These values could either measure the memory just for the executor process, or for the overall system.
        private long sizeUsageCurrent; //(in bytes)
        private long sizeUsageLimit; //(in bytes)

        private Memory() { }

        public Memory(long sizeTotal, long sizeTotalFree, long sizeUsageCurrent, long sizeUsageLimit)
        {
            this.sizeTotal = sizeTotal;
            this.sizeTotalFree = sizeTotalFree;
            this.sizeUsageCurrent = sizeUsageCurrent;
            this.sizeUsageLimit = sizeUsageLimit;
        }

        public Memory(MemorySystemInfo info)
        {
            this.sizeTotal = info.SizeTotal;
        }

        public void UpdatePerformance(MemoryPerformanceInfo info)
        {
            this.sizeTotalFree = info.SizeTotalFree;
            this.sizeUsageCurrent = info.SizeUsageCurrent;
        }

        public void UpdateLimits(MemoryLimitInfo info)
        {
            this.sizeUsageLimit = info.SizeUsageLimit;
        }

        /// <summary>
        /// Gets the total size of the memory module in bytes.
        /// </summary>
        public long SizeTotal
        {
            get { return sizeTotal; }
        }

        /// <summary>
        /// Gets the size of the free memory of the memory module in bytes.
        /// </summary>
        public long SizeTotalFree
        {
            get { return sizeTotalFree; }
        }

        /// <summary>
        /// Gets the size limit of usable memory for this memory module in bytes.
        /// </summary>
        public long SizeUsageCurrent
        {
            get { return sizeUsageCurrent; }
        }

        /// <summary>
        /// Gets the size limit of usable memory for this memory module in bytes.
        /// </summary>
        public long SizeUsageLimit
        {
            get { return sizeUsageLimit; }
        }

        public MemorySystemInfo ToMemorySystemInfo()
        {
            return new MemorySystemInfo(this.sizeTotal);
        }

        public MemoryPerformanceInfo ToMemoryPerformanceInfo()
        {
            return new MemoryPerformanceInfo(this.sizeTotalFree, this.sizeUsageCurrent);
        }

        public MemoryLimitInfo ToMemoryLimitInfo()
        {
            return new MemoryLimitInfo(this.sizeUsageLimit);
        }

        public override string ToString()
        {
            return string.Format("{0} bytes Total, {1} bytes Free, {2} bytes Used, {3} bytes Limit", sizeTotal, sizeTotalFree, sizeUsageCurrent, sizeUsageLimit);
        }
    }
}
