using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

using Utilify.Platform.Execution.Shared;

namespace Utilify.Platform.Executor
{
    internal class JobPreparer : PlatformServiceBase
    {
        private IJobManager proxy;
        private JobTable jobTable;
        private String executorId;
        private EventWaitHandle newJobEvent;
        // This table will need to get cleared out after some time otherwise it will cause memory problems.
        private Dictionary<String, ApplicationSubmissionInfo> applicationTable = new Dictionary<string,ApplicationSubmissionInfo>();
        private int pollInterval = 100;

        private Dictionary<DependencyType, IDependencyDownloader> downloaders = new Dictionary<DependencyType, IDependencyDownloader>();        

        internal JobPreparer(JobTable jobTable)
        {
            if (jobTable == null)
                throw new ArgumentNullException("jobTable");

            this.jobTable = jobTable;

            this.newJobEvent = new AutoResetEvent(true);

            this.BackOff = new BackOffHelper(3);
        }

        protected override void Run()
        {
            CheckValidState();

            logger.Debug("Running in JobPreparer...");
            while (true)
            {
                try
                {
                    //logger.Debug("Waiting for event or until pollinterval expires");
                    newJobEvent.WaitOne(pollInterval, false);

                    if (IsStopRequested)
                        break;

                    // Note: We never have problems getting jobs here even if the Manager suddenly goes offline (because we get from the JobTable
                    // not the Manager). But there is a problem where a job that was being prepared is interrupted mid-way and we jump out to here 
                    // without finishing.. the job will be lost and left hanging around in the Preparing state. So since this thread processes jobs 
                    // sequentially, we should be able to pull out Preparing jobs and either let them start again, or set them to Stopped and send 
                    // them back.

                    // todoLater: Put some flags inside the Job that indicate which steps have been completed successfully
                    // so we can restart from there.
                    Job[] problemJobs = jobTable.GetJobs(ExecutionStatus.Preparing);
                    foreach (Job pj in problemJobs)
                    {
                        pj.FailureCount++;
                        if (pj.FailureCount >= 3)
                        {
                            // This Job failed too many times, just send it back.
                            pj.ExecutionError.AppendLine("Errors occurred while preparing Job for execution. Retry attempts failed. Setting to stopped.");
                            jobTable.ChangeStatus(pj, ExecutionStatus.Stopped);
                        }
                        else
                        {
                            // There was a problem, but lets give the Job another chance.
                            jobTable.ChangeStatus(pj, ExecutionStatus.New);
                        }
                    }

                    // Get all New Jobs ready to be prepared.
                    Job[] jobs = jobTable.GetJobs(ExecutionStatus.New);
                    
                    if (jobs.Length > 0)
                        logger.Info("Got {0} Jobs to Prepare.", jobs.Length);                    

                    foreach (Job job in jobs)
                    {
                        bool errorPreparing = false;
                        try
                        {
                            // Check if we should continue
                            if (job.IsCancelled)
                                continue;

                            // Tell everyone that this Job is being prepared.
                            logger.Debug(String.Format("Updating Job {0} status to 'Preparing'.", job.JobId));
                            jobTable.ChangeStatus(job, ExecutionStatus.Preparing);

                            // Prepare environment for executing job
                            logger.Debug(String.Format("Preparing Job {0}.", job.JobId));
                            job.ExecutionLog.AppendLine("Preparing environment for Job");

                            // Create Job Directory (will also create the app directory if it doesnt already exist)
                            try
                            {
                                logger.Debug(String.Format("Creating Directory {0}", job.JobDirectory));
                                Directory.CreateDirectory(job.JobDirectory);
                            }
                            catch (IOException ioex)
                            {
                                errorPreparing = true;
                                logger.Warn("An I/O Error occurred while preparing Job for execution.", ioex);
                            }

                            if (!errorPreparing && TryVerifyApplication(job))
                            {
                                logger.Debug("Application Verified, now continuing with Job");

                                bool gotDependencies = TryVerifyDependencies(job.GetDependencies(), job, false);

                                // Check if we should continue
                                if (job.IsCancelled)
                                    continue;

                                if (gotDependencies && TryCopyRuntimeAssemblies(job))
                                {
                                    // After everything has been prepared change the jobs status.
                                    logger.Debug(String.Format("Updating Job {0} status to 'Ready'.", job.JobId));
                                    jobTable.ChangeStatus(job, ExecutionStatus.Ready);
                                }
                                else
                                {
                                    errorPreparing = true;
                                }
                            }

                            if (errorPreparing)
                            {
                                job.ExecutionError.AppendLine("An error occurred while preparing Job for execution. Setting to stopped.");
                                jobTable.ChangeStatus(job, ExecutionStatus.Stopped);
                            }
                        }
                        catch (InvalidTransitionException itex)
                        {
                            // if the invalid transition wasnt caused by a cancelled job, then there is a
                            // problem with the system state. rethrow exception to kill executor (or just skip job?).
                            if (jobTable.GetStatus(job) != ExecutionStatus.Cancelled)
                                throw itex;
                        }
                    }
                }
                catch (InternalServerException) //insulate exctor from the Manager's exceptions
                {
                    //the proxy base will reset the proxy, and create a new communication channel for use in the next round
                    if (BackOff.NumberOfRetries > BackOff.MaxRetries)
                        break;

                    int backOffInterval = BackOff.GetNextInterval();
                    logger.Warn("Server exception. Backing off for {0} ms.", Helper.GetFormattedTimeSpan(backOffInterval));
                    Thread.Sleep(backOffInterval);
                    logger.Debug("Woke up again...");
                }
                catch (Exception e)
                {
                    OnError(e, true);
                    break;
                }
            } // while 
        }

        private void CheckValidState()
        {
            if (!string.IsNullOrEmpty(this.executorId))
                return;

            //get executor info from the internal state
            InternalState.WaitHandle.WaitOne();

            //if we still don't have a valid config, blow up:
            ExecutorConfiguration config = InternalState.ExecutorConfiguration;
            if (config != null)
                this.executorId = config.Id;

            if (string.IsNullOrEmpty(this.executorId))
                throw new InvalidOperationException("Executor configuration could not be retrieved");

            //now get the proxy 
            this.proxy = InternalState.JobManager;
        }

        /// <summary>
        /// Verifies if we have the application submission info and application-level dependencies for this job:
        /// If not, gets them from the Manager.
        /// </summary>
        /// <param name="job"></param>
        private bool TryVerifyApplication(Job job)
        {
            bool isVerified = false;
            // If this is the first time we are seeing this application, download the dependencies.
            ApplicationSubmissionInfo app;
            if (!applicationTable.TryGetValue(job.ApplicationId, out app))
            {
                logger.Debug("First time we have seen Application");

                job.ExecutionLog.AppendLine("Downloading Application submission info for Job");
                bool gotApplication = false;
                while (!gotApplication)
                {
                    try
                    {
                        // Check if we should continue
                        if (job.IsCancelled)
                            break;

                        app = proxy.GetApplication(executorId, job.ApplicationId);
                        // todo: do better validation of the application here
                        if (app != null)
                            gotApplication = true;
                    }
                    catch (Utilify.Platform.CommunicationException)
                    {
                        //the proxy base will reset the proxy, and create a new communication channel for use in the next round
                        if (BackOff.NumberOfRetries > BackOff.MaxRetries)
                            break;

                        int backOffInterval = BackOff.GetNextInterval();
                        logger.Warn("Failed to retrieve the Application {0}. Backing off for {1} ms.",
                            job.ApplicationId, Helper.GetFormattedTimeSpan(backOffInterval));
                        Thread.Sleep(backOffInterval);
                    }
                }
                logger.Debug(String.Format("Successfully retrieved Application {0}.", app.Id));

                if (gotApplication)
                {
                    job.ExecutionLog.AppendLine("Downloading Application Dependencies");
                    // If we got the apps' dependencies, then add the Application to the list of those that we've seen before
                    if (TryVerifyDependencies(app.GetDependencies(), job, true))
                        applicationTable.Add(job.ApplicationId, app);
                    else
                        gotApplication = false;
                }
                isVerified = gotApplication;
            }
            else
            {
                isVerified = (app != null);
            }
            return isVerified;
        }

        private bool TryCopyRuntimeAssemblies(Job job)
        {
            bool copied = false;

            try
            {
                String destinationPath = job.ApplicationDirectory;
                String src = null;
                String dest = null;
                //we assume that if the current dll is GAC-ed all other ones are GAC-ed too.
                //because we don't want to load the assembly #3 (IExecutor implementations) into this AppDomain.
                bool isGACed = this.GetType().Assembly.GlobalAssemblyCache;
                if (!isGACed)
                {
                    //1. Copy the IExecutable Assembly i.e the Framework
                    src = typeof(Utilify.Framework.IExecutable).Assembly.Location;
                    dest = Path.Combine(destinationPath, Path.GetFileName(src));
                    if (!File.Exists(dest))
                    {
                        logger.Debug("Copying runtime assembly {0} to {1}", src, dest);
                        File.Copy(src, dest);
                    }

                    //2. Copy the IExecutor Assembly
                    src = typeof(IExecutor).Assembly.Location;
                    dest = Path.Combine(destinationPath, Path.GetFileName(src));
                    if (!File.Exists(dest))
                    {
                        logger.Debug("Copying runtime assembly {0} to {1}", src, dest);
                        File.Copy(src, dest);
                    }

                    //3. Copy the Execution dll (which contains the IExecutor implementations)
                    //string executionDllBaseDir = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
                    //src = Path.Combine(executionDllBaseDir, ExecutionHelper.ExecutionAssemblyFileName);

                    // todoDiscuss: Verify that this change is ok. Since all DLLs are in the same directory, just use the IExecutor path to
                    //       find this DLL. Without this the tests fail because of the AppDomain base directory in nunit.
                    src = Path.Combine(Path.GetDirectoryName(typeof(IExecutor).Assembly.Location), ExecutionHelper.ExecutionAssemblyFileName); ;
                    dest = Path.Combine(destinationPath, Path.GetFileName(src));
                    if (!File.Exists(dest))
                    {
                        logger.Debug("Copying runtime assembly {0} to {1}", src, dest);
                        File.Copy(src, dest);
                    }
                }

                copied = true;
            }
            catch (IOException ioex)
            {
                logger.Warn("Error copying runtime assemblies: " + ioex);
            }
            return copied;
        }

        //we need to somehow pass the error back in the job log.
        private bool TryVerifyDependencies(DependencyInfo[] dependencies, Job job, bool forApplication)
        {
            bool gotAllDeps = true;
            
            if (dependencies == null)
                return true;

            job.ExecutionLog.AppendLine("Verifying job Dependencies");
            logger.Debug("Verifying {0} dependencies for {1}", dependencies.Length, ((forApplication) ? "app" : "job"));
            foreach (DependencyInfo dependency in dependencies)
            {
                //figure out if this dependency is already in the destination path:
                string path = job.JobDirectory;
                if (forApplication || IsApplicationLevelDependency(dependency.Type))
                    path = job.ApplicationDirectory; //if its an app dependency, put it there.

                string depFileName = dependency.Filename;
                if (dependency.Type == DependencyType.RemoteUrl)
                {
                    try
                    {
                        Uri uri = new Uri(depFileName);
                        //todoLater: if/when we support sub-directories on the Executor, we'll need all this code below:
                        //depFileName = uri.LocalPath.Replace('/', Path.DirectorySeparatorChar);
                        //fix up the file name
                        //if (depFileName.StartsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.CurrentCultureIgnoreCase))
                        //{
                        //    depFileName = depFileName.Substring(1); //leave out the first character
                        //}

                        //for now, we don't allow sub-directories
                        depFileName = Path.GetFileName(uri.LocalPath);
                    }
                    catch (UriFormatException ufx)
                    {
                        job.ExecutionError.AppendLine("Invalid uri format: " + depFileName);
                        logger.Warn("Invalid uri format for remote dependency.", ufx);
                        return false;
                    }
                }
                
                //logger.Debug("**** path = {0}, depFileName = {1}", path, depFileName);

                string fullPath = Path.Combine(path, depFileName);
                
                //logger.Debug(String.Format("File {1} exists ? : [{0}]", File.Exists(fullPath), fullPath));
                
                if (File.Exists(fullPath))
                    continue; //move on to the next one

                //todoLater: check the local dependency cache: at the moment it only checks in the current job working dir :/

                // Check if we should continue to download:
                if (job.IsCancelled)
                    break;

                if (!TryDownloadDependency(job, dependency, fullPath))
                {
                    gotAllDeps = false;
                    break; //no point trying to get other dependencies, since the job may not run anyway.
                }
            }

            return gotAllDeps;
        }

        // Download dependency
        // Issue 021: How to protect against executors pulling jobs/dependencies they shouldnt have access to?
        // DONE: Issue 061: Check if this dependency has already been downloaded into the local dependency cache.
        private bool TryDownloadDependency(Job job, DependencyInfo dependency, string destinationPath)
        {
            bool gotDependency = false;

            IDependencyDownloader downloader = GetDependencyDownloader(dependency.Type);

            while (!gotDependency)
            {
                try
                {
                    // Check if we should continue
                    if (job.IsCancelled)
                        break;

                    gotDependency = downloader.GetDependency(dependency, destinationPath);

                }
                catch (Utilify.Platform.DependencyNotFoundException)
                {
                    logger.Warn("Failed to retrieve the Dependency {0} {1}. Dependency not found.",
                        dependency.Id, dependency.Name);

                    break;
                }
                catch (Utilify.Platform.CommunicationException cx)
                {
                    if (BackOff.NumberOfRetries > BackOff.MaxRetries)
                        break;

                    int backOffInterval = BackOff.GetNextInterval();
                    logger.Warn("Failed to retrieve the Dependency {0} {1} : {3}. Backing off for {2}.",
                        dependency.Id, dependency.Name, Helper.GetFormattedTimeSpan(backOffInterval),
                        cx.ToString());
                    Thread.Sleep(backOffInterval);
                }
                catch (System.UnauthorizedAccessException ux)
                {
                    logger.Warn("Could not access dependency: ", ux);
                    break;
                }
                catch (IOException iox)
                {
                    logger.Warn("Could not download dependency file to " + destinationPath, iox);
                    break;
                }

            }

            return gotDependency;
        }

        private IDependencyDownloader GetDependencyDownloader(DependencyType dependencyType)
        {
            IDependencyDownloader downloader = null;
            if (downloaders.ContainsKey(dependencyType))
            {
                downloader = downloaders[dependencyType];
            }
            else 
            {
                switch (dependencyType)
                {
                    case DependencyType.DataFile:
                    case DependencyType.ClrModule:
                    case DependencyType.JavaModule:
                    case DependencyType.NativeModule:
                        downloader = new SimpleDependencyDownloader(this.proxy);
                        break;
                    case DependencyType.RemoteUrl:
                        downloader = new RemoteDependencyDownloader(InternalState.DataTransferService);
                        break;
                }

                downloaders[dependencyType] = downloader;
            }

            return downloader;            
        }

        private bool IsApplicationLevelDependency(DependencyType type)
        {
            if (type == DependencyType.ClrModule
                    || type == DependencyType.JavaModule
                    // || type == DependencyType.NativeModule // The native module is not a library that we are loading into an appdomain or classloader so it shouldnt be in the app directory.
                )
                return true;
            else
                return false;
        }

        internal void OnExecutionStatusChanged(object sender, ExecutionStatusEventArgs e)
        {
            switch (e.Status)
            {
                case ExecutionStatus.New:
                    newJobEvent.Set();
                    break;
                case ExecutionStatus.Cancelled:
                    // Cancel job.
                    break;
            }
        }

        protected internal override string Name
        {
            get { return "JobPreparer"; }
        }
    }
}
