package com.utilify.framework.client.tests;

import java.util.UUID;

import org.junit.Test;

import com.utilify.framework.client.DependencyContent;
import com.utilify.framework.tests.TestHelper;

public class DependencyContentTest {

	/*
		[Category("DependencyContent")]
	 */
	@Test
	public void DependencyContentCtor() {
		String id = UUID.randomUUID().toString();
		new DependencyContent(id, TestHelper.getSomeBytes());
		// no assertions, just to make sure the proper ctor works
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyContentNullDependencyId() {
		new DependencyContent(null, TestHelper.getSomeBytes());
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyContentEmptyDependencyId() {
		new DependencyContent(" ", TestHelper.getSomeBytes());
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyContentNullContents() {
		new DependencyContent("SomeDepId", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyContentEmptyContents() {
		new DependencyContent("SomeDepId", new byte[0]);
	}
}