package com.utilify.framework.client.tests;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.utilify.framework.DependencyScope;
import com.utilify.framework.client.DependencyQueryInfo;

public class DependencyQueryInfoTest  {

	/*
		[Category("DependencyQueryInfo")]
	*/
	
	@Test(expected = IllegalArgumentException.class)
	public void createDependencyQueryInfoNullId() {
		new DependencyQueryInfo(null, DependencyScope.APPLICATION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyQueryInfoEmptyId() {
		new DependencyQueryInfo(" ", DependencyScope.JOB);
		// doesnt really matter what the scope is: we expect the exception
	}

	@Test
	public void DependencyQueryInfoCtor() {
		String id = UUID.randomUUID().toString();
		DependencyQueryInfo query = new DependencyQueryInfo(id, DependencyScope.JOB);
		// doesnt really matter what the scope is : but the id shouldnt be null
		Assert.assertNotNull("The assigned app or job id should not be null", query.getApplicationOrJobId());
	}
}