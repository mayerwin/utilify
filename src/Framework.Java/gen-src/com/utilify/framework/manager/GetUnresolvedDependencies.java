
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.DependencyQueryInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="query" type="{http://schemas.utilify.com/2007/09/Client}DependencyQueryInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "query"
})
@XmlRootElement(name = "GetUnresolvedDependencies")
public class GetUnresolvedDependencies {

    @XmlElement(nillable = true)
    protected DependencyQueryInfo query;

    /**
     * Gets the value of the query property.
     * 
     * @return
     *     possible object is
     *     {@link DependencyQueryInfo }
     *     
     */
    public DependencyQueryInfo getQuery() {
        return query;
    }

    /**
     * Sets the value of the query property.
     * 
     * @param value
     *     allowed object is
     *     {@link DependencyQueryInfo }
     *     
     */
    public void setQuery(DependencyQueryInfo value) {
        this.query = value;
    }

}
