
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.JobCompletionInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCompletedJobResult" type="{http://schemas.utilify.com/2007/09/Client}JobCompletionInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCompletedJobResult"
})
@XmlRootElement(name = "GetCompletedJobResponse")
public class GetCompletedJobResponse {

    @XmlElement(name = "GetCompletedJobResult", nillable = true)
    protected JobCompletionInfo getCompletedJobResult;

    /**
     * Gets the value of the getCompletedJobResult property.
     * 
     * @return
     *     possible object is
     *     {@link JobCompletionInfo }
     *     
     */
    public JobCompletionInfo getGetCompletedJobResult() {
        return getCompletedJobResult;
    }

    /**
     * Sets the value of the getCompletedJobResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JobCompletionInfo }
     *     
     */
    public void setGetCompletedJobResult(JobCompletionInfo value) {
        this.getCompletedJobResult = value;
    }

}
