
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.ArrayOfJobStatusInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetJobStatusResult" type="{http://schemas.utilify.com/2007/09/Client}ArrayOfJobStatusInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getJobStatusResult"
})
@XmlRootElement(name = "GetJobStatusResponse")
public class GetJobStatusResponse {

    @XmlElement(name = "GetJobStatusResult", nillable = true)
    protected ArrayOfJobStatusInfo getJobStatusResult;

    /**
     * Gets the value of the getJobStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfJobStatusInfo }
     *     
     */
    public ArrayOfJobStatusInfo getGetJobStatusResult() {
        return getJobStatusResult;
    }

    /**
     * Sets the value of the getJobStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfJobStatusInfo }
     *     
     */
    public void setGetJobStatusResult(ArrayOfJobStatusInfo value) {
        this.getJobStatusResult = value;
    }

}
