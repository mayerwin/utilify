
package com.utilify.framework.manager;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.utilify.framework.EntityNotFoundException;
import com.utilify.framework.InvalidArgumentException;
import com.utilify.framework.InvalidOperationException;
import com.utilify.framework.Namespaces;
import com.utilify.framework.client.ApplicationStatusInfo;
import com.utilify.framework.client.ApplicationSubmissionInfo;
import com.utilify.framework.client.ArrayOfDependencyInfo;
import com.utilify.framework.client.ArrayOfJobStatusInfo;
import com.utilify.framework.client.ArrayOfJobSubmissionInfo;
import com.utilify.framework.client.ArrayOfResultStatusInfo;
import com.utilify.framework.client.DependencyContent;
import com.utilify.framework.client.DependencyQueryInfo;
import com.utilify.framework.client.JobCompletionInfo;
import com.utilify.framework.client.JobSubmissionInfo;
import com.utilify.framework.client.ResultContent;

/**
 * The <code>IApplicationManager</code> is the remote interface of the ApplicationManager service 
 * that provides access to the Utilify distributed execution platform. It exposes the Application Manager 
 * web service and includes methods to create, submit, and monitor applications, and jobs.
 */
@WebService(name = "IApplicationManager", targetNamespace = Namespaces.Manager)
public interface IApplicationManager {

    /**
     * Requests the ApplicationManager to abort the application with the specified Id.
     * @param applicationId
     * 	the Id of the application to abort.
     * @throws InvalidArgumentException 
     * 	if <code>applicationId</code> is <code>null</code> or empty.
     * @throws EntityNotFoundException
     * 	if an application with the specified Id was not found.
     * @throws InvalidOperationException
     * 	if the application is not yet initialized or if it is already stopped.
     */
    @WebMethod(operationName = "AbortApplication", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/AbortApplication")
    @RequestWrapper(localName = "AbortApplication", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.AbortApplication")
    @ResponseWrapper(localName = "AbortApplicationResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.AbortApplicationResponse")
    public void abortApplication(
        @WebParam(name = "applicationId", targetNamespace = Namespaces.Manager)
        String applicationId)
        throws InvalidArgumentException, EntityNotFoundException, InvalidOperationException
    ;
    
    /**
     * Requests the ApplicationManager to abort the job with the specified Id.
     * @param jobId
     * 	the Id of the job to cancel.
     * @throws EntityNotFoundException
     * 	if a job with the specified Id was not found.
     * @throws InvalidOperationException
     * 	if the job is already completed or cancelled.
     * @throws InvalidArgumentException
     * 	if the <code>jobId</code> is null or empty. 
     */
    @WebMethod(operationName = "AbortJob", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/AbortJob")
    @RequestWrapper(localName = "AbortJob", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.AbortJob")
    @ResponseWrapper(localName = "AbortJobResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.AbortJobResponse")
    public void abortJob(
        @WebParam(name = "jobId", targetNamespace = Namespaces.Manager)
        String jobId)
    	throws InvalidArgumentException, EntityNotFoundException, InvalidOperationException
    ;
    
    /**
     * Retrieves a list of unresolved dependencies of the entity matching the specified query.
     * The entity can be either an application or a job.
     * @param query
     * 	the query information to match an application or a job.
     * @return 
     * 	an instance of {@link ArrayOfDependencyInfo} that wraps the list of dependencies.
     * @throws EntityNotFoundException
     * 	if no entity matches the specified query.
     * @throws InvalidArgumentException 
     * 	if <code>query</code> or one of its parameters is <code>null</code>. 
     */
    @WebMethod(operationName = "GetUnresolvedDependencies", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/GetUnresolvedDependencies")
    @WebResult(name = "GetUnresolvedDependenciesResult", targetNamespace = Namespaces.Manager)
    @RequestWrapper(localName = "GetUnresolvedDependencies", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetUnresolvedDependencies")
    @ResponseWrapper(localName = "GetUnresolvedDependenciesResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetUnresolvedDependenciesResponse")
    public ArrayOfDependencyInfo getUnresolvedDependencies(
        @WebParam(name = "query", targetNamespace = Namespaces.Manager)
        DependencyQueryInfo query)
        throws InvalidArgumentException, EntityNotFoundException
    ;

    /**
     * Requests the ApplicationManager to pause the application with the specified Id.
     * @param applicationId
     * 	the Id of the application to pause.
     * @throws InvalidOperationException
     * 	if the application is not yet initialised, already paused, or already stopped.
     * @throws EntityNotFoundException
     * 	if an application with the specified was not found.
     * @throws InvalidArgumentException
     * 	if <code>applicationId</code> is <code>null</code> or empty.
     */
    @WebMethod(operationName = "PauseApplication", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/PauseApplication")
    @RequestWrapper(localName = "PauseApplication", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.PauseApplication")
    @ResponseWrapper(localName = "PauseApplicationResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.PauseApplicationResponse")
    public void pauseApplication(
        @WebParam(name = "applicationId", targetNamespace = Namespaces.Manager)
        String applicationId)
        throws InvalidArgumentException, EntityNotFoundException, InvalidOperationException
    ;

    /**
     * Gets the status of the specified application.
     * @param applicationId
     * 	the Id of the application.
     * @return
     *	an instance {@link ApplicationStatusInfo} containing the status information.
     * @throws EntityNotFoundException
     * 	if an application with the specified Id was not found.
     * @throws InvalidArgumentException
     * 	if <code>applicationId</code> is <code>null</code> or empty.
     */
    @WebMethod(operationName = "GetApplicationStatus", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/GetApplicationStatus")
    @WebResult(name = "GetApplicationStatusResult", targetNamespace = Namespaces.Manager)
    @RequestWrapper(localName = "GetApplicationStatus", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetApplicationStatus")
    @ResponseWrapper(localName = "GetApplicationStatusResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetApplicationStatusResponse")
    public ApplicationStatusInfo getApplicationStatus(
        @WebParam(name = "applicationId", targetNamespace = Namespaces.Manager)
        String applicationId)
        throws InvalidArgumentException, EntityNotFoundException
    ;

    /**
     * Gets the status of the jobs with the specified Ids.
     * @param jobIds
     * 	an instance of {@link ArrayOfstring} that wraps a list of string Ids.
     * @return
     * 	an instance of {@link ArrayOfJobStatusInfo} containing the job status information objects. 
     * @throws EntityNotFoundException
     * 	if one or more of the jobs whose Ids are specified could not be found. 
     * @throws InvalidArgumentException 
     * 	if <code>jobIds</code> is <code>null</code> or one of the Ids is <code>null</code> or empty.
     */
    @WebMethod(operationName = "GetJobStatus", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/GetJobStatus")
    @WebResult(name = "GetJobStatusResult", targetNamespace = Namespaces.Manager)
    @RequestWrapper(localName = "GetJobStatus", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetJobStatus")
    @ResponseWrapper(localName = "GetJobStatusResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetJobStatusResponse")
    public ArrayOfJobStatusInfo getJobStatus(
        @WebParam(name = "jobIds", targetNamespace = Namespaces.Manager)
        ArrayOfstring jobIds)
        throws InvalidArgumentException, EntityNotFoundException
    ;

    /**
     * Requests the ApplicationManager to resumes the (previously paused) application with the specified Id.
     * @param applicationId
     * 	the Id of the application to resume.
     * @throws InvalidOperationException
     * 	if the application is not paused.
     * @throws EntityNotFoundException
     * 	if an application with the specified was not found.
     * @throws InvalidArgumentException 
     * 	if <code>applicationId</code> is <code>null</code> or empty.
     */
    @WebMethod(operationName = "ResumeApplication", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/ResumeApplication")
    @RequestWrapper(localName = "ResumeApplication", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.ResumeApplication")
    @ResponseWrapper(localName = "ResumeApplicationResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.ResumeApplicationResponse")
    public void resumeApplication(
        @WebParam(name = "applicationId", targetNamespace = Namespaces.Manager)
        String applicationId)
        throws InvalidArgumentException, EntityNotFoundException, InvalidOperationException
    ;

    /**
     * Sends the specified contents for the specified dependency, to the ApplicationManager.
     * @param dependency
     * 	an instance of {@link DependencyContent} containing the dependency information and content data.
     * @throws InvalidOperationException
     * 	if the dependency content has already been resolved on (or previously sent to) the Manager.
     * @throws EntityNotFoundException
     * 	if a dependency with the specified information was not found.
     * @throws InvalidArgumentException 
     * 	if <code>content</code> is <code>null</code>.
     */
    @WebMethod(operationName = "SendDependency", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/SendDependency")
    @RequestWrapper(localName = "SendDependency", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.SendDependency")
    @ResponseWrapper(localName = "SendDependencyResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.SendDependencyResponse")
    public void sendDependency(
        @WebParam(name = "dependency", targetNamespace = Namespaces.Manager)
        DependencyContent dependency)
        throws InvalidArgumentException, EntityNotFoundException, InvalidOperationException
    ;

    /**
     * Submits an application with the specified submission information to the ApplicationManager.
     * @param appInfo
     * 	an instance of {@link ApplicationSubmissionInfo} containing the submission information.
     * @return
     * 	the Id of the newly submitted application.
     * @throws InvalidArgumentException 
     * 	if <code>appInfo</code> is <code>null</code> or one of its properties is invalid.
     */
    @WebMethod(operationName = "SubmitApplication", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/SubmitApplication")
    @WebResult(name = "SubmitApplicationResult", targetNamespace = Namespaces.Manager)
    @RequestWrapper(localName = "SubmitApplication", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.SubmitApplication")
    @ResponseWrapper(localName = "SubmitApplicationResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.SubmitApplicationResponse")
    @Action(input = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/SubmitApplication", output = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/SubmitApplicationResponse")
    public String submitApplication(
        @WebParam(name = "appInfo", targetNamespace = Namespaces.Manager)
        ApplicationSubmissionInfo appInfo)
    	throws InvalidArgumentException
    ;

    /**
     * Submits a set of jobs with the specified submission information to the ApplicationManager.
     * @param jobInfo
     * 	an instance of {@link ArrayOfJobSubmissionInfo} that wraps a list of {@link JobSubmissionInfo} containing 
     * the submission information for the jobs.
     * @return
     * 	an instance of {@link ArrayOfstring} containing the list of Ids of the newly submitted jobs. 
     * @throws InvalidArgumentException
     * 	if <code>jobInfo</code> or one of its elements is <code>null</code>.
     * @throws EntityNotFoundException
     * 	if an application with an Id specified in one of the <code>JobSubmissionInfo</code> instances is not found.
     */
    @WebMethod(operationName = "SubmitJobs", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/SubmitJobs")
    @WebResult(name = "SubmitJobsResult", targetNamespace = Namespaces.Manager)
    @RequestWrapper(localName = "SubmitJobs", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.SubmitJobs")
    @ResponseWrapper(localName = "SubmitJobsResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.SubmitJobsResponse")
    public ArrayOfstring submitJobs(
        @WebParam(name = "jobInfo", targetNamespace = Namespaces.Manager)
        ArrayOfJobSubmissionInfo jobInfo)
        throws InvalidArgumentException, EntityNotFoundException
    ;

    /**
     * Gets the completed job instance from the ApplicationManager.
	 * If the job has not finished execution, it returns <code>null</code>.
     * @param jobId
     * 	the Id of the job to get.
     * @return
     * 	an instance of {@link JobCompletionInfo} that contains the completion information and the job instance.
     * @throws InvalidArgumentException
     * 	if <code>jobId</code> is <code>null</code> or empty. 
     * @throws EntityNotFoundException
     * 	if a job with the specified Id was not found.
     */
    @WebMethod(operationName = "GetCompletedJob", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/GetCompletedJob")
    @WebResult(name = "GetCompletedJobResult", targetNamespace = "http://schemas.utilify.com/2007/09/Manager")
    @RequestWrapper(localName = "GetCompletedJob", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetCompletedJob")
    @ResponseWrapper(localName = "GetCompletedJobResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetCompletedJobResponse")
    public JobCompletionInfo getCompletedJob(
        @WebParam(name = "jobId", targetNamespace = Namespaces.Manager)
        String jobId)
        throws InvalidArgumentException, EntityNotFoundException
    ;
    
    /**
     * Gets the status of the results of the specfied job.
     * If the job has not finished execution yet, there would be no results and an empty list of results is returned.  
     * @param jobId
     * 	the Id of the job for which the result status is required.
     * @return
     * 	an instance of {@link ArrayOfResultStatusInfo} that contains the result status information.
     * @throws InvalidArgumentException
     * 	if <code>jobId</code> is <code>null</code> or empty.
     * @throws EntityNotFoundException
     * 	if a job with the specified Id was not found.
     */
    @WebMethod(operationName = "GetJobResults", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/GetJobResults")
    @WebResult(name = "GetJobResultsResult", targetNamespace = Namespaces.Manager)
    @RequestWrapper(localName = "GetJobResults", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetJobResults")
    @ResponseWrapper(localName = "GetJobResultsResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetJobResultsResponse")
    public ArrayOfResultStatusInfo getJobResults(
        @WebParam(name = "jobId", targetNamespace = Namespaces.Manager)
        String jobId)
        throws InvalidArgumentException, EntityNotFoundException
    ;

    /**
     * Retrieves the content of the result file with the specified Id.
     * If the result has not yet been produced because the job has not finished executing,
     * it returns an instance of <code>ResultContent</code> with <code>null</code> data contents.
     * @param resultId
     * 	the Id of the result for which content is to be retrieved.
     * @return
     * 	an instance of {@link ResultContent} containing the result data.
     * @throws InvalidArgumentException
     * 	if <code>resultId</code> is <code>null</code> or empty.
     * @throws EntityNotFoundException
     * 	if a result with the specified Id was not found.
     */
    @WebMethod(operationName = "GetResult", action = "http://schemas.utilify.com/2007/09/Manager/IApplicationManager/GetResult")
    @WebResult(name = "GetResultResult", targetNamespace = Namespaces.Manager)
    @RequestWrapper(localName = "GetResult", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetResult")
    @ResponseWrapper(localName = "GetResultResponse", targetNamespace = Namespaces.Manager, className = "com.utilify.framework.manager.GetResultResponse")
    public ResultContent getResult(
        @WebParam(name = "resultId", targetNamespace = Namespaces.Manager)
        String resultId)
        throws InvalidArgumentException, EntityNotFoundException
    ;

}
