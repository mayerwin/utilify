
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/*
 * <p>Java class for ApplicationStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApplicationStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UnInitialized"/>
 *     &lt;enumeration value="Submitted"/>
 *     &lt;enumeration value="Ready"/>
 *     &lt;enumeration value="Paused"/>
 *     &lt;enumeration value="Stopped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
/**
 * Represents the status of an application. At any time, an application can have one of the following 
 * statuses: <br>
 * <li>UnInitialized</li><br>
 * The application is yet to be initialized on the remote Manager. 
 * <li>Submitted</li><br>
 * The application is submitted to the Manager.
 * <li>Ready</li><br>
 * The application is ready to run. This means that the application's dependencies have been resolved and any Qos constraints have been met.
 * Only jobs that belong to a ready application are eligible to be scheduled to run on an Executor.
 * <li>Paused</li><br>
 * The application is paused on the Manager. A paused application will not execute any jobs. All jobs added to an application
 * after it is paused are queued on the Manager - to be run (if and) when it is resumed.
 * <li>Stopped</li><br>
 * The application is stopped. This happens when there is an explicit call to 'Abort' the application.
 * No jobs can be added / executed as part of a stopped application.
 */
@XmlType(name = "ApplicationStatus")
@XmlEnum
public enum ApplicationStatus {

    /**
     * Status for an un-initialized (i.e. newly instantiated) application.
     */
    @XmlEnumValue("UnInitialized")
    UNINITIALIZED("UnInitialized"),
    /**
     * Status for a submitted application.
     */
    @XmlEnumValue("Submitted")
    SUBMITTED("Submitted"),
    /**
     * Status for an application which is ready.
     */
    @XmlEnumValue("Ready")
    READY("Ready"),
    /**
     * Status for an application that is paused.
     */
    @XmlEnumValue("Paused")
    PAUSED("Paused"),
    /**
     * Status for an application that is stopped.
     */
    @XmlEnumValue("Stopped")
    STOPPED("Stopped");
    private final String value;

    ApplicationStatus(String v) {
        value = v;
    }

    /**
     * Gets the value of the application status.
     * @return the application status value as a String.
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>ApplicationStatus</code> object.
     * @param v - the value from which to create the <code>ApplicationStatus</code>.
     * @return the <code>ApplicationStatus</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable status values.
     */
    public static ApplicationStatus fromValue(String v) {
        for (ApplicationStatus c: ApplicationStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
