
package com.utilify.framework;

import com.utilify.framework.client.Application;
import com.utilify.framework.client.Job;

/**
 * Represents a job completed event that occurs when a job is completed.
 * A job completed event is raised by the {@link Application} class, to inform the user when 
 * a job is completed and passes a reference to the completed job.
 */
public class JobCompletedEvent {
	private Job job;
	
	/**
	 * Constructs an instance of the <code>JobCompletedEvent</code> class with the specified job.
	 * @param job - an instance of {@link Job}.
	 */
	public JobCompletedEvent(Job job){
		this.job = job;
	}
	
	/**
	 * Gets the job associated with this event.
	 * @return an instance of <code>Job</code>.
	 */
	public Job getJob(){
		return job;
	}
}
