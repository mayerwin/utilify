
package com.utilify.framework;

/**
 * An internal utility class used to contain standard framework error messages.
 * This class is for internal use only.
 */
public final class ErrorMessage {

	/**
	 * The ValueCannotBeNullOrEmpty message
	 */
	public static final String ValueCannotBeNullOrEmpty = "Value cannot be null or empty";
	/**
	 * The ValueCannotBeNull message
	 */
	public static final String ValueCannotBeNull = "Value cannot be null";
	/**
	 * The ValueCannotBeLessThan message
	 */
	public static final String ValueCannotBeLessThan = "Value cannot be less than";
	/**
	 * The ValueCannotBeGreaterThan message
	 */
	public static final String ValueCannotBeGreaterThan = "Value cannot be greater than";
	/**
	 * The InvalidServerResponseMessage message
	 */
	public static final String InvalidServerResponseMessage = "Server returned an invalid response";
	/**
	 * The JobSubmissionErrorMessage message
	 */
	public static final String JobSubmissionErrorMessage = "Error submitting job";

}
