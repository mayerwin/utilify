
package com.utilify.framework;

import javax.xml.ws.WebFault;

/**
 * Thrown when an entity (such as an Application, Job, Dependency, or an Executor) could not be found on the Manager.
 */
@WebFault(name = EntityNotFoundFault.FaultName, targetNamespace = Namespaces.Framework)
public class EntityNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 3863396642933297971L;
	
	/*
     * Java type that goes as soapenv:Fault detail element.
     */
    private EntityNotFoundFault faultInfo;

    //needed for JAXWS to figure out if this is the wrapper class for a declared fault
    /**
     * Gets the SOAP fault that caused this exception.
     * @return the fault object.
     */
    public EntityNotFoundFault getFaultInfo(){
    	return faultInfo;
    }
    
    /**
     * Constructs an EntityNotFoundException with the specified detail message and fault object.
     * @param message - the detail message.
     * @param faultInfo - the fault object.
     */ //Required ctor for JAXWS
    public EntityNotFoundException(String message, EntityNotFoundFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * Gets the underlying message from the SOAP fault. If the fault object is <code>null</code>, an empty String is 
     * returned.
     * @see java.lang.Throwable#getMessage()
     */
    public String getMessage(){
    	return (faultInfo == null) ? "" : faultInfo.getMessage();
    }

    /**
     * Gets the type of entity (Application, Dependency, Job or Executor) that could not be found.
     * @return the type of the missing entity.
     */
    public EntityType getType(){
    	return (faultInfo == null) ? null : faultInfo.getEntityType();
    }

    /**
     * Gets the entity Id that could not be found.
     * @return the Id of the missing entity.
     */
    public String getEntityId(){
    	return (faultInfo == null) ? "" : faultInfo.getEntityId();
    }
}
