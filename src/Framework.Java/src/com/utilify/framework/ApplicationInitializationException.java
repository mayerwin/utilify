
package com.utilify.framework;

/**
 * Thrown when an application could not be initialized on the remote Manager for any reason.
 */
public class ApplicationInitializationException extends FrameworkException {

	private static final long serialVersionUID = -8638893439300665720L;
	
	/**
	 * Constructs an ApplicationInitializationException with no detail message.
	 */
	public ApplicationInitializationException() { }
    
	
	/**
	 * Constructs an ApplicationInitializationException with the specified detail message.
	 * @param message - the detail message.
	 */
	public ApplicationInitializationException(String message) {
    	super(message);
    }
    
	/**
	 * Constructs an ApplicationInitializationException with the specified detail message and cause.
	 * @param message - the detail message.
	 * @param cause - the cause of the exception.
	 */
	public ApplicationInitializationException(String message, Throwable cause){
    	super(message, cause);
    }
}