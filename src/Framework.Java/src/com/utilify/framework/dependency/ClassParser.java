package com.utilify.framework.dependency;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/*
 	Java VM Class file spec
 	 
     ClassFile {
    	u4 magic;
    	u2 minor_version;
    	u2 major_version;
    	u2 constant_pool_count;
    	cp_info constant_pool[constant_pool_count-1];
    	u2 access_flags;
    	u2 this_class;
    	u2 super_class;
    	u2 interfaces_count;
    	u2 interfaces[interfaces_count];
    	u2 fields_count;
    	field_info fields[fields_count];
    	u2 methods_count;
    	method_info methods[methods_count];
    	u2 attributes_count;
    	attribute_info attributes[attributes_count];
    }

	u2 = 2 bytes,
	u4 = 4 bytes.
	
	We only need to get till the constant_pool table. (to get to the class names)
	Even there, we can skip reading all the constants which are not strings / classes.
 */
/**
 * 
 */
class ClassParser {

	static final int JAVA_MAGIC = 0xCAFEBABE;

	static final String ARRAY_DESCRIPTOR = "[";
	
    static final int CONSTANT_UTF8 = 1;
    static final int CONSTANT_UNICODE = 2;
    static final int CONSTANT_INTEGER = 3;
    static final int CONSTANT_FLOAT = 4;
    static final int CONSTANT_LONG = 5;
    static final int CONSTANT_DOUBLE = 6;
    static final int CONSTANT_CLASS = 7;
    static final int CONSTANT_STRING = 8;
    static final int CONSTANT_FIELD = 9;
    static final int CONSTANT_METHOD = 10;
    static final int CONSTANT_INTERFACEMETHOD = 11;
    static final int CONSTANT_NAMEANDTYPE = 12;

	//private static final Logger logger = Logger.getLogger(ClassParser.class.getName());

    private ClassBuilder builder = null;
    
    class Constant {

        private byte _tag;
        private int _nameIndex;
        private int _typeIndex;
        private Object _value;

        Constant(byte tag, int nameIndex) {
            this(tag, nameIndex, -1);
        }

        Constant(byte tag, Object value) {
            this(tag, -1, -1);
            _value = value;
        }

        Constant(byte tag, int nameIndex, int typeIndex) {
            _tag = tag;
            _nameIndex = nameIndex;
            _typeIndex = typeIndex;
            _value = null;
        }

        byte getTag() {
            return _tag;
        }

        int getNameIndex() {
            return _nameIndex;
        }

        int getTypeIndex() {
            return _typeIndex;
        }

        Object getValue() {
            return _value;
        }
        
        /**
         * @see java.lang.Object#toString()
         */
        public String toString() {

            StringBuffer s = new StringBuffer("");

            s.append("tag: " + getTag());
            if (getNameIndex() > -1) {
                s.append(" nameIndex: " + getNameIndex());
            }
            if (getTypeIndex() > -1) {
                s.append(" typeIndex: " + getTypeIndex());
            }
            if (getValue() != null) {
                s.append(" value: " + getValue());
            }
            return s.toString();
        }
    }
	
    ClassParser(ClassBuilder builder){
    	this.builder = builder;
    }
    
    JavaClass parse(File file) throws IOException{
    	FileInputStream ins = null;
    	try{
    		ins = new FileInputStream(file);
    		return parse(file.getCanonicalPath(), ins);
    	} finally {
    		if (ins != null)
    			ins.close();
    	}
    }
    
	JavaClass parse(String source, InputStream ins) throws IOException{
		JavaClass jClass = null;
		try {
        	DataInputStream in = new DataInputStream(ins);

        	//parseMagic
            int magic = in.readInt();
            if (magic != JAVA_MAGIC) {
                throw new IOException("Invalid class file: " + source);
            }

        	//skip/parse minorVersion 
            in.skipBytes(2);	//in.readUnsignedShort();
        	//skip/parse majorVersion 
            in.skipBytes(2);	//in.readUnsignedShort();

            Constant[] constantPool = parseConstantPool(in);

            //skip/parse the access flags
            in.skipBytes(2);	//in.readUnsignedShort();
            
            String className = parseClassName(in, constantPool);
            jClass = new JavaClass(className, builder);

            //logger.fine("Parsing class: " + jClass.getName());
            //logger.fine("Constant pool size: " + constantPool.length);
            for (Constant con : constantPool) {
            	if (con != null && con.getTag() == CONSTANT_CLASS){
            		String referencedClass = getClassName(constantPool, con);
            		//use package filter and remove un-needed references
            		if (referencedClass != null && !referencedClass.startsWith(ARRAY_DESCRIPTOR)){
            			if (!builder.shouldFilter(referencedClass))
            				jClass.addReferencedClass(referencedClass);
            		}
            	}
    		}
            
            return jClass;

        } finally {
            if (ins != null) {
                try {
                    ins.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
	}

	private Constant[] parseConstantPool(DataInputStream in) throws IOException {
        int constantPoolSize = in.readUnsignedShort();
        Constant[] pool = new Constant[constantPoolSize];
        //constant pool table indexes go from 1 to count-1.
        for (int i = 1; i < constantPoolSize; i++) {

            Constant result = null;
            
            byte tag = in.readByte();

            switch (tag){
            case (CONSTANT_CLASS):
            case (CONSTANT_STRING):
                result = new Constant(tag, in.readUnsignedShort());
                break;
            case (CONSTANT_FIELD):
            case (CONSTANT_METHOD):
            case (CONSTANT_INTERFACEMETHOD):
            case (CONSTANT_NAMEANDTYPE):
            	in.skipBytes(4);
                //result = new Constant(tag, in.readUnsignedShort(), in
                  //      .readUnsignedShort());
                break;
            case (CONSTANT_INTEGER):
            	in.skipBytes(4);
            	//result = new Constant(tag, new Integer(in.readInt()));
                break;
            case (CONSTANT_FLOAT):
            	in.skipBytes(4);
            	//result = new Constant(tag, new Float(in.readFloat()));
                break;
            case (CONSTANT_LONG):
            	in.skipBytes(8);
                //result = new Constant(tag, new Long(in.readLong()));
                break;
            case (CONSTANT_DOUBLE):
            	in.skipBytes(8);
                //result = new Constant(tag, new Double(in.readDouble()));
                break;
            case (CONSTANT_UTF8):
                result = new Constant(tag, in.readUTF());
                break;
            default:
                throw new IOException("Unknown constant: " + tag);
            }

            pool[i] = result;

            // 8-byte constants use two constant pool entries
            if (tag == CONSTANT_DOUBLE
                    || tag == CONSTANT_LONG) {
                i++;
            }
        }
        return pool;
    }

    private String parseClassName(DataInputStream in, Constant[] constantPool) throws IOException {
        //the entry for the class constant
    	int entryIndex = in.readUnsignedShort();
    	Constant classConstant = getConstant(constantPool, entryIndex);
        String parsedClassName = getClassName(constantPool, classConstant);        
        return parsedClassName;
    }
    
    private String getClassName(Constant[] constantPool, Constant classConstant){
    	if (constantPool == null || classConstant == null)
    		return null;
    	return getClassName(constantPool, classConstant.getNameIndex());
    }

	/**
	 * @param constantPool
	 * @param classConstant
	 * @param nameIndex
	 * @return the name of the class with the given name index from the constant pool
	 * @throws IllegalArgumentException
	 */
	private String getClassName(Constant[] constantPool, int nameIndex) throws IllegalArgumentException {
    	//this should be a utf8/string constant
    	Constant classNameConstant = getConstant(constantPool, nameIndex);
    	if (classNameConstant != null)
    		return slashesToDots(toUTF8(classNameConstant));    	
    	return null;
	}
    
	private Constant getConstant(Constant[] constantPool, int index){
		if (index < 0 || index > constantPool.length)
			throw new IllegalArgumentException("Invalid index for constant: " + index + 
					", Index needs to be between 0 and " + constantPool.length);
		return constantPool[index];
	}
	
    private String toUTF8(Constant entry) {
        if (entry != null && entry.getTag() == CONSTANT_UTF8) {
            return (String) entry.getValue();
        }
        return null;
    }

    String slashesToDots(String s) {
    	if (s == null)
    		return null;
        return s.replace('/', '.');
    }
}
