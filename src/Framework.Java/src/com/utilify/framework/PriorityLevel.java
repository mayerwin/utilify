
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for PriorityLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PriorityLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Low"/>
 *     &lt;enumeration value="Normal"/>
 *     &lt;enumeration value="High"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
/**
 * Represents the priority level of an application or a job. Priorities can be of the following levels:<br>
 * <li>Low</li><br>
 * The minimum priority.
 * <li>Normal</li><br>
 * The default priority which all jobs get when created.
 * <li>High</li><br>
 * The maximum priority.
 */
@XmlType(name = "PriorityLevel")
@XmlEnum
public enum PriorityLevel {

    /**
     * The minimum priority level.
     */
    @XmlEnumValue("Low")
    LOW("Low"),
    /**
     * The default priority level.
     */
    @XmlEnumValue("Normal")
    NORMAL("Normal"),
    /**
     * The maximum priority level.
     */
    @XmlEnumValue("High")
    HIGH("High");
    private final String value;

    PriorityLevel(String v) {
        value = v;
    }

    /**
     * Gets the value of the <code>PriorityLevel</code>
     * @return the value of the type (as a String).
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>PriorityLevel</code> object.
     * @param v the value from which to create the <code>PriorityLevel</code>.
     * @return the <code>PriorityLevel</code> object
     * @throws IllegalArgumentException if the specified value is not one of the acceptable type values.
     */
    public static PriorityLevel fromValue(String v) {
        for (PriorityLevel c: PriorityLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
