
package com.utilify.framework;

import com.utilify.framework.client.Application;

/**
 * The listener interface for receiving error events. 
 * The class that is interested in processing an error event implements this interface, 
 * and the object created with that class is registered with an {@link Application}, 
 * using it's {@link Application#addErrorListener} method. When the error event occurs, 
 * that object's {@link #onError} method is invoked. 
 */
public interface ErrorListener {
	
	/**
	 * Invoked when an error occurs. 
	 * @param event - the event object the contains the error information.
	 */
	void onError(ErrorEvent event);
}
