
package com.utilify.framework;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for EntityNotFoundFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityNotFoundFault">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EntityType" type="{http://schemas.utilify.com/2007/09/Framework}EntityType"/>
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents a SOAP fault that occurs when a requested entity could not be located by the server.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = EntityNotFoundFault.FaultName, propOrder = {
    "entityId",
    "entityType",
    "message"
})
public class EntityNotFoundFault {

	/**
	 * The name of the fault (as represented in the XML Schema)
	 */
	public static final String FaultName = "EntityNotFoundFault";
	
    @XmlElement(name = "EntityId", required = true, nillable = true)
    private String entityId;
    @XmlElement(name = "EntityType", required = true)
    private EntityType entityType;
    @XmlElement(name = "Message", required = true, nillable = true)
    private String message;

    /**
     * Gets the value of the entity Id.
     * @return the Id of the missing entity.
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * Sets the value of the entity Id.
     * @param value - the Id of the missing entity.
     */
    @SuppressWarnings("unused") //used by JAXWS
    private void setEntityId(String value) {
        this.entityId = value;
    }

    /**
     * Gets the value of the entity type.
     * @return the type of the missing entity.
     */
    public EntityType getEntityType() {
        return entityType;
    }

    /**
     * Sets the value of the entity type.
     * @param value - the type of the missing entity.   
     */
    @SuppressWarnings("unused") //used by JAXWS
    private void setEntityType(EntityType value) {
        this.entityType = value;
    }

    /**
     * Gets the fault message.
     * @return the fault message.   
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the fault message.
     * @param value - the fault message.   
     */
    @SuppressWarnings("unused") //used by JAXWS
    private void setMessage(String value) {
        this.message = value;
    }

}
