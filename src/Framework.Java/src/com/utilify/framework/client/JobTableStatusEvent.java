package com.utilify.framework.client;

import com.utilify.framework.JobStatus;

/// <summary>
/// Event args passed when a Job changes status within the job table (internal event).
/// </summary>
/**
 *
 */
public class JobTableStatusEvent
{
	private JobStatus status;

	protected JobTableStatusEvent()
	{ }
	
	protected JobTableStatusEvent(JobStatus status)
	{
		this.status = status;
	}
	
	protected JobStatus getStatus() {
		return status;
	}
}