
package com.utilify.framework.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ArrayOfResultInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfResultInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultInfo" type="{http://schemas.utilify.com/2007/09/Client}ResultInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Wrapper class to represent a list of {@link ResultInfo} objects.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfResultInfo", propOrder = {
    "resultInfos"
})
class ArrayOfResultInfo {

    @XmlElement(name = "ResultInfo", nillable = true)
    private List<ResultInfo> resultInfos;

    /**
     * Creates an instance of the <code>ArrayOfResultInfo</code> class with an empty list of results. 
     */
    public ArrayOfResultInfo(){
    	resultInfos = new ArrayList<ResultInfo>();
    }
    
    /**
     * Creates an instance of the <code>ArrayOfResultInfo</code> class with the specified collection of results. 
     * @param results
     * 	the collection of results to add.
     */
    public ArrayOfResultInfo(Collection<ResultInfo> results){
    	this();
    	if (results != null){
    		resultInfos.addAll(results);
    	}
    }
    
    /**
     * Creates an instance of the <code>ArrayOfResultInfo</code> class with the specified array of results. 
     * @param results
     * 	the array of results to add.
     */
    public ArrayOfResultInfo(ResultInfo[] results){
    	this();
    	if (results != null){
    		for (int i = 0; i < results.length; i++) {
    			resultInfos.add(results[i]);
			}
    	}
    }

    /**
     * Gets the list of results.
     * @return
     * 	an array of <code>ResultInfo</code> instances. 
     */    
    public ResultInfo[] toArray(){
    	ResultInfo[] results = new ResultInfo[resultInfos.size()];
    	for (int i = 0; i < results.length; i++) {
			results[i] = resultInfos.get(i);
		}
    	return results;
    }
}
