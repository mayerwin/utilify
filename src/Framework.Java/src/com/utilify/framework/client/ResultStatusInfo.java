
package com.utilify.framework.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ResultStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResultStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Info" type="{http://schemas.utilify.com/2007/09/Client}ResultInfo"/>
 *         &lt;element name="JobId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResultId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the status information for a result produced by a job.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultStatusInfo", propOrder = {
    "info",
    "jobId",
    "resultId"
})
public class ResultStatusInfo {

    @XmlElement(name = "Info", required = true, nillable = true)
    private ResultInfo info;
    @XmlElement(name = "JobId", required = true, nillable = true)
    private String jobId;
    @XmlElement(name = "ResultId", required = true, nillable = true)
    private String resultId;

    private ResultStatusInfo(){}

    /**
     * Gets the result information.
     * @return
     * 	the result information.
     */
    public ResultInfo getInfo() {
        return info;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setInfo(ResultInfo value) {
        this.info = value;
    }

    /**
     * Gets the Id of the job that produced this result.
     * @return
     * 	the job Id.
     */
    public String getJobId() {
        return jobId;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setJobId(String value) {
        this.jobId = value;
    }

    /**
     * Gets the Id of this result.
     * @return
     * 	the result Id.
     */
    public String getResultId() {
        return resultId;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setResultId(String value) {
        this.resultId = value;
    }
}
