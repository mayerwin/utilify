
package com.utilify.framework.client;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ErrorMessage;

/*
 * <p>Java class for ApplicationSubmissionInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationSubmissionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClientHost" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Dependencies" type="{http://schemas.utilify.com/2007/09/Client}ArrayOfDependencyInfo" minOccurs="0"/>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Qos" type="{http://schemas.utilify.com/2007/09/Client}QosInfo"/>
 *         &lt;element name="Username" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the information required to submit an application to the remote Manager.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationSubmissionInfo", propOrder = {
    "applicationName",
    "clientHost",
    "dependencies",
    "id",
    "qos",
    "username"
})
public class ApplicationSubmissionInfo {

    @XmlElement(name = "ApplicationName", required = true, nillable = true)
    private String applicationName;
    @XmlElement(name = "ClientHost", required = true, nillable = true)
    private String clientHost;
    @XmlElement(name = "Dependencies", nillable = true)
    private ArrayOfDependencyInfo dependencies;
    @XmlElement(name = "Id", nillable = true)
    private String id;
    @XmlElement(name = "Qos", required = true, nillable = true)
    private QosInfo qos;
    @XmlElement(name = "Username", required = true, nillable = true)
    private String username;

    private ApplicationSubmissionInfo() { }
    
    /**
	 * Creates an instance of the <code>ApplicationSubmissionInfo</code> class with the specified application name
	 * and username. 
     * The application is submitted with an empty list of dependencies and 
     * the default (@see {@link QosInfo#getDefault()} qos requirements.
	 * @param applicationName
	 * 	the name of the application submitted.
	 * @param username
	 * 	the user submitting this application.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationName</code> is <code>null</code> or empty, or <br>
	 * <code>username</code> is <code>null</code> or empty.
     */
    public ApplicationSubmissionInfo(String applicationName, String username) {
    	this(applicationName, username, null, QosInfo.getDefault());
    }
    
    /**
	 * Creates an instance of the <code>ApplicationSubmissionInfo</code> class with the specified application name,
	 * username and dependency information. 
	 * The application is submitted with the default (@see {@link QosInfo#getDefault()} qos requirements.
	 * @param applicationName
	 * 	the name of the application submitted.
	 * @param username
	 * 	the user submitting this application.
	 * @param dependencies
	 * 	the list of common dependencies for all jobs in this application.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationName</code> is <code>null</code> or empty, or <br>
	 * <code>username</code> is <code>null</code> or empty.
     */
    public ApplicationSubmissionInfo(String applicationName, String username, DependencyInfo[] dependencies){
    	this(applicationName, username, dependencies, QosInfo.getDefault());	
    }

	/**
	 * Creates an instance of the <code>ApplicationSubmissionInfo</code> class with the specified application name,
	 * username, dependency and qos information.
	 * @param applicationName
	 * 	the name of the application submitted.
	 * @param username
	 * 	the user submitting this application.
	 * @param dependencies
	 * 	the list of common dependencies for all jobs in this application.
	 * @param qos
	 * 	the quailty of service information for this application.
	 * @throws IllegalArgumentException
	 * 	if <code>applicationName</code> is <code>null</code> or empty, or <br>
	 * <code>username</code> is <code>null</code> or empty.
	 */
	public ApplicationSubmissionInfo(String applicationName, String username, 
			DependencyInfo[] dependencies, QosInfo qos) {
		setApplicationName(applicationName);
		setUsername(username);
		setDependencies(dependencies);
		setQos(qos);
		try {
			setClientHost(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			setClientHost("");
		}
	}
	
	/**
     * Gets the application name.
     * @return
     * 	the name of this application.    
     */
    public String getApplicationName() {
        return applicationName;
    }

    private void setApplicationName(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": Application name");
        this.applicationName = value;
    }

    /**
     * Gets the client host name.
     * 
     * @return
     * 	the hostname of the client machine from which this application is submitted.   
     */
    public String getClientHost() {
        return clientHost;
    }

    private void setClientHost(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": client host");
        this.clientHost = value;
    }

    /**
     * Gets the dependencies for this application.
     * 
     * @return
     *     an array of {@link DependencyInfo} objects containing the dependency information for this application. 
     */
    public DependencyInfo[] getDependencies() {
    	if (dependencies == null)
    		return new DependencyInfo[0];

    	return dependencies.toArray();
    }

    private void setDependencies(DependencyInfo[] value) {
    	if (value == null)
    		this.dependencies = new ArrayOfDependencyInfo();
    	else
    		this.dependencies = new ArrayOfDependencyInfo(value);
    }

    @SuppressWarnings("unused") //used on the server-side
	private String getId() {
        return id;
    }

    @SuppressWarnings("unused") //set on the server-side
	private void setId(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": id");
        this.id = value;
    }

    /**
     * Gets the quality of service information for this application.
     * 
     * @return
     * 	and instance of {@link QosInfo} containing the qos information.
     *     
     */
    public QosInfo getQos() {
        return qos;
    }

    private void setQos(QosInfo value) {
    	if (value == null )
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": qos");
        this.qos = value;
    }

    /**
     * Gets the username.
     * 
     * @return
     * 	the user who is submitting this application.  
     */
    public String getUsername() {
        return username;
    }

    private void setUsername(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + " : username");
        this.username = value;
    }
}
