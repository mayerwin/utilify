
package com.utilify.framework.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.sun.xml.ws.Closeable;
import com.utilify.framework.ApplicationInitializationException;
import com.utilify.framework.ApplicationProperties;
import com.utilify.framework.ApplicationStatus;
import com.utilify.framework.DependencyNotFoundException;
import com.utilify.framework.DependencyScope;
import com.utilify.framework.EntityNotFoundException;
import com.utilify.framework.EntityType;
import com.utilify.framework.ErrorEvent;
import com.utilify.framework.ErrorListener;
import com.utilify.framework.ErrorMessage;
import com.utilify.framework.FrameworkException;
import com.utilify.framework.Helper;
import com.utilify.framework.JobCompletedEvent;
import com.utilify.framework.JobCompletedListener;
import com.utilify.framework.JobNotFoundException;
import com.utilify.framework.JobStatus;
import com.utilify.framework.JobStatusEvent;
import com.utilify.framework.JobStatusListener;
import com.utilify.framework.JobSubmissionException;
import com.utilify.framework.JobSubmittedEvent;
import com.utilify.framework.JobSubmittedListener;
import com.utilify.framework.JobType;
import com.utilify.framework.NumberSequenceGenerator;
import com.utilify.framework.ResultNotFoundException;
import com.utilify.framework.dependency.DependencyResolver;
import com.utilify.framework.manager.ApplicationManager;
import com.utilify.framework.manager.IApplicationManager;

/**
 * Represents an Application, which is a logical container for a set of tasks or Jobs. A Job is the smallest atomic unit of work that
 * can be individually addressed, scheduled, and executed on a remote machine that is part of the distributed system.
 * An Application is a higher level abstraction that can be executed on a distribted network of computers, with a specified
 * QoS (quality of service - {@link QosInfo}).
 * The <code>Application</code> class is also the starting point for developers using the Utilify framework API, so remotely connect
 * to the Manager, submit the application and its constituent jobs, and obtain the results of execution.
 */
public class Application implements Disposable, JobTableStatusListener {

    private static NumberSequenceGenerator appIdSeq = new NumberSequenceGenerator();

    // Proxy to Utilify Manager
    private IApplicationManager manager;

    // Application Properties
    private List<DependencyInfo> dependencies;
    private String name;
    private String id = ""; //default
    private ApplicationStatus status = ApplicationStatus.UNINITIALIZED; //default
    private QosInfo qos = QosInfo.getDefault();
    
    //  job table for holding/tracking Jobs
    private ClientJobTable jobTable = new ClientJobTable();
        
    // Internal threads for Submitting and Monitoring Jobs
    private DependencyResolverThread resolver;
    private JobMonitorThread monitor;
    private JobSubmitterThread submitter;
    private Thread appDependencyResolver;
    private Thread jobMonitor;
    private Thread jobSubmitter;
    private Object stopThreadsLock = new Object();
    private boolean stopThreads;

    // Events (Other events are kept inside the Submitter and Monitor threads)
    private List<ErrorListener> errorListeners;
    
    // Logging
    private Logger logger;

    /**
     * Creates an instance of the Application.
     */
    public Application() {
    	this("Application" + appIdSeq.GetNext());
    }

    /**
     * Creates an instance of the Application with the given name.
     * @param name
     * 	the name of the application.
     * @throws IllegalArgumentException 
     * 	if name is a null reference or an empty string.
     */
    public Application(String name) {
    	this(name, null, null);
    }

    /**
     * Creates an instance of the Application with the given name and qos.
     * @param name
     * 	the name of the application.
     * @param qos
     * the qos for the application.
     * @throws IllegalArgumentException 
     * 	if name is a null reference or an empty string.
     */
    public Application(String name, QosInfo qos) {
    	this(name, qos, null);
    }

    /**
     * Creates an instance of the Application with the given name and qos.
     * @param name
     * 	the name of the application.
     * @param dependencies
     * common dependencies for all jobs in the application
     * @throws IllegalArgumentException 
     * 	if name is a null reference or an empty string.
     */
    public Application(String name, Iterable<? extends DependencyInfo> dependencies) {
    	this(name, null, dependencies);
    }
    
    //todoLater: add credentials : which may affect the shape of these ctors
    /**
     * Creates an instance of the Application with the given name and list of dependencies.
     * @param name 
     * 	the name of the application.
     * @param qos
     * the qos for the application.
     * @param dependencies 
     * 	common dependencies for all jobs in the application
     * @throws IllegalArgumentException 
     * 	if name is a null reference or an empty string
     */
    public Application(String name, QosInfo qos, Iterable<? extends DependencyInfo> dependencies) {
    	
        this.logger = Logger.getLogger(this.getClass().getName());

        if (name == null || name.trim().length() == 0)
            throw new IllegalArgumentException("Application name cannot be null or empty");

        // TODO: Add custom settings.
        
        this.name = name;
        this.status = ApplicationStatus.UNINITIALIZED;
        this.dependencies = new ArrayList<DependencyInfo>();

        // TODO: Add backoff value
        
        // Qos
        if (qos != null)
        	this.qos = qos;
        else
        	this.qos = QosInfo.getDefault();
        
        // Dependencies
        if (dependencies != null) {
        	for (DependencyInfo dep : dependencies) {
                if (dep == null)
                	throw new IllegalArgumentException("Elements of the dependencies list should not be null");
                
                this.dependencies.add(dep);
            }
        }

        //for now read from the default properties config file
        ApplicationProperties props = new ApplicationProperties();
        ApplicationManager appmgr = new ApplicationManager(props.getManagerUrl());
        this.manager = appmgr.getApplicationManagerDefaultEndPoint();
        //TODO get proper config are connect (??) : allow both xml config in system.service model / using API
        
        //initialize the job-monitor, submitter objects, since we need to subscribe to events
        monitor = new JobMonitorThread();
    	submitter = new JobSubmitterThread();
    	
    	// Other event stuff
        this.errorListeners = new ArrayList<ErrorListener>();
        
        this.jobTable.addStatusListener(this);
    }

	public void onJobTableStatusChanged(JobTableStatusEvent event) {
        // Wake up threads that were resting so they can do what they need.
        if (event.getStatus() == JobStatus.UN_INITIALIZED) {
        	this.submitter.resumeThread();
        } else if (event.getStatus() == JobStatus.SUBMITTED) {
        	this.monitor.resumeThread();
        }
	}
    
	/**
     * Gets the Id of the application.
     * If the application is not yet initialized, this returns an empty string.
     * @return 
     * 	the application Id
     */
    public String getId() {
    	return id;
    }
    
    /**
     * Gets the name of the application.
     * @return 
     * 	the application name.
     */
    public String getName() {
    	return name;
    }

    /**
     * Gets the status of the application.
     * @return 
     * 	the application status.
     * @see ApplicationStatus
     */
    public ApplicationStatus getStatus() {
        ApplicationStatus s;
        synchronized(this.status){
            s = status;
        }
        return s; 
    }

    private void setStatus(ApplicationStatus status) {
    	synchronized (this.status) {
            this.status = status;
        }
    }

    /**
     * Adds a job to the application, for remote execution. 
     * Adding a job to an application causes it to be sent to the manager for scheduling, 
     * if the appication is already started. If the application is not already started, 
     * the job is queued locally, and is sent to the manager when the application is started.
     * @param job 
     * 	an instance of a class that implementats the {@link Executable} interface
     * @return 
     * 	an instance of {@link Job} that contains additional information about the job submitted, 
     * such as its Id on the server, execution status and so on.
     * @throws IllegalArgumentException 
     * 	if job is a null reference
     * @throws ObjectDisposedException 
     * 	if the application has already been disposed.
     */
    public Job addJob(Executable job) {
        verifyDisposed();
        if (job == null)
            throw new IllegalArgumentException("Job cannot be null.");
        
        //no need to check if a job implements Serializable, since Executable extends Serializable

        Job remoteJob = new Job(job);
        //buffer jobs instead of sending away immediately
        //the job-submitted thread will pick it up and send it off
        jobTable.add(remoteJob);
        
        return remoteJob;
    }

    /**
     * Cancels (i.e aborts) a remotely running application and all of its jobs.
     * @throws IllegalStateException 
     * 	if the application is not yet initialized or if it is already stopped.
     * @throws ObjectDisposedException 
     * 	if the application has already been disposed.
     */
    public void cancel() throws IllegalStateException {
    	
        verifyDisposed();

        ApplicationStatus s = getStatus();
        if (s == ApplicationStatus.STOPPED)
            throw new IllegalStateException("The application is already stopped.");
        if (s == ApplicationStatus.UNINITIALIZED)
            throw new IllegalStateException("The application is not yet initialized.");

        manager.abortApplication(this.getId());		
        setStatus(ApplicationStatus.STOPPED);
        
        // Get all Jobs from the ClientJobTable and Cancel them.
        Job[] allJobs = jobTable.getJobs();
        for (Job job : allJobs)
        {
        	cancelJob(job);
        }
    }

    /**
     * Cancels a previously submitted job.
     * Calling this method will cause an 'abort' request to the remote Manager, which then attempts to cancel the 
     * job (if it is already running) or remove it from the list of schedulable jobs, if it is not.
     * It is possible that a job which has been cancelled has actually completed execution,
     * in which case it cannot be stopped - and will eventually have a status of {@link JobStatus#COMPLETED}.
     * In other cases, the job will eventually have a status of {@link JobStatus#CANCELLED} due to this method call.   
     * @param job 
     * 	the job to cancel.
     * @throws IllegalArgumentException 
     * 	if <code>job</code> is a null reference. 
     * @throws ObjectDisposedException 
     * 	if the application has already been disposed.
     */
    public void cancelJob(Job job){
    	
        verifyDisposed();

        if (job == null)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": job");

        // Only set Cancelled for Jobs yet to be Submitted. Other Jobs to be Cancelled by JobMonitor.
        if (job.getStatus() == JobStatus.UN_INITIALIZED)
        {
            jobTable.changeStatus(job, JobStatus.CANCELLED);
        }
        else
        {
            // If the job has been sent to the manager the job monitor will need to know to cancel it.
            // Tell the job monitor that the job should be cancelled.
            // If it is already cancelled or had a cancel requested, dont set it again.
            if (!job.isCancelRequested() && job.getStatus() != JobStatus.CANCELLED)
                job.setCancelRequested(true);
        }
    }
     
    /**
     * Gets a copy of the list of the dependencies of the application.
     * @return 
     * 	an array containing the dependencies
     */
    private DependencyInfo[] getDependencies() { //this doesn't need to be a public API - for now
        DependencyInfo[] deps = new DependencyInfo[dependencies.size()];
        if (deps.length > 0)
            dependencies.toArray(deps);
        return deps;
    }

    /**
     * Gets the results produced by a job after it has finished execution. If the job has not finished execution, or has not produced
     * any results, this method returns an empty array.
     * @param job
     * 	the job for which results are needed.
     * @return 
     * 	an array of {@link ResultStatusInfo} objects containing the result information for the specified job
     * @throws FrameworkException 
     * 	if there is an internal error trying to get the job results.
     */
    public ResultStatusInfo[] getJobResults(Job job) throws FrameworkException {
        verifyDisposed();

        if (job == null)
            throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": job");

        ResultStatusInfo[] results = new ResultStatusInfo[0];
        
        //ideally this method should be called only for jobs which have declared results and 
        //have also completed execution
        //if the job has not completed execution, but has declared results it will simply get the initial list

        Results resultJob = null; 
        if (job.getInitialInstance() instanceof Results){
        	resultJob = (Results)job.getInitialInstance();
        }
        
        if (resultJob != null && resultJob.getExpectedResults().length > 0) {
			try {
				ArrayOfResultStatusInfo resStatusArray = manager.getJobResults(job.getId());
				if (resStatusArray == null)				
					throw new FrameworkException("Could not get job result status: " + ErrorMessage.InvalidServerResponseMessage);
				results = resStatusArray.toArray();
				if (results == null)				
					throw new FrameworkException("Could not get job result status: " + ErrorMessage.InvalidServerResponseMessage);
				
			} catch (EntityNotFoundException e) {
				//has to be an job-not-found exception : but should not occur
				if (e.getType() == EntityType.JOB)
					throw new JobNotFoundException(e.getMessage(), e.getEntityId());

					throw e; //this is a runtime-exception anyway
			}
        }
		
        return results;
    }
    
    /**
     * Gets the data content of the specified result. 
     * @param result 
     * 	the result for which the data is needed.
     * @return 
     * 	an instance of {@link ResultContent} that has the data of the result.
     * @throws FrameworkException 
     * 	if there is an internal error trying to get the result data.
     */
    public ResultContent getResultContent(ResultStatusInfo result) throws FrameworkException{
		if (result == null)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": result");
		if (result.getResultId() == null || result.getResultId().trim().length() == 0)
			throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": result id");
		
		ResultContent content = null;
		try {
			content = manager.getResult(result.getResultId());
			if (content == null)
				throw new FrameworkException("Could not get result: " + ErrorMessage.InvalidServerResponseMessage);
		} catch (EntityNotFoundException e) {
			//has to be an result-not-found exception : but should not occur
			if (e.getType() == EntityType.RESULT)
				throw new ResultNotFoundException(e.getMessage(), e.getEntityId());
			throw e; //this is a runtime-exception anyway
		}
		return content;
	}

    /**
     * Starts this application on the manager.
     * @throws ApplicationInitializationException 
     * 	The application could not be initialized on the remote manager
     * @throws IllegalStateException 
     * 	The application is already started, paused or aborted
     * @throws ObjectDisposedException
     *  if the application has already been disposed.
     */
    public void start() throws ApplicationInitializationException {
        verifyDisposed();

        if (status != ApplicationStatus.UNINITIALIZED)
            throw new IllegalStateException("The application is already started, paused or aborted.");

        //this method is not meant to be thread-safe, but we should still lock the state when starting it
        //ideally there will be no other threads that will be active at this point, i.e no contention
        //so there shouldn't be much of a penalty here
        synchronized (this.status) {
            //todoLater: accept, username, Qos via API
            ApplicationSubmissionInfo appInfo = new ApplicationSubmissionInfo(this.name,
                "User", this.getDependencies(), this.qos);

            //todoLater: catch any communication exceptions here and wrap them
            id = manager.submitApplication(appInfo);
            if (id != null && id.trim().length() != 0)
                status = ApplicationStatus.SUBMITTED;
            else
                throw new ApplicationInitializationException("Could not initialize application.");
        }

        //fire off a worker thread to do the dependency stuff for the app
        //this may take a bit long - since we may need to process file transfers - so not using the ThreadPool
        if (this.dependencies.size() > 0) {
            startDependencyResolver();
        }

        startJobMonitor();
        startJobSubmitter();
    }

    /**
     * Pauses this application. Adding more jobs to a paused application will result in the jobs being suspended on the manager 
     * for later execution, when the application is resumed.
     * @throws IllegalStateException 
     * 	if the application is not yet initialised, already paused, or already stopped.
     * @throws ObjectDisposedException
     * 	if the application is already disposed.
     */
    public void pause() {
        verifyDisposed();

        ApplicationStatus s = getStatus();
        if (s == ApplicationStatus.UNINITIALIZED)
            throw new IllegalStateException("Cannot pause an application that is not yet initialized.");
        if (s == ApplicationStatus.PAUSED)
            throw new IllegalStateException("Cannot pause an application that is already paused.");
        if (s == ApplicationStatus.STOPPED)
            throw new IllegalStateException("Cannot pause an application that is already stopped.");

        manager.pauseApplication(this.getId());
		setStatus(ApplicationStatus.PAUSED);

		//todoLater: wrap any comm exceptions
    }

    /**
     * Resumes this application. 
     * This will allow waiting jobs in the application to be eligible for scheduling.
     * @throws IllegalStateException
     * 	if the application is not paused.
     * @throws ObjectDisposedException
     *  if the application is already disposed.
     */
    public void resume() {
        verifyDisposed();

        ApplicationStatus s = getStatus();
        if (s != ApplicationStatus.PAUSED)
            throw new IllegalStateException("Cannot resume an application that is not paused.");

        //todoLater: wrap any exceptions from remote calls
		manager.resumeApplication(this.getId());

        //we need to call this here, since we don't know which state the app will be in next.
        //it may be waiting for dependencies, or ready
		ApplicationStatusInfo statusInfo = manager.getApplicationStatus(this.getId());
        setStatus(statusInfo.getStatus());
    }

    private boolean disposed = false;    

    /**
     * Disposes this application, which causes it to release resources such as file handles, network connections,
     * web service proxies etc.
     * @see com.utilify.framework.client.Disposable#dispose()
     */
    public void dispose() {
        dispose(true);
    }

    protected void dispose(boolean disposing) {
        if (disposed)
            return;
        if (disposing) {
            //clean up managed stuff
            Closeable closeableProxy = (Closeable)manager;
            if (closeableProxy != null){
            	closeableProxy.close();
            } 

            manager = null;
            stopAllThreads();
        }

        //we have no base class to call dispose on, so nothing more to do here.

        disposed = true;
    }

    private void verifyDisposed() {
        if (disposed) {
            throw new ObjectDisposedException(this.getClass().getName());
        }
    }

    private boolean isStopRequested() {
        boolean stop = false;
        synchronized (stopThreadsLock) {
            stop = stopThreads;
        }
        return stop;
    }
    
    private void requestStopThreads(boolean stop) {
    	synchronized (stopThreadsLock) {
            stopThreads = stop;
        }
    }

    private void startDependencyResolver() {
    	resolver = new DependencyResolverThread();
        appDependencyResolver = new Thread(resolver);
        appDependencyResolver.setName("AppDependencyResolver");
        appDependencyResolver.setDaemon(true);
        appDependencyResolver.start();
    }

    private void startJobMonitor() {
        //start off a new thread to monitor jobs and send locally queued jobs 
    	jobMonitor = new Thread(monitor);
        jobMonitor.setName("JobMonitor");
        jobMonitor.setDaemon(true);
        jobMonitor.start();
    }

    private void startJobSubmitter() {
        jobSubmitter = new Thread(submitter);
        jobSubmitter.setName("JobSubmitter");
        jobSubmitter.setDaemon(true);
        jobSubmitter.start();
    }

    private void stopAllThreads() {
        requestStopThreads(true);
        int timeoutMillis = 1000;
        submitter.resumeThread(); // wake up submitter so it can exit (we know it shouldnt be null)
        monitor.resumeThread();
        Helper.waitAndAbort(appDependencyResolver, timeoutMillis);
        Helper.waitAndAbort(jobSubmitter, timeoutMillis);
        Helper.waitAndAbort(jobMonitor, timeoutMillis);

        //remove the references
        appDependencyResolver = null;
        jobSubmitter = null;
        jobMonitor = null;
    }

    /**
     * Adds the specified listener instance to the list of listeners which will get notified when
     * an {@link ErrorEvent} occurs.
     * @param listener - the listener to be notified.
     * @throws IllegalArgumentException: if <code>listener</code> is a null reference.
     */
    public void addErrorListener(ErrorListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	synchronized (errorListeners) {
    		errorListeners.add(listener);
		}
    }

    /**
     * Removes the specified listener instance from the list of listeners which will get notified when
     * an {@link ErrorEvent} occurs.
     * @param listener - the listener to be removed.
     * @throws IllegalArgumentException: if <code>listener</code> is a null reference.
     */
    public void removeErrorListener(ErrorListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	synchronized (errorListeners) {
    		errorListeners.remove(listener);
		}
    }   
    
    /**
     * Adds the specified listener instance to the list of listeners which will get notified when
     * an {@link JobStatusEvent} occurs.
     * @param listener - the listener to be notified.
     * @throws IllegalArgumentException: if <code>listener</code> is a null reference.
     */
    public void addStatusListener(JobStatusListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	if (monitor != null){
    		System.out.println("Calling jobmonitor.addStatusListener");
    		monitor.addStatusListener(listener);
    		System.out.println("Done Calling jobmonitor.addStatusListener");
    	} else {
    		System.out.println("So you think monitor is null? ");
    	}
    }

    /**
     * Removes the specified listener instance from the list of listeners which will get notified when
     * an {@link JobStatusEvent} occurs.
     * @param listener - the listener to be removed.
     * @throws IllegalArgumentException: if <code>listener</code> is a null reference.
     */
    public void removeStatusListener(JobStatusListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	if (monitor != null)
    		monitor.removeStatusListener(listener);
    }

    /**
     * Adds the specified listener instance to the list of listeners which will get notified when
     * a {@link JobCompletedEvent} occurs.
     * @param listener - the listener to be notified.
     * @throws IllegalArgumentException: if <code>listener</code> is a null reference.
     */
    public void addJobCompletedListener(JobCompletedListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	if (monitor != null)
    		monitor.addJobCompletedListener(listener);
    }

    /**
     * Removes the specified listener instance from the list of listeners which will get notified when
     * an {@link JobCompletedEvent} occurs.
     * @param listener - the listener to be removed.
     * @throws IllegalArgumentException: if <code>listener</code> is a null reference.
     */
    public void removeJobCompletedListener(JobCompletedListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	if (monitor != null)
    		monitor.removeJobCompletedListener(listener);
    }
    
    /**
     * Adds the specified listener instance to the list of listeners which will get notified when
     * a {@link JobSubmittedEvent} occurs.
     * @param listener - the listener to be notified.
     * @throws IllegalArgumentException: if <code>listener</code> is a null reference.
     */
    public void addJobSubmittedListener(JobSubmittedListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	if (monitor != null)
    		monitor.addJobSubmittedListener(listener);
    }

    /**
     * Removes the specified listener instance from the list of listeners which will get notified when
     * an {@link JobSubmittedEvent} occurs.
     * @param listener - the listener to be removed.
     * @throws IllegalArgumentException: if <code>listener</code> is a null reference.
     */
    public void removeJobSubmittedListener(JobSubmittedListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
    	if (monitor != null)
    		monitor.removeJobSubmittedListener(listener);
    }
    
    private void raiseErrorEvent(Exception error) {
        logger.fine("Raising OnError: " + error.getMessage());
        ErrorEvent event = new ErrorEvent(error);
        List<ErrorListener> copy = new ArrayList<ErrorListener>();
    	synchronized (errorListeners) {
    		copy.addAll(errorListeners);
		}
    	for (ErrorListener listener : errorListeners) {
			try {
				listener.onError(event);
			} catch (Exception ex){
				logger.warning("Error raising error event. Continuing..." + ex.toString());
			}
		}
    }

    /// <summary>
    /// Internal use only. This is called by SendApplicationDependencies / SubmitJobsRemote to send the dependencies for an app/job if needed
    /// </summary>
    /// <param name="appOrJobId"></param>
    /// <param name="scope"></param>
    /// <param name="dependencies"></param>
    private void sendDependenciesIfNeeded(String appOrJobId, DependencyScope scope, Collection<DependencyInfo> depInfos) throws IOException, DependencyNotFoundException {
    	
    	Hashtable<String, DependencyInfo> depLookup = new Hashtable<String, DependencyInfo>();
    	for (DependencyInfo dep : depInfos) {
    		depLookup.put(dep.getHash(), dep);
    	}

    	if (depLookup.size() > 0) {

    		DependencyInfo[] unresolvedDeps = null;
    		try {
	    		ArrayOfDependencyInfo unresolvedDepArray = 
					manager.getUnresolvedDependencies(new DependencyQueryInfo(appOrJobId, scope));
				if (unresolvedDepArray != null)
	    			unresolvedDeps = unresolvedDepArray.toArray();
			} catch (EntityNotFoundException e) {
				String message = e.getMessage();
				EntityType type = e.getType();
				String entityId = e.getEntityId();
				if (type == EntityType.DEPENDENCY)
					throw new DependencyNotFoundException(message, entityId);
				
				throw e; //this should never execute actually : but it is a RuntimeException anyway
			}

    		if (unresolvedDeps != null && unresolvedDeps.length > 0) {
    			for (DependencyInfo unresolvedDep :  unresolvedDeps) {
    				if (isStopRequested()) // just check since this could potentially take long
    					break;
    				if (depLookup.containsKey(unresolvedDep.getHash())) {
    					logger.fine("Trying to send Dependency: " + unresolvedDep.getFilename());
    					DependencyInfo localDepInfo = depLookup.get(unresolvedDep.getHash());
    					//the unresolved deps we get from the manager will exclude the remote_url deps
    					sendDependencyRemote(unresolvedDep.getId(), localDepInfo.getLocalPath());
    				} else {
    					// raise an error event to tell the user
    					raiseErrorEvent(new FrameworkException(
    							"Could not send dependencies. Server reported invalid file hash for dependency " + unresolvedDep));
    				}
    			}
    		} // if length is zero, that's good - meaning nothing to send!:)
    	} // if lookup count = 0, no declared dependencies - good.
    }
    
    /// <summary>
    /// Sends an app/job's dependency to the manager
    /// </summary>
    private void sendDependencyRemote(String depId, String filePath) 
    	throws IOException, IllegalStateException, DependencyNotFoundException {
        //how do we stream this content?
        byte[] fileContent = Helper.readFile(filePath);

        DependencyContent content = new DependencyContent(depId, fileContent);

        try {
			manager.sendDependency(content);
		} catch (EntityNotFoundException e) {
			if (e.getType() == EntityType.DEPENDENCY)
				throw new DependencyNotFoundException(e.getMessage(), e.getEntityId());

			throw e; //this should never happen normally
		}
    }

    //to finish job cycle: instead of a 'completed' event on the app., we just have the JobStatusChanged. 
    //And the client is expected to call the GetResults method to retrieve the results
    
    private class DependencyResolverThread implements Runnable {
    	
		/**
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			sendApplicationDependencies();
		}
		
	    //this is run on a seperate thread, after 'start'
	    private void sendApplicationDependencies()
	    {
	        try {
	        	logger.info("Starting dependency resolver thread...");
	            sendDependenciesIfNeeded(getId(), DependencyScope.APPLICATION, dependencies);
	        } catch (ObjectDisposedException ox) {
	            logger.fine("Object disposed when sending application dependencies.");
	        } catch (IOException ix) {
				raiseErrorEvent(ix);
			} catch (DependencyNotFoundException dx) {
				raiseErrorEvent(dx);
			}
	        logger.info("Application dependency resolver thread ended.");
	    }
    }
    
    private class JobSubmitterThread implements Runnable {
    	
    	private Semaphore jobsToSubmitSemaphore = new Semaphore(0);
    	
		/**
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			submitJobs();
		}
		
		private synchronized void resumeThread() {
			if (jobsToSubmitSemaphore.availablePermits() == 0)
				jobsToSubmitSemaphore.release();
		}

		private void submitJobs() {
			int loopWaitTime = 1000; // millis
			int maxJobBufferSize = 3;
			int maxWaitLoops = 3;

			int waitLoops = 0;
			boolean submitInThisRound = false;
			Job[] jobsToSubmit;

			// submits jobs that are locally queued and clear the list
			try {
				logger.info("Starting job submitter thread...");
				while (!isStopRequested()) {
                    // Get all the new Jobs to Submit them to the Manager.
                    jobsToSubmit = jobTable.getJobs(StatusGroup.NEW);
					
                    if (jobsToSubmit.length == 0) // is this synchronized with the other locks?
                    {
                        logger.fine("*** Sleeping job submitter because we have no jobs to submit.");                        
                    	jobsToSubmitSemaphore.acquire(); // try to sleep.
                        logger.fine("*** Waking job submitter.");
                        if (isStopRequested())
                            continue;
                    }
					
					if (jobsToSubmit.length > 0)
						logger.fine(String.format("Got %d jobs to submit", jobsToSubmit.length));

					submitInThisRound = (jobsToSubmit.length >= maxJobBufferSize)
						|| (jobsToSubmit.length > 0 && waitLoops >= maxWaitLoops);

					if (jobsToSubmit.length > 0) {
						logger.fine(String.format(
							"Submitting in this round = %b. MaxBufferSize = %d, WaitLoops = %d, MaxWaitLoops = %d", 
							submitInThisRound, maxJobBufferSize, waitLoops, maxWaitLoops));
					}
					
					if (submitInThisRound) {
						try {
							// send these off
							submitJobsRemote(jobsToSubmit);
							jobsToSubmit = null;
							// reset waitloops
							waitLoops = 0;
						} catch (Exception ex) {
							// something went wrong trying to serialize jobs /
							// resolve dependencies / send dependencies
							logger.warning(ex.toString());
							raiseErrorEvent(new JobSubmissionException(
									"Error submitting jobs", ex));
						}
					}

					Thread.sleep(loopWaitTime);
					waitLoops++;
				}
				
			} catch (InterruptedException ix) {
				logger.fine("SubmitJobs thread interrupted.");
			} catch (ObjectDisposedException ox) {
				logger.fine("Object disposed when submitting jobs");
			}

			logger.info("Submit jobs thread ended.");
		}

		private void submitJobsRemote(Job[] jobs) throws IOException, FrameworkException {

			List<JobSubmissionInfo> jobInfosToSubmit = new ArrayList<JobSubmissionInfo>();

			for (Job remoteJob : jobs) {
				// since this method is called by the Submitjobs and runs a
				// potentially long loop, we check at each step
				if (isStopRequested())
					return;

				Executable job;
				synchronized (remoteJob) {
					job = remoteJob.getInitialInstance();
				}				

				//1. find dependencies
                List<DependencyInfo> deps = getDependenciesToSubmit(job);
                remoteJob.setDependencies(deps);
				
                //2. find expected results to be sent to the Manager later.
                List<ResultInfo> res = getResultsToSubmit(job);

				//3. serialize job instance
				byte[] instance = Helper.serialize(job);

				//4. find job type
				JobType type;
				if (job instanceof NativeExecutable)
					type = JobType.NATIVE_MODULE;
				else
					type = JobType.CODE_MODULE;

                //5. add to list of jobs to send 
				JobSubmissionInfo jobInfo = new JobSubmissionInfo(getId(), type, instance, deps, res);
				logger.fine(String.format("Adding job to submit: Platform:%s, Type:%s", jobInfo.getClientPlatform(), jobInfo.getJobType()));
				jobInfosToSubmit.add(jobInfo);
			}

            // TODO: Remove any cancelled Jobs before they get submitted.

            //6. send jobs to remote manager
            //we expect to get the ids in the same order as the jobInfos submitted, which are in the same order as the jobs param
            //
            //we have two seperate loops: one for collecting all job infos, one for submitting. This is so that we batch remote calls
            //i.e reduce overheads - better performance
            sendJobsToManager(jobInfosToSubmit, jobs);
		}

		private void sendJobsToManager(List<JobSubmissionInfo> jobInfosToSubmit, Job[] jobs) throws DependencyNotFoundException, IOException {

			//we have two seperate loops: one for collecting all job infos,
			//one for submitting. This is so that we batch remote calls
			//i.e reduce overheads - better performance

			//6. send jobs to remote manager 
			//we expect to get the ids in the same order as the jobInfos
			//submitted, which are in the same order as the jobs param

            boolean submitted = false;
            String[] jobIds = new String[0];
            JobSubmissionException jx = null;
		
            while (!submitted) {
            	
            	if (isStopRequested()) // since all submissions could take a long time.
            		return;
            	
				try {
					ArrayOfJobSubmissionInfo jobSubArray = new ArrayOfJobSubmissionInfo(jobInfosToSubmit);
					ArrayOfstring jobIdArray = manager.submitJobs(jobSubArray);
					jobIds = jobIdArray.getStrings().toArray(new String[] {});
					submitted = (jobIds != null && jobIds.length == jobs.length);
				} catch (EntityNotFoundException ex) {
                    logger.fine("Failed to send JobInfos (no backoff implemented yet). " + ex.getMessage());
                    // TODO: Implement backoff.
//                    if (backOff.NumberOfRetries <= backOff.MaxRetries)
//                        Thread.Sleep(backOff.GetNextInterval());
//                    else
//                    {
                        jx = new JobSubmissionException("Could not retrieve job ids from the server", ex);
                        break;
//                    }				
				}
	            logger.fine("Submitted " + jobIds.length + " jobs");

				//7. Get jobids and put these jobs in list of jobs to monitor
	            if (!submitted) {
	            	raiseErrorEvent(jx);
				} else {
					for (int i = 0; i < jobIds.length; i++) {
						// since this method is called by the Submitjobs and runs a
						// potentially long loop, we check at each step
						if (isStopRequested())
							return;
	
						//not-initializing the jobStatus with a JobStatusInfo object here
						//that will be automatically done in the next monitoring cycle.
						
						//set the job id and initial Submitted status here -
						//since it doesnt make sense to have an id but no status!
						Job remoteJob = jobs[i];
						remoteJob.setId(jobIds[i]);
						jobTable.changeStatus(remoteJob, JobStatus.SUBMITTED); 
						// We could have a Cancelled Job being set to Submitted again.
						// this case will be handled in the Monitor.
	
						// now check if the job dependencies need to be sent				
						sendDependenciesIfNeeded(jobIds[i], DependencyScope.JOB, 
								remoteJob.getDependencies());
					}
				}
            }
		}

		private List<ResultInfo> getResultsToSubmit(Executable job) {
            List<ResultInfo> jobResults = new ArrayList<ResultInfo>();
            
			if (job instanceof Results) {
				Results resJob = (Results) job;
				
				//call the getExpectedResults in a try-catch since it is unknown code
				//and may throw exceptions!
				ResultInfo[] resultInfos = null;
				try{
					resultInfos = resJob.getExpectedResults();
				} catch (Exception ex){
					logger.warning("Could not find expected results for job. Results.getExpectedResults threw exception: " + 
							Helper.getFullStackTrace(ex));
					raiseErrorEvent(ex);
				}
				if (resultInfos != null && resultInfos.length > 0) {
					for (int i = 0; i < resultInfos.length; i++) {
						if (resultInfos[i] != null) //just check for safety
							jobResults.add(resultInfos[i]);
					}
				}
			}
			return jobResults;
		}

		private List<DependencyInfo> getDependenciesToSubmit(Executable job) {

			List<DependencyInfo> jobDependencies = new ArrayList<DependencyInfo>();

			// if the object implements this interface, then depJob won't be null
			if (job instanceof Dependent) {
				Dependent depJob = (Dependent) job;
				DependencyInfo[] interfaceDeps = null;
				
				//call the interface method in a try-catch since it is unknown code and may throw exceptions!
				try{
					interfaceDeps = depJob.getDependencies();
				}catch (Exception ex){
					logger.warning("Could not get additional dependencies for job. " +
							"Dependent.getDependencies() threw an exception: " + Helper.getFullStackTrace(ex));
					raiseErrorEvent(ex); //to inform the client
				}
				if (interfaceDeps != null && interfaceDeps.length > 0) {
					for (int i = 0; i < interfaceDeps.length; i++) {
						if (interfaceDeps[i] != null) //just check for safety
							jobDependencies.add(interfaceDeps[i]);
					}
				}
			}
            
			//1.1 auto-detect and add module dependencies
			try {
				Collection<DependencyInfo> detectedDeps = DependencyResolver.detectDependencies(job.getClass());
				for (DependencyInfo dep : detectedDeps) {
					// if the module is found in the app's common dependencies
					// or
					// user-declared dependencies for this job :ignore it
					// otherwise add it to the job's dependency list
					if (!dependencies.contains(dep) && !jobDependencies.contains(dep))
						jobDependencies.add(dep);
				}
			} catch (Exception e) {
				logger.warning("Automatic dependency resolving failed. Not adding any detected dependencies.");
			}
			
			return jobDependencies;
		}
    }
    
    private class JobMonitorThread implements Runnable {

    	private Semaphore jobsToMonitorSemaphore = new Semaphore(0);
    	
        private List<JobStatusListener> jobStatusListeners;
        private List<JobSubmittedListener> jobSubmittedListeners;
        private List<JobCompletedListener> jobCompletedListeners;
        
        //job statuses (for easy lookup)
        private Map<String, JobStatusInfo> jobStatusLookup;
        
        JobMonitorThread(){
        	this.jobStatusLookup = new HashMap<String, JobStatusInfo>();
        	
            this.jobStatusListeners = new ArrayList<JobStatusListener>();
            this.jobSubmittedListeners = new ArrayList<JobSubmittedListener>();
            this.jobCompletedListeners = new ArrayList<JobCompletedListener>();
        }
        
		void addStatusListener(JobStatusListener listener){
        	synchronized (jobStatusListeners) {
        		jobStatusListeners.add(listener);
    		}
        }

        void removeStatusListener(JobStatusListener listener){
        	synchronized (jobStatusListeners) {
        		jobStatusListeners.remove(listener);
    		}
        }

        void addJobCompletedListener(JobCompletedListener listener){
        	synchronized (jobCompletedListeners) {
        		jobCompletedListeners.add(listener);
    		}
        }

        void removeJobCompletedListener(JobCompletedListener listener){
        	synchronized (jobCompletedListeners) {
        		jobCompletedListeners.remove(listener);
    		}
        }

        void addJobSubmittedListener(JobSubmittedListener listener){
        	synchronized (jobSubmittedListeners) {
        		jobSubmittedListeners.add(listener);
    		}
        }

        void removeJobSubmittedListener(JobSubmittedListener listener){
        	synchronized (jobSubmittedListeners) {
        		jobSubmittedListeners.remove(listener);
    		}
        }

		/**
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			monitorJobs();
		}
		
		private synchronized void resumeThread() {
			if (jobsToMonitorSemaphore.availablePermits() == 0)
				jobsToMonitorSemaphore.release();
		}

	    private void monitorJobs() {

	        int loopWaitTime = 2000;
	        List<String> jobIds = new ArrayList<String>();
	
	        try {
	        	logger.info("Starting monitor jobs thread...");
	            while (!isStopRequested()){
	            	
	                jobIds.clear();
	                jobIds.addAll(jobTable.getJobIds(StatusGroup.ACTIVE));

                    if (jobIds.size() == 0) // is this synchronized with the other locks?
                    {
                        logger.fine("*** Sleeping job monitor because we have no jobs to monitor.");
                        jobsToMonitorSemaphore.acquire();
                        logger.fine("*** Waking job monitor.");
                        if (isStopRequested())
                            continue;
                    }

	                //todoLater: do perf tests to see if copying this entire status thing in every loop is bad
	                //locking the jobStatusLookup in the foreach after GetJobStatus may increase contention with the GetJobStatus public API
	
	                // query running jobs
	                try {
                    	logger.fine("Got " + jobIds.size() + " jobs to monitor");
	                    if (jobIds.size() > 0) {
	                        ArrayOfstring jobIdArray = new ArrayOfstring();
	                        jobIdArray.getStrings().addAll(jobIds);
	                        ArrayOfJobStatusInfo jobStatusArray = manager.getJobStatus(jobIdArray);
	                        JobStatusInfo[] jobStatuses = null;
	                        if (jobStatusArray != null)
	                        	jobStatuses = jobStatusArray.toArray();
	                        	
	                        if (jobStatuses != null) {
//	                            for (int i = 0; i < jobStatuses.length; i++) {
	                        	for (JobStatusInfo newStatus : jobStatuses) {
	                                //check
	                                if (newStatus != null &&  newStatus.getApplicationId().equals(getId()) && 
	                                		jobIds.contains(newStatus.getJobId())) {

	                                	// No need to lock jobStatusLookup - since it is used only here in this thread
	                                	
//	                                	// Get reference to the old StatusInfo for this Job.
	                                    JobStatusInfo oldStatus = null;
	                                    if (jobStatusLookup.containsKey(newStatus.getJobId()))
	                                    	oldStatus = jobStatusLookup.get(newStatus.getJobId());

	                                    // Update the StatusInfo entry for this Job.
	                                    jobStatusLookup.put(newStatus.getJobId(), newStatus);                                    
	                                    
                                        // Remove Completed or Cancelled Jobs so they dont keep getting monitored.
                                        Job remoteJob = jobTable.getJob(newStatus.getJobId());

                                        // Process Completed Jobs
                                        if (newStatus.getStatus() == JobStatus.COMPLETED ||
                                            newStatus.getStatus() == JobStatus.CANCELLED)
                                        {
                                            JobCompletionInfo info = manager.getCompletedJob(remoteJob.getId());
                                            if (info != null)
                                            {
                                                logger.fine("Job " + remoteJob.getId() + " completed.");
                                                Executable completedJob = (Executable)Helper.deserialize(info.getJobInstance());
                                                if (completedJob != null)
                                                    remoteJob.setCompletedInstance(completedJob);
                                                remoteJob.setStartTime(info.getStartTime());
                                                remoteJob.setFinishTime(info.getFinishTime());
                                            }
                                            else
                                            {
                                                raiseErrorEvent(new FrameworkException(
                                                		String.format("Job {0} status is {1}, but could not retrieve completion info.",
                                                    newStatus.getJobId(), newStatus.getStatus())));
                                            }
                                        }

                                        // Handle Status Change. Raise Events to handle Job status changes.
	                                    if (oldStatus == null || !oldStatus.equals(newStatus)) {
                                            // We need to set the status AFTER we have set all the other info because if someone
                                            // is checking for job completion using remoteJob.waitForCompletion() someone may try to access other information
                                            // before it is available.
                                            if (remoteJob.getStatus() != newStatus.getStatus())
                                                jobTable.changeStatus(remoteJob, newStatus.getStatus());

                                            if (newStatus.getStatus() == JobStatus.SUBMITTED) {
                                                raiseJobSubmittedEvent(remoteJob);
                                            } else if (newStatus.getStatus() == JobStatus.COMPLETED) {
                                                raiseJobCompletedEvent(remoteJob);
                                            } 

                                            // TODO discuss: Should 'changed' NOT be raised if the status is Completed or Submitted? The developer 
                                            // should be smart enough not to handle both in an attempt to do the same thing twice.

                                            //the status has changed : raise the event (executes user code, could take long-ish)
                                            raiseJobStatusChangedEvent(newStatus);
                                        }
	                                }                    
	                            }
	                        }
	                    }
	                    
	                    //now look at any jobs for which cancel has been requested while it was being submitted
	                    //and cancel them remotely
	                    for (Job rjob : jobTable.getJobs(StatusGroup.ACTIVE)) 
                        {
							synchronized(rjob)
                            {
								if (rjob.isCancelRequested() && (rjob.getId() != null || !rjob.getId().trim().equals("")))
                                {
                                    manager.abortJob(rjob.getId());
									//in the next monitoring cycle the manager would report a CANCELLED status
									rjob.setCancelRequested(false); //since we've cancelled it
								}
							}
						}	                    
	                } catch (Exception ex) {
	                    raiseErrorEvent(new FrameworkException("Error monitoring jobs. Retrying...", ex));
	                }
	                Thread.sleep(loopWaitTime);
	            }
	            
	        } catch (InterruptedException ix) {
	            logger.fine("MonitorJobs thread aborted.");
	        } catch (ObjectDisposedException ox) {
	            logger.fine("Object disposed when monitoring jobs.");
	        }
	
	        logger.info("MonitorJobs thread ended.");
	    }

	    private void raiseJobStatusChangedEvent(JobStatusInfo statusInfo) {
	    	JobStatusEvent event = new JobStatusEvent(statusInfo);
    		List<JobStatusListener> copy = new ArrayList<JobStatusListener>();
	    	synchronized (jobStatusListeners) {
	    		copy.addAll(jobStatusListeners);
	    	}
    		for (JobStatusListener listener : copy) {
				try {
					listener.onJobStatusChanged(event);
				} catch (Exception ex) {
					logger.warning("Error raising status changed event. Continuing..." + ex.toString());
				}
			}
	    }
	    
	    private void raiseJobCompletedEvent(Job job) {
	    	JobCompletedEvent event = new JobCompletedEvent(job);
    		List<JobCompletedListener> copy = new ArrayList<JobCompletedListener>();
	    	synchronized (jobCompletedListeners) {
	    		copy.addAll(jobCompletedListeners);
			}
    		for (JobCompletedListener listener : copy) {
				try {
					listener.onJobCompleted(event);
				} catch (Exception ex) {
					logger.warning("Error raising job completed event. Continuing..." + ex.toString());
				}
			}
	    }

	    private void raiseJobSubmittedEvent(Job job) {
	    	JobSubmittedEvent event = new JobSubmittedEvent(job);
    		List<JobSubmittedListener> copy = new ArrayList<JobSubmittedListener>();
	    	synchronized (jobSubmittedListeners) {
	    		copy.addAll(jobSubmittedListeners);
			}
    		for (JobSubmittedListener listener : copy) {
				try {
					listener.onJobSubmitted(event);
				} catch (Exception ex) {
					logger.warning("Error raising job submitted event. Continuing..." + ex.toString());
				}
			}
	    }    
    } //JobMonitorThread class    
}
