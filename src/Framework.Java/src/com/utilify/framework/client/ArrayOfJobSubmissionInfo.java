
package com.utilify.framework.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ArrayOfJobSubmissionInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfJobSubmissionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JobSubmissionInfo" type="{http://schemas.utilify.com/2007/09/Client}JobSubmissionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Wrapper class to represent a list of {@link JobSubmissionInfo} objects.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfJobSubmissionInfo", propOrder = {
    "jobSubmissionInfos"
})
public class ArrayOfJobSubmissionInfo {

    @XmlElement(name = "JobSubmissionInfo", nillable = true)
    private List<JobSubmissionInfo> jobSubmissionInfos;

    /**
     * Creates an instance of the <code>ArrayOfJobSubmissionInfo</code> class with an empty list of jobs.
     */
    public ArrayOfJobSubmissionInfo(){
    	jobSubmissionInfos = new ArrayList<JobSubmissionInfo>();
    }
    
    /**
     * Creates an instance of the <code>ArrayOfJobSubmissionInfo</code> class with the specified collection of jobs.
     * @param jobs
     * 	the collection of jobs to submit.
     */
    public ArrayOfJobSubmissionInfo(Collection<JobSubmissionInfo> jobs){
    	this();
    	if (jobs != null){
    		jobSubmissionInfos.addAll(jobs);
    	}
    }
    
    /**
     * Creates an instance of the <code>ArrayOfJobSubmissionInfo</code> class with the specified array of jobs.
     * @param jobs
     * 	the array of jobs to submit.
     */
    public ArrayOfJobSubmissionInfo(JobSubmissionInfo[] jobs){
    	this();
    	if (jobs != null){
    		for (int i = 0; i < jobs.length; i++) {
    			jobSubmissionInfos.add(jobs[i]);
			}
    	}
    }

    /**
     * Gets the list of jobs.
     * @return
     * 	an array of <code>JobSubmissionInfo</code> instances.
     */    
    public JobSubmissionInfo[] toArray(){
    	JobSubmissionInfo[] infos = new JobSubmissionInfo[jobSubmissionInfos.size()];
    	for (int i = 0; i < infos.length; i++) {
			infos[i] = jobSubmissionInfos.get(i);
		}
    	return infos;
    }
}
