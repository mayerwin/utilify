
package com.utilify.framework.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ArrayOfDependencyInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDependencyInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DependencyInfo" type="{http://schemas.utilify.com/2007/09/Client}DependencyInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Wrapper class to represent a list of {@link DependencyInfo} objects.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDependencyInfo", propOrder = {
    "dependencyInfos"
})
public class ArrayOfDependencyInfo {

    @XmlElement(name = "DependencyInfo", nillable = true)
    private List<DependencyInfo> dependencyInfos;

    /**
     * Creates an instance of the <code>ArrayOfDependencyInfo</code> class with an empty list of dependencies.
     */
    public ArrayOfDependencyInfo(){
    	dependencyInfos = new ArrayList<DependencyInfo>();
    }
    
    /**
     * Creates an instance of the <code>ArrayOfDependencyInfo</code> class with the specified collection of dependencies.
     * @param deps
     * 	the collection of dependencies to add.
     */
    public ArrayOfDependencyInfo(Collection<DependencyInfo> deps){
    	this();
    	if (deps != null){
    		dependencyInfos.addAll(deps);
    	}
    }
    
    /**
     * Creates an instance of the <code>ArrayOfDependencyInfo</code> class with the specified array of dependencies.
     * @param deps
     * 	the array of dependencies to add.
     */
    public ArrayOfDependencyInfo(DependencyInfo[] deps){
    	this();
    	if (deps != null){
    		for (int i = 0; i < deps.length; i++) {
				dependencyInfos.add(deps[i]);
			}
    	}
    }
    
    /**
     * Gets the list of dependencies.
     * @return
     *	an array of <code>DependencyInfo</code> instances.
     */    
    public DependencyInfo[] toArray(){    	
    	DependencyInfo[] deps = new DependencyInfo[dependencyInfos.size()];
    	for (int i = 0; i < deps.length; i++) {
			deps[i] = dependencyInfos.get(i);
		}
    	return deps;
    }
}
