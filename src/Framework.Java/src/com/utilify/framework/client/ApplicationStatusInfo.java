
package com.utilify.framework.client;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.ApplicationStatus;
import com.utilify.framework.ErrorMessage;

/*
 * <p>Java class for ApplicationStatusInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationStatusInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompletedJobs" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="NumberOfJobs" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Status" type="{http://schemas.utilify.com/2007/09/Framework}ApplicationStatus"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
/**
 * Represents the status information for an application.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationStatusInfo", propOrder = {
    "applicationId",
    "completedJobs",
    "numberOfJobs",
    "startTime",
    "status"
})
public class ApplicationStatusInfo {

    @XmlElement(name = "ApplicationId", required = true, nillable = true)
    private String applicationId;
    @XmlElement(name = "CompletedJobs")
    private long completedJobs;
    @XmlElement(name = "NumberOfJobs")
    private long numberOfJobs;
    @XmlElement(name = "StartTime", required = true)
    @XmlSchemaType(name = "dateTime")
    private Date startTime;
    @XmlElement(name = "Status", required = true)
    private ApplicationStatus status;

    private ApplicationStatusInfo(){}
    
	/**
     * Gets the application Id.
     * @return
     * 	the Id of the application.
     */
    public String getApplicationId() {
        return applicationId;
    }

    @SuppressWarnings("unused") //used by JAXWS
	private void setApplicationId(String value) {
    	if (value == null || value.trim().length() == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": value");
        this.applicationId = value;
    }

    /**
     * Gets the number of completed jobs for this application.
     * @return
     * 	the number of completed jobs. 
     */
    public long getCompletedJobs() {
        return completedJobs;
    }

    @SuppressWarnings("unused") //used by JAXWS
   private void setCompletedJobs(long value) {
    	if (value < 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeLessThan + " zero : completedJobs");
        if (value > numberOfJobs)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeGreaterThan + " numberOfJobs : completedJobs");
        this.completedJobs = value;
    }

    /**
     * Gets the total number of jobs submitted so far for this application.
     * @return
     * 	the total number of jobs that are part of this application. 
     */
    public long getNumberOfJobs() {
        return numberOfJobs;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setNumberOfJobs(long value) {
    	if (value < 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeLessThan + " zero : numberOfJobs");
        if (value < completedJobs)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeLessThan + " completedJobs : numberOfJobs");
        	
    	this.numberOfJobs = value;
    }

    /**
     * Gets the application start time.
     * @return
     * 	the start time of this application.
     */
    public Date getStartTime() {
        return startTime;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setStartTime(Date value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull+ ": startTime");
        this.startTime = value;
    }

    /**
     * Gets the status of this application.
     * @return
     *	the {@link ApplicationStatus}
     *     
     */
    public ApplicationStatus getStatus() {
        return status;
    }

    @SuppressWarnings("unused") //used by JAXWS
    private void setStatus(ApplicationStatus value) {
    	if (value == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull+ ": status");
        this.status = value;
    }
}
