@javax.xml.bind.annotation.XmlSchema(namespace = "http://schemas.utilify.com/2007/09/Client", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
@javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(value = com.utilify.framework.DateTypeAdapter.class, type = java.util.Date.class)
package com.utilify.framework.client;
