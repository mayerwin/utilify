
package com.utilify.framework;

import javax.xml.ws.WebFault;

/**
 * Thrown when an invalid argument is passed to remote method on the server.
 */
@WebFault(name = ArgumentFault.FaultName, targetNamespace = Namespaces.Framework)
public class InvalidArgumentException extends IllegalArgumentException{

	private static final long serialVersionUID = 2226079426073878207L;
	
	/*
     * Java type that goes as soapenv:Fault detail element. 
     */
    private ArgumentFault faultInfo;

    //needed for JAXWS to figure out if this is the wrapper class for a declared fault
    /**
     * Gets the SOAP fault that caused this exception
     * @return the associated {@link ArgumentFault}
     */
    public ArgumentFault getFaultInfo(){
    	return faultInfo;
    }
    
    private InvalidArgumentException(){
    	super();
    }
    
    /**
	 * Constructs an InvalidArgumentException with the specified detail message and fault.
	 * @param message - the detail message.
	 * @param faultInfo - the fault object that is the cause of this exception.
     */
    public InvalidArgumentException(String message, ArgumentFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }
}
