
package com.utilify.framework.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.utilify.framework.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfResultInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ArrayOfResultInfo");
    private final static QName _QosInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "QosInfo");
    private final static QName _DependencyQueryInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "DependencyQueryInfo");
    private final static QName _JobSubmissionInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "JobSubmissionInfo");
    private final static QName _ResultContent_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ResultContent");
    private final static QName _ArrayOfJobStatusInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ArrayOfJobStatusInfo");
    private final static QName _ArrayOfDependencyStatusInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ArrayOfDependencyStatusInfo");
    private final static QName _DependencyContent_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "DependencyContent");
    private final static QName _ArrayOfJobSubmissionInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ArrayOfJobSubmissionInfo");
    private final static QName _ResultInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ResultInfo");
    private final static QName _ApplicationSubmissionInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ApplicationSubmissionInfo");
    private final static QName _ArrayOfResultStatusInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ArrayOfResultStatusInfo");
    private final static QName _ResultStatusInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ResultStatusInfo");
    private final static QName _ArrayOfDependencyInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ArrayOfDependencyInfo");
    private final static QName _DependencyInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "DependencyInfo");
    private final static QName _JobStatusInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "JobStatusInfo");
    private final static QName _ApplicationStatusInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "ApplicationStatusInfo");
    private final static QName _DependencyStatusInfo_QNAME = new QName("http://schemas.utilify.com/2007/09/Client", "DependencyStatusInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.utilify.framework.client
     * 
     */
    public ObjectFactory() {
    }

//    /**
//     * Create an instance of {@link DependencyStatusInfo }
//     * 
//     */
//    public DependencyStatusInfo createDependencyStatusInfo() {
//        return new DependencyStatusInfo();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfResultStatusInfo }
//     * 
//     */
//    public ArrayOfResultStatusInfo createArrayOfResultStatusInfo() {
//        return new ArrayOfResultStatusInfo();
//    }
//
//    /**
//     * Create an instance of {@link ResultInfo }
//     * 
//     */
//    public ResultInfo createResultInfo() {
//        return new ResultInfo();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfResultInfo }
//     * 
//     */
//    public ArrayOfResultInfo createArrayOfResultInfo() {
//        return new ArrayOfResultInfo();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfJobSubmissionInfo }
//     * 
//     */
//    public ArrayOfJobSubmissionInfo createArrayOfJobSubmissionInfo() {
//        return new ArrayOfJobSubmissionInfo();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfJobStatusInfo }
//     * 
//     */
//    public ArrayOfJobStatusInfo createArrayOfJobStatusInfo() {
//        return new ArrayOfJobStatusInfo();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfDependencyStatusInfo }
//     * 
//     */
//    public ArrayOfDependencyStatusInfo createArrayOfDependencyStatusInfo() {
//        return new ArrayOfDependencyStatusInfo();
//    }
//
//    /**
//     * Create an instance of {@link JobStatusInfo }
//     * 
//     */
//    public JobStatusInfo createJobStatusInfo() {
//        return new JobStatusInfo();
//    }
//
//    /**
//     * Create an instance of {@link ApplicationSubmissionInfo }
//     * 
//     */
//    public ApplicationSubmissionInfo createApplicationSubmissionInfo() {
//        return new ApplicationSubmissionInfo();
//    }
//
//    /**
//     * Create an instance of {@link ArrayOfDependencyInfo }
//     * 
//     */
//    public ArrayOfDependencyInfo createArrayOfDependencyInfo() {
//        return new ArrayOfDependencyInfo();
//    }
//
//    /**
//     * Create an instance of {@link ResultStatusInfo }
//     * 
//     */
//    public ResultStatusInfo createResultStatusInfo() {
//        return new ResultStatusInfo();
//    }
//
//    /**
//     * Create an instance of {@link DependencyInfo }
//     * 
//     */
//    public DependencyInfo createDependencyInfo() {
//        return new DependencyInfo();
//    }
//
//    /**
//     * Create an instance of {@link JobSubmissionInfo }
//     * 
//     */
//    public JobSubmissionInfo createJobSubmissionInfo() {
//        return new JobSubmissionInfo();
//    }
//
//    /**
//     * Create an instance of {@link QosInfo }
//     * 
//     */
//    public QosInfo createQosInfo() {
//        return new QosInfo();
//    }
//
//    /**
//     * Create an instance of {@link DependencyContent }
//     * 
//     */
//    public DependencyContent createDependencyContent() {
//        return new DependencyContent();
//    }
//
//    /**
//     * Create an instance of {@link ApplicationStatusInfo }
//     * 
//     */
//    public ApplicationStatusInfo createApplicationStatusInfo() {
//        return new ApplicationStatusInfo();
//    }
//
//    /**
//     * Create an instance of {@link ResultContent }
//     * 
//     */
//    public ResultContent createResultContent() {
//        return new ResultContent();
//    }
//
//    /**
//     * Create an instance of {@link DependencyQueryInfo }
//     * 
//     */
//    public DependencyQueryInfo createDependencyQueryInfo() {
//        return new DependencyQueryInfo();
//    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ArrayOfResultInfo")
    public JAXBElement<ArrayOfResultInfo> createArrayOfResultInfo(ArrayOfResultInfo value) {
        return new JAXBElement<ArrayOfResultInfo>(_ArrayOfResultInfo_QNAME, ArrayOfResultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QosInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "QosInfo")
    public JAXBElement<QosInfo> createQosInfo(QosInfo value) {
        return new JAXBElement<QosInfo>(_QosInfo_QNAME, QosInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DependencyQueryInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "DependencyQueryInfo")
    public JAXBElement<DependencyQueryInfo> createDependencyQueryInfo(DependencyQueryInfo value) {
        return new JAXBElement<DependencyQueryInfo>(_DependencyQueryInfo_QNAME, DependencyQueryInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobSubmissionInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "JobSubmissionInfo")
    public JAXBElement<JobSubmissionInfo> createJobSubmissionInfo(JobSubmissionInfo value) {
        return new JAXBElement<JobSubmissionInfo>(_JobSubmissionInfo_QNAME, JobSubmissionInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultContent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ResultContent")
    public JAXBElement<ResultContent> createResultContent(ResultContent value) {
        return new JAXBElement<ResultContent>(_ResultContent_QNAME, ResultContent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfJobStatusInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ArrayOfJobStatusInfo")
    public JAXBElement<ArrayOfJobStatusInfo> createArrayOfJobStatusInfo(ArrayOfJobStatusInfo value) {
        return new JAXBElement<ArrayOfJobStatusInfo>(_ArrayOfJobStatusInfo_QNAME, ArrayOfJobStatusInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDependencyStatusInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ArrayOfDependencyStatusInfo")
    public JAXBElement<ArrayOfDependencyStatusInfo> createArrayOfDependencyStatusInfo(ArrayOfDependencyStatusInfo value) {
        return new JAXBElement<ArrayOfDependencyStatusInfo>(_ArrayOfDependencyStatusInfo_QNAME, ArrayOfDependencyStatusInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DependencyContent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "DependencyContent")
    public JAXBElement<DependencyContent> createDependencyContent(DependencyContent value) {
        return new JAXBElement<DependencyContent>(_DependencyContent_QNAME, DependencyContent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfJobSubmissionInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ArrayOfJobSubmissionInfo")
    public JAXBElement<ArrayOfJobSubmissionInfo> createArrayOfJobSubmissionInfo(ArrayOfJobSubmissionInfo value) {
        return new JAXBElement<ArrayOfJobSubmissionInfo>(_ArrayOfJobSubmissionInfo_QNAME, ArrayOfJobSubmissionInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ResultInfo")
    public JAXBElement<ResultInfo> createResultInfo(ResultInfo value) {
        return new JAXBElement<ResultInfo>(_ResultInfo_QNAME, ResultInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationSubmissionInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ApplicationSubmissionInfo")
    public JAXBElement<ApplicationSubmissionInfo> createApplicationSubmissionInfo(ApplicationSubmissionInfo value) {
        return new JAXBElement<ApplicationSubmissionInfo>(_ApplicationSubmissionInfo_QNAME, ApplicationSubmissionInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResultStatusInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ArrayOfResultStatusInfo")
    public JAXBElement<ArrayOfResultStatusInfo> createArrayOfResultStatusInfo(ArrayOfResultStatusInfo value) {
        return new JAXBElement<ArrayOfResultStatusInfo>(_ArrayOfResultStatusInfo_QNAME, ArrayOfResultStatusInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultStatusInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ResultStatusInfo")
    public JAXBElement<ResultStatusInfo> createResultStatusInfo(ResultStatusInfo value) {
        return new JAXBElement<ResultStatusInfo>(_ResultStatusInfo_QNAME, ResultStatusInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDependencyInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ArrayOfDependencyInfo")
    public JAXBElement<ArrayOfDependencyInfo> createArrayOfDependencyInfo(ArrayOfDependencyInfo value) {
        return new JAXBElement<ArrayOfDependencyInfo>(_ArrayOfDependencyInfo_QNAME, ArrayOfDependencyInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DependencyInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "DependencyInfo")
    public JAXBElement<DependencyInfo> createDependencyInfo(DependencyInfo value) {
        return new JAXBElement<DependencyInfo>(_DependencyInfo_QNAME, DependencyInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobStatusInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "JobStatusInfo")
    public JAXBElement<JobStatusInfo> createJobStatusInfo(JobStatusInfo value) {
        return new JAXBElement<JobStatusInfo>(_JobStatusInfo_QNAME, JobStatusInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationStatusInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "ApplicationStatusInfo")
    public JAXBElement<ApplicationStatusInfo> createApplicationStatusInfo(ApplicationStatusInfo value) {
        return new JAXBElement<ApplicationStatusInfo>(_ApplicationStatusInfo_QNAME, ApplicationStatusInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DependencyStatusInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Client", name = "DependencyStatusInfo")
    public JAXBElement<DependencyStatusInfo> createDependencyStatusInfo(DependencyStatusInfo value) {
        return new JAXBElement<DependencyStatusInfo>(_DependencyStatusInfo_QNAME, DependencyStatusInfo.class, null, value);
    }

}
