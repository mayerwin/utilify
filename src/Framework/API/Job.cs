using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Utilify.Platform;

namespace Utilify.Framework
{
    /// <summary>
    /// Represents the client-side view of an executable job in system. A job is the smallest unit of work that can be independently scheduled and executed
    /// remotely by the Utilify Platform.
    /// </summary>
    public class Job
    {
        private object completedJobLock = new object();

        private string id;
        private JobStatus status;
        private IExecutable job;
        private IExecutable completedJob;
        private Exception executionException;

        private DateTime clientStartTime;

        private DateTime submittedTime;
        private DateTime scheduledTime;

        private DateTime startTime;
        private DateTime finishTime;

        private DateTime clientFinishTime;

        private TimeSpan cpuTime;
        private int numMappings;

        private string executionHost;
        
        private List<DependencyInfo> dependencies;

        /// <summary>
        /// Initializes a new instance of the <see cref="Job"/> class from the specified instance of an <see cref="IExecutable" /> implementation.
        /// </summary>
        /// <param name="job">The job.</param>
        public Job(IExecutable job)
        {
            if (job == null)
                throw new ArgumentNullException("job");
            this.job = job;
            // not setting user defined dependencies here. do that during submission.
            dependencies = new List<DependencyInfo>();

            this.clientStartTime = DateTime.UtcNow;
        }

        //no need to lock here, since this is only being read by possibly different threads - never written to.
        /// <summary>
        /// Gets the initial instance of the job that was submitted to the Manager.
        /// </summary>
        /// <value>The initial instance.</value>
        public IExecutable InitialInstance
        {
            get { return job; } 
        }

        /// <summary>
        /// Gets the completed instance of the job. The completed instance is available only after job execution is completed.
        /// Until then, this property returns a null value.
        /// </summary>
        /// <value>The completed instance.</value>
        public IExecutable CompletedInstance
        {
            get 
            {
                IExecutable copy = null;
                lock (completedJobLock) //can't lock completedJob itself: it may be null
                {
                    copy = completedJob;
                }
                return copy;
            }
            internal set 
            {
                lock (completedJobLock)
                {
                    completedJob = value;
                }
            }
        }

        /// <summary>
        /// Gets the exception that occurred during remote execution of the Job.
        /// </summary>
        public Exception ExecutionException
        {
            get 
            {
                Exception exCopy = null;
                lock (completedJobLock)
                {
                    exCopy = executionException;
                }
                return exCopy;
            }
            internal set 
            {
                lock (completedJobLock)
                {
                    executionException = value;
                }
            }
        }

        /// <summary>
        /// Gets the job status.
        /// </summary>
        /// <value>The status.</value>
        public JobStatus Status
        {
            get 
            {
                JobStatus copy = JobStatus.UnInitialized;
                lock (this)
                {
                    copy = status;
                }
                return copy;
            }
            internal set 
            {
                lock (this)
                {
                    status = value;
                }
            }
        }

        /// <summary>
        /// Gets the job id.
        /// </summary>
        /// <value>The id.</value>
        public string Id
        {
            get 
            {
                string copy = null;
                lock (this) //simpler to lock entire object - since this is an important property to get/set
                {
                    copy = id;
                }
                return copy;
            }
            internal set 
            {
                lock (this)
                {
                    id = value;
                }
            }
        }

        /// <summary>
        /// Gets the start time. The start time is the UTC datetime value when the job began execution on an Executor.
        /// </summary>
        /// <value>The start time.</value>
        public DateTime StartTime
        {
            get { return startTime; }
            internal set { startTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the finish time. The finish time is the UTC datetime value when the job finished execution on an Executor. 
        /// </summary>
        /// <value>The finish time.</value>
        public DateTime FinishTime
        {
            get { return finishTime; }
            internal set { finishTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the scheduled time. The scheduled time is the UTC datetime value when the job was scheduled to be run by the Manager.
        /// </summary>
        /// <value>The scheduled time.</value>
        public DateTime ScheduledTime
        {
            get { return scheduledTime; }
            internal set { scheduledTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the submitted time. The submitted time is the UTC datetime value when the job was submitted to the Manager.
        /// </summary>
        /// <value>The submitted time.</value>
        public DateTime SubmittedTime
        {
            get { return submittedTime; }
            internal set { submittedTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the name of the host where the job was executed.
        /// </summary>
        /// <value>The execution hostname.</value>
        public string ExecutionHost
        {
            get { return executionHost; }
            internal set { executionHost = value; }
        }

        /// <summary>
        /// Gets or sets the number of times this job was scheduled, before it was finally executed.
        /// </summary>
#if TEST 
        public  
#else  
        internal 
#endif
        int NumMappings
        {
            get { return numMappings; }
#if TEST
            internal 
#endif
            set { numMappings = value; }
        }

        /// <summary>
        /// Gets the start time seen at the client
        /// </summary>
#if TEST
        public
#else  
        internal 
#endif
        DateTime ClientStartTime
        {
            get { return clientStartTime; }
        }

        /// <summary>
        /// Gets or sets the finish time seen at the client.
        /// </summary>
#if TEST
        public
#else  
        internal 
#endif
        DateTime ClientFinishTime
        {
            get { return clientFinishTime; }
#if TEST
            internal 
#endif
            set { clientFinishTime = Helper.MakeUtc(value); }
        }

        /// <summary>
        /// Gets the cpu time taken by this job.
        /// This value is available only after the job execution is completed.
        /// Before the job is complete, the value remains as TimeSpan.MinValue.
        /// </summary>
        /// <value>The cpu time.</value>
        public TimeSpan CpuTime
        {
            get { return cpuTime; }
            internal set { cpuTime = value; }
        }

        internal void SetDependencies(IEnumerable<DependencyInfo> dependencies)
        {
            if (dependencies != null)
            {
                this.dependencies.Clear();
                this.dependencies.AddRange(dependencies);
            }
        }

 
        // This is used by the JobSubmitter
        // No need to LOCK, since this is meant to be used only in the JobSubmitter thread.
        // since we don't have a status that says "Submitted and dependencies sent".

        /// <summary>
        /// Specifies whether the job's dependencies have been sent (if needed) successfully.
        /// This property is for internal use only.
        /// </summary>
        internal bool SentDependencies { get; set; }

        /// <summary>
        /// Gets the list of dependencies for this job.
        /// </summary>
        /// <returns></returns>
        public DependencyInfo[] GetDependencies()
        {
            return dependencies.ToArray();
        }

        /// <summary>
        /// Blocks the current thread till the job is complete.
        /// </summary>
        public void WaitForCompletion()
        {
            WaitForCompletion(TimeSpan.MaxValue);
        }

        /// <summary>
        /// Blocks the current thread till the job is complete.
        /// </summary>
        /// <param name="timeout">The maximum time to wait before returning.</param>
        public void WaitForCompletion(TimeSpan timeout)
        {
            double totalInterval = timeout.TotalMilliseconds;
            double waitedSoFar = 0;

            int waitInterval = 500;
            while (waitedSoFar <= totalInterval)
            {
                JobStatus s = Status;
                if (s == JobStatus.Completed
                        || s == JobStatus.Cancelled)
                    break;
                Thread.Sleep(waitInterval);
                waitedSoFar += waitInterval;
            }
        }

        private bool isCancelRequested = false;
        private object isCancelRequestedLock = new object();
        internal bool IsCancelRequested
        {
            get 
            {
                bool isReq = false;
                lock (isCancelRequestedLock)
                {
                    isReq = isCancelRequested;
                }
                return isReq;
            }
            set 
            {
                lock (isCancelRequestedLock)
                {
                    isCancelRequested = value;
                }
            }
        }
    }
}
