using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Utilify.Platform;
using Utilify.Framework;
using Utilify.Platform.Execution.Shared;

namespace Utilify.Platform.Execution
{
    public class ClrExecutor : MarshalByRefObject, IExecutor
    {
        Logger logger = new Logger();

        #region IExecutor Members

        public event EventHandler<ExecutionCompleteEventArgs> ExecutionComplete;

        public void ExecuteTask(object info)
        {
            JobExecutionInfo execInfo = (JobExecutionInfo)info;

            //create the ExecutionContext
            ExecutionContext context = ExecutionContext.Create(execInfo.JobId, execInfo.JobDirectory, 
                new StringAppender(), new StringAppender());

            ExecuteTaskWithContext(execInfo, context); //.JobId, execInfo.ApplicationId, execInfo.JobInstance, execInfo.JobDirectory);

            //remove the context from the existing list Execu
            //context.Destroy();
            context = null;
        }

        #endregion

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "We need to catch any exceptions caused by executing user code, so that it can be passed back to the user.")]
        private void ExecuteTaskWithContext(JobExecutionInfo execInfo, ExecutionContext context) //String jobId, String applicationId, byte[] jobInstance, String jobDirectory)
        {
            Exception executionException = null;
            byte[] finalJobInstance = null;
            StringAppender executionLog = context.Log;
            StringAppender executionError = context.Error;
            DateTime start = DateTime.Now; //default
            DateTime finish = DateTime.MaxValue; //default
            try
            {
                if (execInfo == null)
                    throw new ArgumentNullException("execInfo");

                if (execInfo.JobInstance == null || execInfo.JobInstance.Length == 0)
                    throw new ArgumentException("Job instance cannot be null or empty", "execInfo");
                if (string.IsNullOrEmpty(execInfo.JobDirectory))
                    throw new ArgumentException("Job directory cannot be null or empty", "execInfo");

                //before running anything - just set the final instance to be the same as the initial. So that we don't ever have a null instance.
                finalJobInstance = execInfo.JobInstance;

                executionLog.AppendLine("Environment Current Directory: " + Environment.CurrentDirectory);
                executionLog.AppendLine("Execution Context WorkingDirectory: " + context.WorkingDirectory);
                executionLog.AppendLine("AppDomain FriendlyName: " + AppDomain.CurrentDomain.FriendlyName);
                executionLog.AppendLine("AppDomain BaseDirectory: " + AppDomain.CurrentDomain.BaseDirectory);
                executionLog.AppendLine("AppDomain DynamicDirectory: " + AppDomain.CurrentDomain.DynamicDirectory);
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                executionLog.AppendLine("Assemblies Loaded (ClrExecutor): " + assemblies.Length);
                foreach (Assembly asm in assemblies)
                {
                    executionLog.AppendLine("Assembly (ClrExecutor): full-name= " + asm.FullName + " / name=" + asm.GetName().Name);
                }

                // Deserialise the Job and cast to IExecutable
                executionLog.AppendLine("Deserialising job...");
                object deserialisedJob = SerializationHelper.Deserialize(execInfo.JobInstance);
                IExecutable exec = (IExecutable)deserialisedJob;
                executionLog.AppendLine("Deserialised job. Executing...");
                logger.Debug("Executing Job " + execInfo.JobId + " (ClrExecutor)");

                // Execute Job : for now include only the actual method execution time in the cpuTime:
                start = DateTime.Now;
                exec.Execute(context);
                finish = DateTime.Now; 

                executionLog.AppendLine("Executed job. Serialising it again...");
                // Serialise the completed Job
                byte[] serialisedJob = SerializationHelper.Serialize(exec);
                executionLog.AppendLine("Serialized job. Returning final instance...");
                // Assign the final instance
                finalJobInstance = serialisedJob;
            }
            catch (Exception e)
            {
                if (finish == DateTime.MaxValue)
                    finish = DateTime.Now; //in case we don't get as far as finishing the execution. 

                executionError.AppendLine("Error executing job: " + e.ToString());
                executionException = e;
                logger.Debug("Error executing job: " + e.ToString());
            }
            finally
            {
                byte[] executionExceptionInstance = null;
                if (executionException != null)
                {
                    try
                    {
                        executionExceptionInstance = SerializationHelper.Serialize(executionException);
                    }
                    catch (Exception ex)
                    {
                        executionError.AppendLine("Could not serialize execution exception because of error : " + ex.Message);
                        executionError.AppendLine("Execution exception stack trace follows: ");
                        executionError.AppendLine(executionException.ToString());
                    }
                }

                //todoDecide: decide if we should include the serialisation time etc in the cpuTime?
                long cpuTime = (long)((finish - start).TotalMilliseconds);
                // Raise Completed Event.
                ExecutionCompleteEventArgs completeArgs = 
                    new ExecutionCompleteEventArgs(
                    execInfo.JobId,
                    execInfo.ApplicationId, 
                    finalJobInstance, 
                    executionLog, 
                    executionError, 
                    executionExceptionInstance,
                    cpuTime);
                
                logger.Debug("Raising execution complete event. Exception is null? " + (completeArgs.ExecutionException == null));

                EventHelper.RaiseEvent<ExecutionCompleteEventArgs>(ExecutionComplete, this, completeArgs);
            }
        }
    }
}
