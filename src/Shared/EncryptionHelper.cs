using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;

namespace Utilify.Platform
{
    internal static class EncryptionHelper
    {
        #region DPAPI (En-/De-)cryption helpers
        //using local machine protection, since the user running the process may change

        /// <summary>
        /// Encrypts the input byte array using DPAPI (using an auto-generated entropy which is stored for later use)
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="useEntropy">if set to <c>true</c> [use entropy].</param>
        /// <param name="scope">The scope.</param>
        /// <returns>The encrypted byte array</returns>
        internal static byte[] Encrypt(byte[] input, bool useEntropy, DataProtectionScope scope)
        {
            if (input == null)
                throw new ArgumentNullException("Input cannot be null");
            if (input.Length == 0)
                throw new ArgumentException("Input cannot be empty");

            byte[] entropy = null;
            if (useEntropy)
                entropy = GetEntropy();

            return ProtectedData.Protect(input, entropy, scope);
        }

        /// <summary>
        /// Decrypts the input byte array using DPAPI and the stored entropy
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="useEntropy">if set to <c>true</c> [use entropy].</param>
        /// <param name="scope">The scope.</param>
        /// <returns>The decypted byte array</returns>
        internal static byte[] Decrypt(byte[] input, bool useEntropy, DataProtectionScope scope)
        {
            if (input == null)
                throw new ArgumentNullException("Input cannot be null");
            if (input.Length == 0)
                throw new ArgumentException("Input cannot be empty");

            byte[] entropy = null;
            if (useEntropy)
                entropy = GetEntropy();

            return ProtectedData.Unprotect(input, entropy, scope);
        }

        //just to get an un-intelligible file name 
        private static string GetEntropyFileName()
        {
            string entropyFileName = typeof(EncryptionHelper).Assembly.FullName + ".tmp";
            char[] chars = entropyFileName.ToCharArray();
            StringBuilder sb = new StringBuilder();
            foreach (char c in chars)
            {
                sb.Append(c.ToString());
            }
            entropyFileName = sb.ToString(); //replace the file name with the string representation of the chars in it ;)
            return entropyFileName;
        }

        private static byte[] inMemEntropy = null;
        private static byte[] GetEntropy()
        {
            //read the file only once : better perf
            if (inMemEntropy != null)
                return inMemEntropy;

            //some un-intelligible filename to use to store the entropy bytes
            string entropyFileName = GetEntropyFileName();

            byte[] entropy = new byte[32];

            IsolatedStorageFile store = null;
            IsolatedStorageFileStream file = null;
            try
            {
                //first check if we have the entropy persisted already
                store = IsolatedStorageFile.GetUserStoreForApplication();

                FileMode mode = FileMode.Open;
                string[] filesFound = store.GetFileNames(entropyFileName);
                bool found = (filesFound == null || filesFound.Length == 0);
                if (!found)
                    mode = FileMode.Create; //create it

                //logger.Debug("Found entropy file: " + found);

                file = new IsolatedStorageFileStream(entropyFileName,
                    mode, FileAccess.ReadWrite, FileShare.None, store);
                if (found)
                {
                    file.Read(entropy, 0, entropy.Length);
                }
                else
                {
                    //create entropy
                    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                    rng.GetBytes(entropy);

                    //persist it
                    file.Write(entropy, 0, entropy.Length);
                }
            }
            finally
            {
                if (file != null)
                    file.Close();
                if (store != null)
                    store.Close();
            }

            //cache in memory
            inMemEntropy = entropy;

            return entropy;
        }

        internal static string Encrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            //encrypt, convert to base64 string (so it can be round-tripped
            byte[] unEncryptedPwd = Encoding.UTF8.GetBytes(value);
            byte[] encrypted = Encrypt(unEncryptedPwd, false,
                System.Security.Cryptography.DataProtectionScope.LocalMachine);
            return Convert.ToBase64String(encrypted);
        }

        internal static string Decrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            //try to decode from Base64 string, and decrypt
            if (!string.IsNullOrEmpty(value))
            {
                byte[] encryptedPwd = Convert.FromBase64String(value);
                byte[] decryptedPwd = Decrypt(encryptedPwd, false,
                    System.Security.Cryptography.DataProtectionScope.LocalMachine);
                value = Encoding.UTF8.GetString(decryptedPwd);
            }
            return value;
        }

        #endregion
    }
}
