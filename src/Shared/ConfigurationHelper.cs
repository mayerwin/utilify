using System;
using System.Configuration;

namespace Utilify.Platform.Configuration
{
    internal static class ConfigurationHelper
    {
        /// <summary>
        /// Gets the connection settings for the first &lt;client&gt; element defined in the app.config.
        /// </summary>
        /// <returns></returns>
        internal static ConnectionSettings GetConnectionSettings()
        {   
            //set service type = Default to get the first <client> defined in the app.config
            return GetConnectionSettings(ServiceType.Default, ConfigurationUserLevel.PerUserRoamingAndLocal, false);
        }

        /// <summary>
        /// Gets the settings from the application's configuration file for the current user.
        /// </summary>
        /// <returns></returns>
        internal static ConnectionSettings GetConnectionSettings(ServiceType type)
        {
            return GetConnectionSettings(type, ConfigurationUserLevel.PerUserRoamingAndLocal, false);
        }

        internal static ConnectionSettings GetConnectionSettings(ServiceType type, ConfigurationUserLevel level, bool reload)
        {
            ConnectionSettings settings = null;

            //check if we need to reload the section.
            if (reload)
                ConfigurationManager.RefreshSection(ClientSection.SectionName);

            System.Configuration.Configuration config =
                ConfigurationManager.OpenExeConfiguration(level);
            
            if (config != null)
            {
                //if we are not looking at the application config, 
                //and there was no file for the specific user, try the app-level
                if (level != ConfigurationUserLevel.None && !config.HasFile)
                {
                    System.Configuration.Configuration appConfig = 
                        ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    if (appConfig != null && appConfig.HasFile)
                        config = appConfig;
                }

                settings = new ConnectionSettings();

                ClientSection section =
                    (ClientSection)config.GetSection(ClientSection.SectionName);

                //if we can't find it:
                if (section == null)
                    throw new ConfigurationErrorsException(string.Format("Could not find configuration section '{0}'",
                        ClientSection.SectionName));

                //do our custom validation
                (section as IConfigurationElement).Validate();
                ClientElement client = section.Clients.LookupClient(type);
                if (client != null)
                    (settings as IConfigurationSettings<ClientElement>).LoadConfiguration(client);
                
            }
            else
            {
                throw new ConfigurationErrorsException("Could not open configuration settings for current user.");
            }

            return settings;
        }

        internal static void SaveConnectionSettings(ConnectionSettings settings)
        {
            if (settings == null)
                throw new ArgumentNullException("settings");

            SaveConnectionSettings(settings, ConfigurationUserLevel.PerUserRoamingAndLocal);
        }

        internal static void SaveConnectionSettings(ConnectionSettings settings, ConfigurationUserLevel level)
        {
            if (settings == null)
                throw new ArgumentNullException("settings");

            System.Configuration.Configuration config = 
                ConfigurationManager.OpenExeConfiguration(level);

            if (config != null)
            {
                ClientSection section = (ClientSection)config.GetSection(ClientSection.SectionName);
                if (section == null)
                {
                    section = new ClientSection();
                    config.Sections.Add(ClientSection.SectionName, section);
                    switch (level)
                    {
                        case ConfigurationUserLevel.PerUserRoaming:
                            section.SectionInformation.AllowExeDefinition = 
                                ConfigurationAllowExeDefinition.MachineToRoamingUser;
                            break;

                        case ConfigurationUserLevel.PerUserRoamingAndLocal:
                            section.SectionInformation.AllowExeDefinition =
                                ConfigurationAllowExeDefinition.MachineToLocalUser;
                            break;
                    }                    
                }

                var configSettings = settings as IConfigurationSettings<ClientElement>;
                //foreach (string type in Enum.GetNames(typeof(ServiceType)))
                //{
                    var element = section.Clients.LookupClient(ServiceType.Default);
                    if (element != null)
                        configSettings.UpdateConfigurationElement(ref element);                    
                //Todo:fix : config cant be saved at the moment
                //config.Save(ConfigurationSaveMode.Modified, true);
            }
            else
            {
                throw new ConfigurationErrorsException("Could not open configuration settings for current user.");
            }
        }
    }
}
