using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;

namespace Utilify.Platform
{
    internal static class IOHelper
    {
        private const string companyName = Constants.CompanyName;

        #region Directory / path helpers

        internal static string GetDataDirectory(bool createIfNeeded)
        {
            System.Reflection.Assembly caller = System.Reflection.Assembly.GetCallingAssembly();
            string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string dataDir = Path.Combine(
                                appDataDir,
                                Path.Combine(
                                    companyName,
                                    caller.GetName().Name)
                                    );
            if (createIfNeeded && !Directory.Exists(dataDir))
            {
                Directory.CreateDirectory(dataDir); //todoLater set restrictive ACL permissions
            }
            return dataDir;
        }

        internal static string GetTempDirectory(bool createIfNeeded)
        {
            System.Reflection.Assembly caller = System.Reflection.Assembly.GetCallingAssembly();
            string baseTempDir = Path.GetTempPath();
            string tempDir = Path.Combine(
                                baseTempDir,
                                Path.Combine(
                                    companyName,
                                    caller.GetName().Name)
                                    );
            if (createIfNeeded && !Directory.Exists(tempDir))
            {
                Directory.CreateDirectory(tempDir); //todoLater set restrictive ACL permissions
            }
            return tempDir;
        }

        #endregion

        #region Simple File I/O helpers

        internal static string GetFriendlySizeFormat(long bytes)
        {
            double mb = (double)bytes / (double)Constants.Mega;
            return mb.ToString("0.00 MB");
        }

        internal static byte[] ReadFile(string fullPath)
        {
            if (fullPath == null)
                throw new ArgumentNullException("fullPath");

            if (fullPath.Trim().Length == 0)
                throw new ArgumentException("File path cannot be empty", "fullPath");

            byte[] buffer;
            using (FileStream fs = File.OpenRead(fullPath))
            {
                buffer = new byte[fs.Length];
                //looks like this could fail sometimes: when all the file data can't be read at once.
                fs.Read(buffer, 0, (int)(fs.Length));
                /*
                                int remaining = buffer.Length;
                                int offset = 0;
                                int read = 0;
                                while (read >= 0)
                                {
                                    read = fs.Read(buffer, offset, remaining);
                                    if (read >= 0)
                                    {
                                        remaining -= read;
                                        offset += read;
                                    }
                                    else
                                        break;
                                }
                 */
            }
            return buffer;
        }

        internal static void WriteFile(string fullPath, string content)
        {
            if (fullPath == null)
                throw new ArgumentNullException("fullPath");

            if (fullPath.Trim().Length == 0)
                throw new ArgumentException("File path cannot be empty", "fullPath");

            if (content == null)
                throw new ArgumentNullException("content");

            using (StreamWriter sw = File.CreateText(fullPath))
            {
                sw.Write(content);
            }
        }

        /// <summary>
        /// Writes the data in the given stream to the specified file.
        /// The stream is closed after the data is written.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="stream"></param>
        /// <param name="bufferSize"></param>
        internal static void WriteFile(string filePath, Stream stream, int bufferSize)
        {
            byte[] buffer = new byte[bufferSize];

            using (FileStream fs = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write))
            {
                do
                {
#if DEBUG
                    //System.Threading.Thread.Sleep(100);
#endif

                    // read bytes from input stream
                    int bytesRead = stream.Read(buffer, 0, bufferSize);
                    if (bytesRead <= 0) break;

                    // write bytes to output stream
                    fs.Write(buffer, 0, bytesRead);
                } while (true);

                fs.Close();
            }
        }


        #endregion
    }
}
