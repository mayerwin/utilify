#if !MONO

using System;
using System.ServiceModel;
using Utilify.Platform.Contract;

namespace Utilify.Platform
{
    internal partial class JobManagerProxy : WCFProxyBase<IJobManager>, IJobManager
    {
        #region Constructors
        public JobManagerProxy() : base()
        {}

        public JobManagerProxy(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion

        #region IJobManager Members

        ApplicationSubmissionInfo IJobManager.GetApplication(string executorId, string applicationId)
        {
            try
            {
                return base.Channel.GetApplication(executorId, applicationId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        JobSubmissionInfo[] IJobManager.GetJobs(string executorId)
        {
            try
            {
                return base.Channel.GetJobs(executorId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        public string[] GetCancelledJobs(string executorId)
        {
            try
            {
                return base.Channel.GetCancelledJobs(executorId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        DependencyContent IJobManager.GetDependency(string dependencyId)
        {
            try
            {
                return base.Channel.GetDependency(dependencyId);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IJobManager.SendResult(ResultContent content)
        {
            try
            {
                base.Channel.SendResult(content);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        public void SendCompletedJob(JobCompletionInfo info)
        {
            try
            {
                base.Channel.SendCompletedJob(info);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        public void UpdateCurrentJobs(JobExecutionInfo info)
        {
            try
            {
                base.Channel.UpdateCurrentJobs(info);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        #endregion

    }
}
#endif