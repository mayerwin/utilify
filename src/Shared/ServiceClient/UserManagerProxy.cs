#if !MONO
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Utilify.Platform
{
    internal partial class UserManagerProxy : WCFProxyBase<IUserManager>, IUserManager
    {
        #region Constructors
        public UserManagerProxy() { }

        public UserManagerProxy(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion

        #region IUserManager Members

        Utilify.Platform.Security.UserInfo IUserManager.Authenticate()
        {
            Utilify.Platform.Security.UserInfo userInfo;

            try
            {
                userInfo = base.Channel.Authenticate();
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return userInfo;
        }

        Utilify.Platform.Security.UserInfo[] IUserManager.GetUsers()
        {
            List<Utilify.Platform.Security.UserInfo> infos = new List<Utilify.Platform.Security.UserInfo>();
            try
            {
                infos.AddRange(base.Channel.GetUsers());
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        Utilify.Platform.Security.GroupInfo[] IUserManager.GetGroups()
        {
            List<Utilify.Platform.Security.GroupInfo> infos = new List<Utilify.Platform.Security.GroupInfo>();
            try
            {
                infos.AddRange(base.Channel.GetGroups());
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        Utilify.Platform.Security.PermissionInfo[] IUserManager.GetPermissions()
        {
            List<Utilify.Platform.Security.PermissionInfo> infos = new List<Utilify.Platform.Security.PermissionInfo>();
            try
            {
                infos.AddRange(base.Channel.GetPermissions());
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        void IUserManager.UpdateUser(Utilify.Platform.Security.UserInfo userInfo)
        {
            try
            {
                base.Channel.UpdateUser(userInfo);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IUserManager.CreateUser(Utilify.Platform.Security.UserInfo userInfo)
        {
            try
            {
                base.Channel.CreateUser(userInfo);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        #endregion
    }
}
#endif