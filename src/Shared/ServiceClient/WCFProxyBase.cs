#if !MONO

using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace Utilify.Platform
{
    /// <summary>
    /// Wraps the ChannelFactory. This class is similar to the built-in ClientBase class, but with an ability to 'Reset' a faulted channel and create a new one.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal abstract class WCFProxyBase<T> : IDisposable , IManagerService, IManagerProxy
        where T : class, IManagerService
    {
        //not inheriting from ClientBase<T> since we don't have a way to reset its private 'Channel'

        //protected readonly Logger logger;

        private ChannelFactory<T> channelFactory = null;

        private T channel = default(T);

        protected WCFProxyBase() 
        {
            //logger = new Logger();
            /*
             * If we intend to create many channels, simply dropping down to the ChannelFactory method of creation 
             * and re-using the factory will be a significant performance win.
             */
            channelFactory = new ChannelFactory<T>("*"); //magic! discovered this from the client-base src.
            //is there a better / documented way?
            
        }

        protected WCFProxyBase(Binding binding, EndpointAddress remoteAddress) 
        {
            channelFactory = new ChannelFactory<T>(binding, remoteAddress); 
        }
        
        protected ChannelFactory<T> ChannelFactory
        {
            get { return channelFactory; }
        }

        protected T Channel
        {
            get 
            {
                VerifyDisposed();
                //TODO: perhaps we should subscribe to the Faulted event and clean up
                if (channel == null)
                    channel = channelFactory.CreateChannel();
                return channel;
            }
        }

        public ClientCredentials ClientCredentials
        {
            get
            {
                VerifyDisposed();
                return channelFactory.Credentials;
            }
        }

        public ServiceEndpoint EndPoint
        {
            get
            {
                VerifyDisposed();
                return channelFactory.Endpoint;
            }
        }

        public CommunicationState State
        {
            get
            {
                VerifyDisposed();
                return channelFactory.State;
            }
        }

        /// <summary>
        /// Aborts the underlying channel and creates a new one that can be used for communication
        /// </summary>
        protected void Reset()
        {
            //let's not verify disposed here: no point - since we are not actually accessing anything here.
            //VerifyDisposed();

            //causes CommunicationObjectAbortedException, so lets not abort!
            //channelFactory.Abort(); 
            
            channel = null;
        }

        //public void Close()
        //{
        //    (this as IDisposable).Dispose();
        //}

        void IManagerService.CheckService()
        {
            try
            {
                //to ensure we don't have a faulted channel:
                //let us reset: just for this method:
                this.Reset();
                this.Channel.CheckService();
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                this.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                this.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        #region IDisposable Members

        private bool disposed = false;

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        protected void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                //clean up managed stuff                
                try
                {
                    if (channelFactory.State != CommunicationState.Faulted && channelFactory.State != CommunicationState.Closed)
                    {
                        channelFactory.Close();
                    }
                    else
                    {
                        channelFactory.Abort();
                    }
                }
                catch (System.ServiceModel.CommunicationException)
                {
                    channelFactory.Abort();
                }
                catch (TimeoutException)
                {
                    channelFactory.Abort();
                }
                catch (Exception)
                {
                    channelFactory.Abort();
                    throw;
                }
                finally
                {
                    channel = null;
                    channelFactory = null;
                }
            }

            //we have no base class to call dispose on, so nothing more to do here.
            disposed = true;
        }

        private void VerifyDisposed()
        {
            if (disposed)
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }
        }

        #endregion

    }
}

#endif