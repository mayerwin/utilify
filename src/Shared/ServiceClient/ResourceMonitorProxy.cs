#if !MONO
using System;
using System.ServiceModel;
using Utilify.Platform.Contract;

namespace Utilify.Platform
{
    internal partial class ResourceMonitorProxy : WCFProxyBase<IResourceMonitor>, IResourceMonitor
    {
        #region Constructors
        public ResourceMonitorProxy() : base()
        { }

        public ResourceMonitorProxy(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion

        #region IResourceMonitor Members

        void IResourceMonitor.Ping(string id)
        {
            try
            {
                base.Channel.Ping(id);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        string IResourceMonitor.Register(SystemInfo info)
        {
            try
            {
                return base.Channel.Register(info);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                //create a new one
                base.Reset();                
                //let the caller know we blew up. but they can re-try the operation again, since we created the channel here.
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IResourceMonitor.UpdatePerformance(PerformanceInfo info)
        {
            try
            {
                base.Channel.UpdatePerformance(info);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IResourceMonitor.UpdateLimits(LimitInfo info)
        {
            try
            {
                base.Channel.UpdateLimits(info);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        #endregion
    }
}
#endif