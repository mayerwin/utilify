﻿using System;
using System.ServiceModel;

namespace Utilify.Platform
{
    [StreamedBehavior]
    internal class DataTransferServiceProxy : WCFProxyBase<IDataTransferService>, IDataTransferService
    {
        //private Logger logger = new Logger();

        #region Constructors

        public DataTransferServiceProxy(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion

        #region IDataTransferService Members

        DataObjectInfo IDataTransferService.Put(DataTransferInfo info)
        {
            try
            {
                return base.Channel.Put(info);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        DataTransferInfo IDataTransferService.Get(DataObjectInfo info)
        {
            try
            {
                return base.Channel.Get(info);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }


        string[] IDataTransferService.List(string containerName)
        {
            try
            {
                return base.Channel.List(containerName);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IDataTransferService.MkDir(string containerName)
        {
            try
            {
                base.Channel.MkDir(containerName);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IDataTransferService.RmDir(string containerName)
        {
            try
            {
                base.Channel.RmDir(containerName);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        void IDataTransferService.Delete(string objectName)
        {
            try
            {
                base.Channel.Delete(objectName);
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
        }

        #endregion
    }
}
