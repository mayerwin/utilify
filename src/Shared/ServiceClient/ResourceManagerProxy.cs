#if !MONO
using System;
using System.Collections.Generic;
using System.ServiceModel;

using Utilify.Platform.Contract;

namespace Utilify.Platform
{
    internal partial class ResourceManagerProxy : WCFProxyBase<IResourceManager>, IResourceManager
    {
        #region Constructors

        public ResourceManagerProxy() { }

        public ResourceManagerProxy(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion
    
        #region IResourceManager Members

        SystemInfo[] IResourceManager.GetExecutors()
        {
            List<SystemInfo> infos = new List<SystemInfo>();
            try
            {
                infos.AddRange(base.Channel.GetExecutors());
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray();
        }

        PerformanceInfo[] IResourceManager.GetSystemPerformance()
        {
            List<PerformanceInfo> infos = new List<PerformanceInfo>();
 	        try
            {
                infos.AddRange(base.Channel.GetSystemPerformance());
            }
            catch (FaultException fx)
            {
                throw FaultMapper.GetException(fx);
            }
            catch (System.ServiceModel.CommunicationException cx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(cx.Message, cx);
            }
            catch (TimeoutException tx)
            {
                base.Reset();
                throw new Utilify.Platform.CommunicationException(tx.Message, tx);
            }
            return infos.ToArray(); 
        }

        #endregion

    }
}
#endif