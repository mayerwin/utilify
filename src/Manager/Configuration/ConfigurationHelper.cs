﻿#if !MONO

using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using Utilify.Platform.Configuration;
using System.ServiceModel.Channels;

namespace Utilify.Platform.Manager
{
    internal static class ConfigurationHelper
    {
        private static Logger logger = new Logger();

        internal static void LogListenerConfig(ServiceHost listener)
        {
            StringBuilder config = new StringBuilder();
            config.AppendLine();
            config.AppendFormat("---------- Listener: {0} ---------- ", listener.Description.Name).AppendLine();

            //authorization
            config.AppendLine("- Authorization -"); 
            config.AppendFormat("ImpersonateCallerForAllOperations = {0}", listener.Authorization.ImpersonateCallerForAllOperations)
                .AppendLine();
            config.AppendFormat("PrincipalPermissionMode = {0}", listener.Authorization.PrincipalPermissionMode)
                .AppendLine();
            if (listener.Authorization != null &&
                listener.Authorization.ExternalAuthorizationPolicies != null && 
                listener.Authorization.ExternalAuthorizationPolicies.Count > 0)
                config.AppendFormat("ExternalAuthorizationPolicies[0] = {0}", listener.Authorization.ExternalAuthorizationPolicies[0].ToString())
                    .AppendLine();
            config.AppendLine();

            //addresses
            config.AppendLine("- BaseAddresses -");
            foreach (var addr in listener.BaseAddresses)
            {
                config.AppendFormat("Base address: {0}", listener.BaseAddresses[0].ToString()).AppendLine();
            }
            config.AppendLine();

            //Timeouts and state
            config.AppendLine("- Timeouts and state -");
            config.AppendFormat("OpenTimeout = {0}", listener.OpenTimeout).AppendLine();
            config.AppendFormat("CloseTimeout = {0}", listener.CloseTimeout).AppendLine();
            config.AppendFormat("State = {0}", listener.State).AppendLine();
            config.AppendFormat("ManualFlowControlLimit = {0}", listener.ManualFlowControlLimit).AppendLine();
            config.AppendLine();

            //UserNameAuthentication
            config.AppendLine("- UserNameAuthenication -");
            config.AppendFormat("UserNameAuthentication UserNamePasswordValidationMode = {0}", 
                listener.Credentials.UserNameAuthentication.UserNamePasswordValidationMode)
                .AppendLine();
            config.AppendFormat("UserNameAuthentication CustomUserNamePasswordValidator = {0}",
                listener.Credentials.UserNameAuthentication.CustomUserNamePasswordValidator)
                .AppendLine();
            config.AppendFormat("UserNameAuthentication IncludeWindowsGroups = {0}",
                listener.Credentials.UserNameAuthentication.IncludeWindowsGroups)
                .AppendLine();
            config.AppendLine();

            //WindowsAuth
            config.AppendLine("- WindowsAuthentication -"); 
            config.AppendFormat("WindowsAuthentication AllowAnonymousLogons = {0}",
                listener.Credentials.WindowsAuthentication.AllowAnonymousLogons)
                .AppendLine();
            config.AppendFormat("WindowsAuthentication IncludeWindowsGroups = {0}",
                listener.Credentials.WindowsAuthentication.IncludeWindowsGroups)
                .AppendLine();
            config.AppendLine();

            //ServiceCert
            if (listener.Credentials != null && 
                listener.Credentials.ServiceCertificate != null &&
                listener.Credentials.ServiceCertificate.Certificate != null)
            {
                config.AppendLine("- ServiceCertificate -");
                config.AppendFormat("ServiceCertificate Certificate SubjectName = {0}",
                    listener.Credentials.ServiceCertificate.Certificate.SubjectName)
                    .AppendLine();
                config.AppendLine();
            }

            //Description
            config.AppendLine("- Description -");
            config.AppendFormat("Description ConfigurationName = {0}",
                listener.Description.ConfigurationName)
                .AppendLine();
            config.AppendFormat("Description Namespace = {0}",
                listener.Description.Namespace)
                .AppendLine();
            config.AppendFormat("Description ServiceType = {0}",
                listener.Description.ServiceType)
                .AppendLine();

            foreach (var ep in listener.Description.Endpoints)
            {
                config.AppendLine(LogEndPointConfig(ep));
            }

            foreach (var beh in listener.Description.Behaviors)
            {
                config.AppendLine(); 
                config.AppendFormat("Description Behavior = {0}", beh.ToString())
                    .AppendLine();

                if (beh is ServiceBehaviorAttribute)
                {
                    var serBeh = beh as ServiceBehaviorAttribute;
                    config.AppendFormat("ServiceBehavior AddressFilterMode = {0}", serBeh.AddressFilterMode).AppendLine();
                    config.AppendFormat("ServiceBehavior AutomaticSessionShutdown = {0}", serBeh.AutomaticSessionShutdown).AppendLine();
                    config.AppendFormat("ServiceBehavior ConcurrencyMode = {0}", serBeh.ConcurrencyMode).AppendLine();
                    config.AppendFormat("ServiceBehavior ConfigurationName = {0}", serBeh.ConfigurationName).AppendLine();
                    config.AppendFormat("ServiceBehavior IgnoreExtensionDataObject = {0}", serBeh.IgnoreExtensionDataObject).AppendLine();
                    config.AppendFormat("ServiceBehavior IncludeExceptionDetailInFaults = {0}", serBeh.IncludeExceptionDetailInFaults).AppendLine();
                    config.AppendFormat("ServiceBehavior InstanceContextMode = {0}", serBeh.InstanceContextMode).AppendLine();
                    config.AppendFormat("ServiceBehavior MaxItemsInObjectGraph = {0}", serBeh.MaxItemsInObjectGraph).AppendLine();
                    config.AppendFormat("ServiceBehavior Name = {0}", serBeh.Name).AppendLine();
                    config.AppendFormat("ServiceBehavior Namespace = {0}", serBeh.Namespace).AppendLine();
                    config.AppendFormat("ServiceBehavior ReleaseServiceInstanceOnTransactionComplete = {0}", 
                        serBeh.ReleaseServiceInstanceOnTransactionComplete).AppendLine();
                    config.AppendFormat("ServiceBehavior TransactionAutoCompleteOnSessionClose = {0}",
                        serBeh.TransactionAutoCompleteOnSessionClose).AppendLine();
                    //config.AppendFormat("ServiceBehavior TransactionIsolationLevel = {0}", serBeh.TransactionIsolationLevel).AppendLine();
                    config.AppendFormat("ServiceBehavior TransactionTimeout = {0}", serBeh.TransactionTimeout).AppendLine();
                    config.AppendFormat("ServiceBehavior ValidateMustUnderstand = {0}", serBeh.ValidateMustUnderstand).AppendLine();
                }
                else if (beh is ServiceThrottlingBehavior)
                {
                    var throttle = beh as ServiceThrottlingBehavior;
                    config.AppendFormat("ServiceThrottling MaxConcurrentCalls = {0}", throttle.MaxConcurrentCalls).AppendLine();
                    config.AppendFormat("ServiceThrottling MaxConcurrentInstances = {0}", throttle.MaxConcurrentInstances).AppendLine();
                    config.AppendFormat("ServiceThrottling MaxConcurrentSessions = {0}", throttle.MaxConcurrentSessions).AppendLine();
                }

            }

            config.AppendLine();
            logger.Debug(config.ToString());
        }

        private static string LogEndPointConfig(System.ServiceModel.Description.ServiceEndpoint ep)
        {
            StringBuilder config = new StringBuilder();

            config.AppendLine();
            config.AppendFormat("EndPoint Name = {0}", ep.Name)
                .AppendLine();
            config.AppendFormat("EndPoint ListenUri = {0}", ep.ListenUri)
                .AppendLine();
            config.AppendFormat("EndPoint ListenUriMode = {0}", ep.ListenUriMode)
                .AppendLine();
            config.AppendFormat("- EndPoint Address = {0}", ep.Address)
                .AppendLine();

            Binding binding = ep.Binding;

            config.AppendFormat("EndPoint Binding = {0}", binding).AppendLine();

            config.AppendFormat("Binding OpenTimeout = {0}", binding.OpenTimeout).AppendLine();
            config.AppendFormat("Binding CloseTimeout = {0}", binding.CloseTimeout).AppendLine();
            config.AppendFormat("Binding SendTimeout = {0}", binding.SendTimeout).AppendLine();
            config.AppendFormat("Binding ReceiveTimeout = {0}", binding.ReceiveTimeout).AppendLine();

            if (binding is NetTcpBinding)
            {
                var nettcp = binding as NetTcpBinding;
                config.AppendFormat("NetTcpBinding Name = {0}", nettcp.Name).AppendLine();
                config.AppendFormat("NetTcpBinding Namespace = {0}", nettcp.Namespace).AppendLine();
                config.AppendFormat("NetTcpBinding HostNameComparisonMode = {0}", nettcp.HostNameComparisonMode).AppendLine();
                config.AppendFormat("NetTcpBinding ListenBacklog = {0}", nettcp.ListenBacklog).AppendLine();
                config.AppendFormat("NetTcpBinding MaxBufferPoolSize = {0}", nettcp.MaxBufferPoolSize).AppendLine();
                config.AppendFormat("NetTcpBinding MaxBufferSize = {0}", nettcp.MaxBufferSize).AppendLine();
                config.AppendFormat("NetTcpBinding MaxConnections = {0}", nettcp.MaxConnections).AppendLine();
                config.AppendFormat("NetTcpBinding MaxReceivedMessageSize = {0}", nettcp.MaxReceivedMessageSize).AppendLine();
                config.AppendFormat("NetTcpBinding PortSharingEnabled = {0}", nettcp.PortSharingEnabled).AppendLine();
                config.AppendFormat("NetTcpBinding Scheme = {0}", nettcp.Scheme).AppendLine();
                config.AppendFormat("NetTcpBinding TransactionFlow = {0}", nettcp.TransactionFlow).AppendLine();
                config.AppendFormat("NetTcpBinding TransactionProtocol = {0}", nettcp.TransactionProtocol).AppendLine();
                config.AppendFormat("NetTcpBinding TransferMode = {0}", nettcp.TransferMode).AppendLine();

                config.AppendFormat("NetTcpBinding Security Mode = {0}", nettcp.Security.Mode).AppendLine();
                config.AppendFormat("NetTcpBinding Security Message.ClientCredentialType = {0}", nettcp.Security.Message.ClientCredentialType).AppendLine();
                config.AppendFormat("NetTcpBinding Security Message.AlgorithmSuite = {0}", nettcp.Security.Message.AlgorithmSuite).AppendLine();
                config.AppendFormat("NetTcpBinding Security Transport.ClientCredentialType = {0}", nettcp.Security.Transport.ClientCredentialType).AppendLine();
                config.AppendFormat("NetTcpBinding Security Transport.ProtectionLevel= {0}", nettcp.Security.Transport.ProtectionLevel).AppendLine();
            }
            else if (binding is WSHttpBinding)
            {
                config.AppendLine("TODO WSHttpBinding print out");
            }

            config.AppendFormat("EndPoint Contract = {0}", ep.Contract.Name)
                .AppendLine();
            config.AppendFormat("EndPoint Contract HasProtectionLevel = {0}", ep.Contract.HasProtectionLevel)
                .AppendLine();
            config.AppendFormat("EndPoint Contract ProtectionLevel = {0}", ep.Contract.ProtectionLevel)
                .AppendLine();
            config.AppendFormat("EndPoint Contract SessionMode = {0}", ep.Contract.SessionMode)
                .AppendLine();

            return config.ToString();
        }

        internal static List<ManagerServiceHost> CreateListeners()
        {
            List<ManagerServiceHost> listeners = null;

            bool foundCustomServiceConfig = false;

            ManagerSectionGroup group = GetSectionGroup(false);
            if (group.IsDeclared)
            {
                (group as IConfigurationElement).Validate();
                if (group.Services != null && group.Services.ElementInformation.IsPresent)
                {
                    ServicesSection services = group.Services;
                    if (services.Items != null && services.Items.ElementInformation.IsPresent && services.Items.Count > 0)
                    {
                        listeners = new List<ManagerServiceHost>();
                        //we have some services declared
                        //we load the config only from our stuff: overriding whatever is specified in <system.serviceModel>
                        foreach (ServiceElement item in services.Items)
                        {
                            ManagerServiceHost host = CreateListener(item.Type);
                            listeners.Add(host);
                        }
                        foundCustomServiceConfig = true;
                    }
                }
            }

            if (!foundCustomServiceConfig)
            {
                listeners = new List<ManagerServiceHost>();
                //create listener services : read from config
                foreach (var type in Enum.GetValues(typeof(ServiceType)))
                {
                    if ((ServiceType)type == ServiceType.Default)
                        continue;

                    ManagerServiceHost host = CreateListener((ServiceType)type);
                    host.ApplyServiceModelConfig = true;

                    listeners.Add(host);
                }
            }

            return listeners;
        }

        internal static ManagerSectionGroup GetSectionGroup(bool validate)
        {
            //todo: need to do validation only once
            
            System.Configuration.Configuration config = 
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            ManagerSectionGroup group = config.GetSectionGroup(ManagerSectionGroup.ManagerSectionGroupName)
                as ManagerSectionGroup;

            ValidationHelper.CheckNull(group, ManagerSectionGroup.ManagerSectionGroupName);

            if (validate)
                (group as IConfigurationElement).Validate();

            return group;
        }        

        /// <summary>
        /// Create listener from the service type name specified in the custom utilify.platform.manager config section
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        private static ManagerServiceHost CreateListener(string serviceType)
        {
            //the service type names are string versions of the names defined in the ServiceType enum:
            if (Enum.IsDefined(typeof(ServiceType), serviceType))
            {
                ServiceType st = (ServiceType)Enum.Parse(typeof(ServiceType), serviceType);
                return CreateListener(st);
            }

            string message = string.Format(Errors.InvalidServiceType,
                string.Join(", ", Enum.GetNames(typeof(ServiceType))));

            ThrowHelper.ArgumentException(message, "serviceType");
            //just to keep the compiler happy:
            return null;
        }

        private static ManagerServiceHost CreateListener(ServiceType type)
        {
            Type serviceType = null;
            Type contractType = null;
            switch (type)
            {
                case ServiceType.ApplicationManager:
                    serviceType = typeof(ApplicationManager);
                    contractType = typeof(IApplicationManager);
                    break;
                case ServiceType.JobManager:
                    serviceType = typeof(JobManager);
                    contractType = typeof(IJobManager);
                    break;
                case ServiceType.WorkloadManager:
                    serviceType = typeof(WorkloadManager);
                    contractType = typeof(IWorkloadManager);
                    break;
                case ServiceType.ResourceManager:
                    serviceType = typeof(ResourceManager);
                    contractType = typeof(IResourceManager);
                    break;
                case ServiceType.ResourceMonitor:
                    serviceType = typeof(ResourceMonitor);
                    contractType = typeof(IResourceMonitor);
                    break;
                case ServiceType.DataTransferService:
                    serviceType = typeof(DataTransferService);
                    contractType = typeof(IDataTransferService);
                    break;
                case ServiceType.UserManager:
                    serviceType = typeof(UserManager);
                    contractType = typeof(IUserManager);
                    break;
                default:
                    ThrowHelper.ArgumentException(
                        string.Format(Errors.InvalidServiceType, type), 
                        "type");
                    break;
            }

            ManagerServiceHost host = new ManagerServiceHost(serviceType, contractType);

            return host;
        }
    }
}

#endif