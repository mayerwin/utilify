﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.Configuration;
using Utilify.Platform.Manager;

namespace Utilify.Platform.Configuration
{
    /// <summary>
    /// Represents the "utilify.platform.manager" configuration section group
    /// </summary>
    public sealed class ManagerSectionGroup : ConfigurationSectionGroup, IConfigurationElement
    {
        public const string ManagerSectionGroupName = "utilify.platform.manager";
        public const string ServicesSectionName = "services";
        public const string ServiceCertificateSectionName = "serviceCertificate";
        public const string StorageSectionName = "storage";

        public ManagerSectionGroup() { }

        public ServicesSection Services
        {
            get { return (ServicesSection)base.Sections[ServicesSectionName]; }
        }

        public ServiceCertificateSection ServiceCertificate
        {
            get { return (ServiceCertificateSection)base.Sections[ServiceCertificateSectionName]; }
        }

        public StorageSection Storage
        {
            get { return (StorageSection)base.Sections[StorageSectionName]; }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            (Services as IConfigurationElement).Validate();
            (ServiceCertificate as IConfigurationElement).Validate();
            (Storage as IConfigurationElement).Validate();
        }

        #endregion
    }

    /// <summary>
    /// Represents the services settings in the Manager configuration section. 
    /// </summary>
    public sealed class ServicesSection : ConfigurationSection, IConfigurationElement
    {
        //this seems to be the usual practice: and apparently gives a performance boost:
        private ConfigurationPropertyCollection properties;
        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                if (this.properties == null)
                {
                    ConfigurationPropertyCollection props = new ConfigurationPropertyCollection();
                    props.Add(new ConfigurationProperty(string.Empty,
                        typeof(ServiceElementCollection), null, null, null, ConfigurationPropertyOptions.IsDefaultCollection));
                    this.properties = props;
                }
                return this.properties;
            }
        }

        [ConfigurationProperty("", Options = ConfigurationPropertyOptions.IsDefaultCollection)]
        public ServiceElementCollection Items
        {
            get
            {
                return (ServiceElementCollection)base[""];
            }
        }

        public ServiceElement LookupService(ServiceType type)
        {
            return this.LookupService(type.ToString());
        }

        public ServiceElement LookupService(string type)
        {
            ValidationHelper.CheckNull(type, "type");
            return Items.LookupService(type);
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            (Items as IConfigurationElement).Validate();
        }

        #endregion
    }

    /// <summary>
    /// Represents the settings for services defined in the Manager configuration section.
    /// </summary>
    [ConfigurationCollection(typeof(ServiceElement),
        CollectionType = ConfigurationElementCollectionType.BasicMap,
        AddItemName = ServiceElementCollection.ServiceElementName)]
    public sealed class ServiceElementCollection : ConfigurationElementCollection, IConfigurationElement
    {
        public const string ServiceElementName = "service";

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //todo: check for duplicates?
        }

        #endregion

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override string ElementName
        {
            get
            {
                return ServiceElementCollection.ServiceElementName;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ServiceElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            ServiceElement el = (ServiceElement)element;
            return el.Type;
        }

        #region Collection Methods

        public void Add(ServiceElement service)
        {
            base.BaseAdd(service);
        }

        public void Remove(string name)
        {
            base.BaseRemove(name);
        }

        public void Remove(ServiceElement service)
        {
            base.BaseRemove(GetElementKey(service));
        }

        public void Clear()
        {
            base.BaseClear();
        }

        public void RemoveAt(int index)
        {
            base.BaseRemoveAt(index);
        }

        public string GetKey(int index)
        {
            return (string)base.BaseGetKey(index);
        }
        
        #endregion

        #region Indexers

        public ServiceElement this[int index]
        {
            get { return (ServiceElement)base.BaseGet(index); }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                base.BaseAdd(index, value);
            }
        }

        public new ServiceElement this[string name]
        {
            get { return (ServiceElement)base.BaseGet(name); }
        }

        #endregion

        internal ServiceElement LookupService(string type)
        {
            ValidationHelper.CheckNull(type, "type");
            ServiceElement service = null;
            for (int i = 0; i < this.Count; i++)
            {
                service = (ServiceElement)BaseGet(i);
                if (service.Type == type)
                    break;
                else
                    service = null;
            }
            return service;
        }
    }

    public sealed class ServiceElement : ConfigurationElement, IConfigurationElement
    {
        public const int DefaultPort = 8080;
        public const bool DefaultIsInteropEndPoint = false;

        public const string TypePropertyName = "type";
        public const string HostPropertyName = "host";
        public const string PortPropertyName = "port";
        public const string InteropPropertyName = "interop";
        public const string AuthenticationModesPropertyName = "authenticationModes";
        public const string ServiceThrottlePropertyName = "serviceThrottling";

        public ServiceElement() { }

        /// <summary>
        /// Gets or sets the service type
        /// </summary>
        [ConfigurationProperty(TypePropertyName, IsRequired = true)]
        public string Type
        {
            get { return (string)this[TypePropertyName]; }
            set { this[TypePropertyName] = value; }
        }

        /// <summary>
        /// Gets or sets the service host name
        /// </summary>
        [ConfigurationProperty(HostPropertyName)]
        public string Host
        {
            get { return (string)this[HostPropertyName]; }
            set { this[HostPropertyName] = value; }
        }

        /// <summary>
        /// Gets or sets the service port
        /// </summary>//if we don't set a default value, the integer validator blows up at runtime!
        [ConfigurationProperty(PortPropertyName, DefaultValue = DefaultPort, IsRequired = true)]
        [IntegerValidator(MinValue = 0, MaxValue = 65536, ExcludeRange = false)]
        public int Port
        {
            get { return (int)this[PortPropertyName]; }
            set
            {
                if (value != this.Port)
                    this[PortPropertyName] = value;
            }
        }

        [ConfigurationProperty(InteropPropertyName, IsRequired = false,
            DefaultValue = DefaultIsInteropEndPoint)]
        public bool IsInteropEndPoint
        {
            get { return (bool)this[InteropPropertyName]; }
            set
            {
                if (value != this.IsInteropEndPoint)
                    this[InteropPropertyName] = value;
            }
        }

        [ConfigurationProperty(AuthenticationModesPropertyName, IsRequired = true)]
        public AuthenticationModeCollection AuthenticationModes 
        {
            get
            {
                return (AuthenticationModeCollection)this[AuthenticationModesPropertyName];
            }
            set
            {
                this[AuthenticationModesPropertyName] = value;
            }
        }

        [ConfigurationProperty(ServiceThrottlePropertyName)]
        public ServiceThrottlingElement ServiceThrottle 
        {
            get
            {
                return (ServiceThrottlingElement)this[ServiceThrottlePropertyName];
            }
            set
            {
                this[ServiceThrottlePropertyName] = value;
            }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //if (!this.SupportsUsernameAuthentication && !this.SupportsWindowsAuthentication)
            //{
            //    throw new ConfigurationErrorsException("The service has to support atleast one of Username / Windows authentication.",
            //        this.ElementInformation.Source, this.ElementInformation.LineNumber);
            //}
        }

        #endregion
    }

    [ConfigurationCollection(typeof(AuthenticationModeElement), AddItemName = AuthenticationModeCollection.AuthModeElementName)]
    public sealed class AuthenticationModeCollection : ConfigurationElementCollection, IConfigurationElement
    {
        public const string AuthModeElementName = "authenticationMode";

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //todo: check for duplicates
        }

        #endregion

        protected override ConfigurationElement CreateNewElement()
        {
            return new AuthenticationModeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            AuthenticationModeElement el = (AuthenticationModeElement)element;
            return el.Type;
        }
    }

    public sealed class AuthenticationModeElement : ConfigurationElement, IConfigurationElement
    {
        public const string CredentialTypePropertyName = "type";

        [ConfigurationProperty(CredentialTypePropertyName, DefaultValue = CredentialType.None)]
        public CredentialType Type 
        {
            get
            {
                return (CredentialType)this[CredentialTypePropertyName];
            }
            set
            {
                this[CredentialTypePropertyName] = value;
            }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //nothing to do for now.
        }

        #endregion
    }

    public sealed class ServiceCertificateSection : ConfigurationSectionBase, IConfigurationElement
    {
        public const string UseCertificateStorePropertyName = "useCertificateStore";
        public const string CertificateFilePropertyName = "certificateFilePath";
        public const string CertificateAccessPasswordPropertyName = "certificateAccessPassword";

        public const string StoreLocationPropertyName = "storeLocation";
        public const string StoreNamePropertyName = "storeName";
        public const string FindTypePropertyName = "x509FindType";
        public const string FindValuePropertyName = "findValue";

        //to allow the caller to remove optional properties so that they need not be persisted to the xml config file.
        internal void SetCertMode()
        {
            if (!UseCertificateStore)
            {
                this.Properties.Remove(StoreLocationPropertyName);
                this.Properties.Remove(StoreNamePropertyName);
                this.Properties.Remove(FindTypePropertyName);
                this.Properties.Remove(FindValuePropertyName);
            }
            else
            {
                this.Properties.Remove(CertificateFilePropertyName);
                this.Properties.Remove(CertificateAccessPasswordPropertyName);
            }
        }

        [ConfigurationProperty(UseCertificateStorePropertyName, IsRequired = true)]
        public bool UseCertificateStore
        {
            get { return (bool)this[UseCertificateStorePropertyName]; }
            set { this[UseCertificateStorePropertyName] = value; }
        }

        [ConfigurationProperty(CertificateFilePropertyName, IsRequired = false)]
        public string CertificateFilePath
        {
            get
            {
                if (this.Properties.Contains(CertificateFilePropertyName))
                    return (string)this[CertificateFilePropertyName];
                else
                    return string.Empty;
            }
            set { this[CertificateFilePropertyName] = value; }
        }

        [ConfigurationProperty(CertificateAccessPasswordPropertyName, IsRequired = false)]
        public string CertificateAccessPassword
        {
            get
            {
                if (this.Properties.Contains(CertificateAccessPasswordPropertyName))
                    return EncryptionHelper.Decrypt((string)this[CertificateAccessPasswordPropertyName]);
                else
                    return string.Empty;
            }
            set
            {
                this[CertificateAccessPasswordPropertyName] = EncryptionHelper.Encrypt(value);
            }
        }

        [ConfigurationProperty(StoreLocationPropertyName, IsRequired = false)]
        public StoreLocation StoreLocation
        {
            get
            {
                if (this.Properties.Contains(StoreLocationPropertyName))
                    return (StoreLocation)this[StoreLocationPropertyName];
                else
                    return StoreLocation.LocalMachine;
            }
            set { this[StoreLocationPropertyName] = value; }
        }

        [ConfigurationProperty(StoreNamePropertyName, IsRequired = false)]
        public StoreName StoreName
        {
            get
            {
                if (this.Properties.Contains(StoreNamePropertyName))
                    return (StoreName)this[StoreNamePropertyName];
                else
                    return StoreName.My;
            }
            set { this[StoreNamePropertyName] = value; }
        }

        [ConfigurationProperty(FindTypePropertyName, IsRequired = false)]
        public X509FindType FindType
        {
            get
            {
                if (this.Properties.Contains(FindTypePropertyName))
                    return (X509FindType)this[FindTypePropertyName];
                else
                    return X509FindType.FindBySubjectName; //default: need this since the property may not be in the collection. See: SetCertMode
            }
            set
            {
                this[FindTypePropertyName] = value;
            }
        }

        [ConfigurationProperty(FindValuePropertyName, IsRequired = false)]
        public string FindValue
        {
            get
            {
                if (this.Properties.Contains(FindValuePropertyName))
                    return (string)this[FindValuePropertyName];
                return string.Empty;
            }
            set { this[FindValuePropertyName] = value; }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            if (UseCertificateStore && string.IsNullOrEmpty((string)FindValue))
            {
                throw new ConfigurationErrorsException("FindValue cannot be null or empty",
                    this.ElementInformation.Source,
                    this.ElementInformation.LineNumber);
            }
            else if (!UseCertificateStore && !System.IO.File.Exists(CertificateFilePath))
            {
                throw new ConfigurationErrorsException(
                    string.Format("Could not find certificate file : '{0}'", CertificateFilePath),
                    this.ElementInformation.Source,
                    this.ElementInformation.LineNumber);
            }
        }

        #endregion
    }

    public sealed class StorageSection : ConfigurationSectionBase, IConfigurationElement
    {
        public const string BasePathPropertyName = "basePath";

        [ConfigurationProperty(BasePathPropertyName, IsRequired = false)]
        public string BasePath
        {
            get { return (string)this[BasePathPropertyName]; }
            set { this[BasePathPropertyName] = value; }
        }

        #region IConfigurationElement Members

        void IConfigurationElement.Validate()
        {
            //lets not validate it here: let the services which use this worry about this.
        }

        #endregion
    }
}
