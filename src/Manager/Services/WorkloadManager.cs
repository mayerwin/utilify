using System;
using System.Collections.Generic;
using Utilify.Platform.Contract;
using Utilify.Platform.Manager.Persistence;
using Utilify.Platform.Manager.Security;

namespace Utilify.Platform.Manager
{
    //todo: (WorkloadManager) expand class summary docs

    /// <summary>
    /// Manages applications and jobs that are submitted by client programs for distributed execution. The WorkloadManager
    /// is the remote interface to the job database that includes various functions such as :
    /// - performing application / job actions requested by the console such as: start / stop / cancel / get status etc
    /// - serving requests to get information about past jobs and applications and 'connect' to them
    /// </summary>
#if !MONO
    [ErrorBehavior(typeof(ServiceErrorHandler))] //attach the error handling behaviour which does Exception to Fault mapping
    [System.ServiceModel.ServiceBehavior(
        Namespace = Namespaces.Manager,
        InstanceContextMode = System.ServiceModel.InstanceContextMode.Single,
        ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Multiple,
        AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
#endif
    internal class WorkloadManager : MarshalByRefObject, IWorkloadManager
    {
        private Logger logger = new Logger();

        #region IWorkloadManager members

        //todoFindOut: need to do a code-review: all the security stuff may break the existing unit tests.

        public void CheckService() { }

        /// <summary>
        /// Gets the entire list of submission information for available applications
        /// </summary>
        /// <returns></returns>
        public Utilify.Platform.ApplicationSubmissionInfo[] GetApplications()
        {
            bool canManageAll = 
                SecurityHelper.CheckPermission(PermissionRequirement.All, 
                    Utilify.Platform.Manager.Security.Permission.ManageAllApplications);

            List<ApplicationSubmissionInfo> infos = null;

            try
            {
                logger.Debug("Call to get list of applications.");                
                if (canManageAll)
                {
                    infos = ApplicationDao.GetApplications();
                }
                else
                {
                    bool canManageOwn = SecurityHelper.CheckPermission(PermissionRequirement.All, 
                        Utilify.Platform.Manager.Security.Permission.ManageApplications);

                    if (canManageOwn)
                        infos = ApplicationDao.GetApplications(SecurityHelper.GetCurrentUserName());
                }                

                logger.Debug("# applications = " + infos.Count);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            if (infos == null)
                infos = new List<ApplicationSubmissionInfo>();

            return infos.ToArray();
        }

        /// <summary>
        /// Get list of all jobs for a given application.
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public JobInfo[] GetJobs(string applicationId)
        {
            // check owner of application
            // check application exists
            // return all jobs for this application
            if (applicationId == null)
                throw new ArgumentNullException("applicationId");

            // Make sure this user can access the Jobs for this App
            SecurityHelper.EnsurePermission(applicationId, EntityType.Application);

            List<JobInfo> infos = new List<JobInfo>();

            logger.Debug("*** Call to get list of Jobs.");
            try
            {
                Guid applicationGuid = Helper.GetGuidSafe(applicationId);
                if (applicationGuid == Guid.Empty)
                    throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                        "applicationId");

                if (!ApplicationDao.ApplicationExists(applicationGuid))
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);

                logger.Debug("*** Getting jobs for application : " + applicationId);

                infos = ApplicationDao.GetJobInfo(applicationGuid);

                if (infos == null)
                    infos = new List<JobInfo>();

                logger.Debug("*** Got " + infos.Count + " Jobs"); 
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return infos.ToArray();
        }

        /// <summary>
        /// Aborts the specified applications. 
        /// Adding (submitting) more jobs to an aborted application will cause an exception. 
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationIds is a null reference</exception>
        /// <exception cref="System.ArgumentException">one of applicationIds is an empty string or in an invalid format</exception>
        /// <exception cref="System.InvalidOperationException">
        /// The application is already <see cref="F:Utilify.Platform.ApplicationStatus.Stopped">stopped</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        public void AbortApplications(string[] applicationIds)
        {
            if (applicationIds == null)
                throw new ArgumentNullException("applicationIds");

            if (applicationIds.Length == 0)
                throw new ArgumentException(Messages.ListEmpty, "applicationIds");

            //need to improve this impl

            // AppId: Check user has access to this app
            foreach (string applicationId in applicationIds)
            {
                if (applicationId == null || applicationId.Trim().Length == 0)
                    throw new ArgumentException(Messages.ElementNullOrEmpty, "applicationIds");

                // Make sure User has permission to Admin this App
                SecurityHelper.EnsurePermission(applicationId, EntityType.Application);
                
                logger.Debug("Call to Abort Application : " + applicationId);

                Guid id = Helper.GetGuidSafe(applicationId);
                if (id == Guid.Empty)
                    throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                        "applicationId");
                try
                {
                    //todoDecide: can we merge this with the ApplicationManager's method somehow? It is just duplicated code!
                    //need to pull out the app object and not just an exists check here, 
                    //since we use the app object later to check status etc.
                    //This method should be safe to use, as long as we don't touch lazy-loaded collections in the application
                    //since the owning session is closed
                    Utilify.Platform.Manager.Persistence.Application application = ApplicationDao.GetApplication(id);
                    if (application == null)
                        throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);
                    
                    //check if it is valid to abort this app:
                    if (application.Status == ApplicationStatus.Stopped)
                        throw new InvalidOperationException("Cannot abort a stopped application");

                    //Issue 013: fire off a thread to send a message to all the executors that are running any jobs that belong to this app
                    //to abort and stop the jobs
                    ApplicationDao.UpdateApplicationStatus(application.Id, ApplicationStatus.Stopped);

                    ApplicationDao.AbortJobs(application.Id);
                }
                catch (PersistenceException px)
                {
                    //the ManagerErrorHandler will hide the inner exception
                    throw new InternalServerException(Messages.InternalServerError, px);
                }
            }
        }

        /// <summary>
        /// Aborts the specified jobs.  
        /// </summary>
        /// <exception cref="System.ArgumentNullException">jobIds is a null reference</exception>
        /// <exception cref="System.ArgumentException">one of the jobIds is an empty string or in an invalid format</exception>
        /// <exception cref="System.InvalidOperationException">
        /// The application is already <see cref="F:Utilify.Platform.JobStatus.Completed">Completed</see> or 
        /// <see cref="F:Utilify.Platform.JobStatus.Cancelled">Cancelled</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.JobNotFoundException">A job with the specified id is not found</exception>
        public void AbortJobs(string[] jobIds)
        {
            if (jobIds == null)
                throw new ArgumentNullException("jobIds");

            if (jobIds.Length == 0)
                throw new ArgumentException(Messages.ListEmpty, "jobIds");

            foreach (string jobId in jobIds)
            {
                if (jobId == null || jobId.Trim().Length == 0)
                    throw new ArgumentException(Messages.ElementNullOrEmpty, "jobIds");

                // JobId: Check user has access to this job
                SecurityHelper.EnsurePermission(jobId, EntityType.Job);
                
                logger.Debug("Call to Abort Job : " + jobId);

                Guid id = Helper.GetGuidSafe(jobId);
                if (id == Guid.Empty) //still need to check this here, since it may have an invalid format
                    throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "job id", jobId),
                        "jobId");

                try
                {
                    Utilify.Platform.Manager.Persistence.Job job = ApplicationDao.GetJob(id);
                    if (job == null)
                        throw new JobNotFoundException(Messages.JobNotFound, jobId);

                    //check if it is valid to abort this job:
                    if (job.Status != JobStatus.Completed && job.Status != JobStatus.Cancelled)
                    {
                        // todoDiscuss: This doesn't need to throw an exception if the job was already completed or cancelled before.
                        // We don't want this to throw an exception when somone tries to abort a job that had just finished a second ago, right?

                        //Issue 014: fire off a thread to send a message to the executor that is running the job to abort and stop the job
                        ApplicationDao.AbortJob(job.Id);
                    }
                }
                catch (InvalidTransitionException ix)
                {
                    logger.Info("Error trying to abort job : " + jobId + "\n" + ix.Message);
                    throw new InvalidOperationException("Cannot abort job " + jobId + " because it is in an invalid state.");
                }
                catch (PersistenceException px)
                {
                    //the ManagerErrorHandler will hide the inner exception
                    throw new InternalServerException(Messages.InternalServerError, px);
                }
            }
        }

        /// <summary>
        /// Resets the specified jobs.  
        /// </summary>
        /// <exception cref="System.ArgumentNullException">jobIds is a null reference</exception>
        /// <exception cref="System.ArgumentException">one of the jobIds is an empty string or in an invalid format</exception>
        /// <exception cref="System.InvalidOperationException">
        /// The application is already <see cref="F:Utilify.Platform.JobStatus.Completed">Completed</see> or 
        /// <see cref="F:Utilify.Platform.JobStatus.Cancelled">Cancelled</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.JobNotFoundException">A job with the specified id is not found</exception>
        public void ResetJobs(string[] jobIds)
        {
            if (jobIds == null)
                throw new ArgumentNullException("jobIds");

            if (jobIds.Length == 0)
                throw new ArgumentException(Messages.ListEmpty, "jobIds");

            foreach (string jobId in jobIds)
            {
                if (jobId == null || jobId.Trim().Length == 0)
                    throw new ArgumentException(Messages.ElementNullOrEmpty, "jobIds");

                // JobId: Check user has access to this job
                SecurityHelper.EnsurePermission(jobId, EntityType.Job);

                logger.Debug("Call to Reset Job : " + jobId);

                Guid id = Helper.GetGuidSafe(jobId);
                if (id == Guid.Empty)
                    throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "job id", jobId),
                        "jobId");

                try
                {
                    Utilify.Platform.Manager.Persistence.Job job = ApplicationDao.GetJob(id);
                    if (job == null)
                        throw new JobNotFoundException(Messages.JobNotFound, jobId);

                    //check if it is valid to reset this job:
                    if (job.Status == JobStatus.Completing || job.Status == JobStatus.Executing || job.Status == JobStatus.Scheduled)
                    {
                        // todoDiscuss: Throw exception with wrong status change attempt?

                        //Issue 014: fire off a thread to send a message to the executor that is running the job to abort and stop the job
                        ApplicationDao.UpdateJobStatus(job.Id, JobStatus.Ready);
                    }
                }
                catch (InvalidTransitionException ix)
                {
                    logger.Info("Error trying to reset job : " + jobId + "\n" + ix.Message);
                }
                catch (PersistenceException px)
                {
                    //the ManagerErrorHandler will hide the inner exception
                    throw new InternalServerException(Messages.InternalServerError, px);
                }
            }
        }

        /// <summary>
        /// Pauses the specified applications. Adding (submitting) more jobs to a paused application will result in the jobs being suspended for later execution, 
        /// when the application is resumed.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationIds is a null reference</exception>
        /// <exception cref="System.ArgumentException">one of the applicationIds is an empty string or in an invalid format</exception>
        /// <exception cref="System.InvalidOperationException">
        /// The application is already <see cref="F:Utilify.Platform.ApplicationStatus.Paused">paused</see>
        /// or <see cref="F:Utilify.Platform.ApplicationStatus.Stopped">stopped</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        public void PauseApplications(string[] applicationIds)
        {
            if (applicationIds == null)
                throw new ArgumentNullException("applicationIds");

            if (applicationIds.Length == 0)
                throw new ArgumentException(Messages.ListEmpty, "applicationIds");

            foreach (string applicationId in applicationIds)
            {
                if (applicationId == null || applicationId.Trim().Length == 0)
                    throw new ArgumentException(Messages.ElementNullOrEmpty, "applicationIds");

                // AppId: Check user has access to this app
                SecurityHelper.EnsurePermission(applicationId, EntityType.Application);

                Guid id = Helper.GetGuidSafe(applicationId);
                if (id == Guid.Empty)
                    throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                        "applicationId");

                try
                {
                    //todoDecide: can we merge this with the ApplicationManager's method somehow? It is just duplicated code!
                    //need to pull out the app object and not just an exists check here, 
                    //since we use the app object later to check status etc.
                    //This method should be safe to use, as long as we don't touch lazy-loaded collections in the application
                    //since the owning session is closed
                    Utilify.Platform.Manager.Persistence.Application application = ApplicationDao.GetApplication(id);
                    if (application == null)
                        throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);
                    
                    //check if it is valid to pause this app:
                    if (application.Status == ApplicationStatus.Stopped)
                        throw new InvalidOperationException("Cannot pause a stopped application");

                    if (application.Status == ApplicationStatus.Paused)
                        throw new InvalidOperationException("Cannot pause a paused application");

                    ApplicationDao.UpdateApplicationStatus(application.Id, ApplicationStatus.Paused);

                }
                catch (PersistenceException px)
                {
                    //the ManagerErrorHandler will hide the inner exception
                    throw new InternalServerException(Messages.InternalServerError, px);
                }
            }
 
        }

        /// <summary>
        /// Gets the status of the applications with the specified ids
        /// </summary>
        /// <param name="applicationIds">ids of the applications whose status is required</param>
        /// <returns>an array of ApplicationStatusInfo representing the application status</returns>
        /// <exception cref="System.ArgumentNullException">applicationIds is a null reference</exception>
        /// <exception cref="System.ArgumentException">one of the applicationId is an empty string or in an invalid format</exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        public ApplicationStatusInfo[] GetApplicationStatus(string[] applicationIds)
        {
            if (applicationIds == null)
                throw new ArgumentNullException("applicationIds");

            if (applicationIds.Length == 0)
                throw new ArgumentException(Messages.ListEmpty, "applicationIds");
            
            List<ApplicationStatusInfo> infos = new List<ApplicationStatusInfo>();

            foreach (string applicationId in applicationIds)
            {
                if (applicationId == null || applicationId.Trim().Length == 0)
                    throw new ArgumentException(Messages.ElementNullOrEmpty, "applicationIds");

                // AppId: Check user has access to this app
                SecurityHelper.EnsurePermission(applicationId, EntityType.Application);

                Guid id = Helper.GetGuidSafe(applicationId);
                if (id == Guid.Empty)
                    throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                        "applicationId");

                try
                {
                    if (!ApplicationDao.ApplicationExists(id))
                        throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);

                    infos.Add(ApplicationDao.GetApplicationStatus(id));
                }
                catch (PersistenceException px)
                {
                    //the ManagerErrorHandler will hide the inner exception
                    throw new InternalServerException(Messages.InternalServerError, px);
                }
            }

            return infos.ToArray();
        }

        /// <summary>
        /// Resumes the specified applications. This will allow waiting jobs in the applications to be eligible for scheduling.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationIds is a null reference</exception>
        /// <exception cref="System.ArgumentException">one of the applicationIds is an empty string or in an invalid format</exception>
        /// <exception cref="System.InvalidOperationException">
        /// An application is not <see cref="F:Utilify.Platform.ApplicationStatus.Paused">paused</see>
        /// </exception>
        /// <exception cref="Utilify.Platform.ApplicationNotFoundException">An application with the specified applicationId is not found</exception>
        public void ResumeApplications(string[] applicationIds)
        {
            if (applicationIds == null)
                throw new ArgumentNullException("applicationIds");
            
            if (applicationIds.Length == 0)
                throw new ArgumentException(Messages.ListEmpty, "applicationIds");

            foreach (string applicationId in applicationIds)
            {
                if (applicationId == null || applicationId.Trim().Length == 0)
                    throw new ArgumentException(Messages.ElementNullOrEmpty, "applicationIds");

                // AppId: Check user has access to this app
                SecurityHelper.EnsurePermission(applicationId, EntityType.Application);

                Guid id = Helper.GetGuidSafe(applicationId);
                if (id == Guid.Empty)
                    throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                        "applicationId");

                try
                {
                    //todoDecide: can we merge this with the ApplicationManager's method somehow? It is just duplicated code!
                    //need to pull out the app object and not just an exists check here, 
                    //since we use the app object later to check status etc.
                    //This method should be safe to use, as long as we don't touch lazy-loaded collections in the application
                    //since the owning session is closed
                    Utilify.Platform.Manager.Persistence.Application application = 
                        ApplicationDao.GetApplication(id);
                    
                    if (application == null)
                        throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);
                    
                    //check if it is valid to resume this app:
                    if (application.Status != ApplicationStatus.Paused)
                        throw new InvalidOperationException("Cannot resume the application because it is not in the 'paused' state.");

                    //this goes back to Submitted and will eventually be set back to Ready if the scheduler decides it is ready.
                    ApplicationDao.UpdateApplicationStatus(application.Id, ApplicationStatus.Submitted);
                    
                }
                catch (PersistenceException px)
                {
                    //the ManagerErrorHandler will hide the inner exception
                    throw new InternalServerException(Messages.InternalServerError, px);
                }
            }
        }

        /// <summary>
        /// Gets the status of the list of jobs with the specified ids.
        /// </summary>
        /// <param name="jobIds">an array of the ids of the jobs whose status is required</param>
        /// <returns>Array of JobStatusInfo objects that contain the status of the jobs</returns>
        /// <exception cref="System.ArgumentNullException">jobIds in a null reference</exception>
        /// <exception cref="System.ArgumentException">jobIds is an empty list, or the elements in the list are null, empty or in an invalid format</exception>
        /// <exception cref="Utilify.Platform.JobNotFoundException">An element of the jobIds list, refers to a non-existent job</exception>
        public JobStatusInfo[] GetJobStatus(string[] jobIds)
        {

            if (jobIds == null)
                throw new ArgumentNullException("jobIds");
            if (jobIds.Length == 0)
                throw new ArgumentException("The list of job ids to query cannot be empty", "jobIds");

            // AppId: Check user has access to this app
            SecurityHelper.EnsurePermission(jobIds, EntityType.Job);

            List<Utilify.Platform.Manager.Persistence.Job> jobs = new List<Utilify.Platform.Manager.Persistence.Job>();
            JobStatusInfo[] statuses = null;

            try
            {
                List<Guid> jobGuids = new List<Guid>();
                foreach (string id in jobIds)
                {
                    Guid jobGuid = Helper.GetGuidSafe(id);
                    if (jobGuid == Guid.Empty)
                        throw new ArgumentException("Elements of the job id list cannot be null, empty or invalid");

                    if (!ApplicationDao.JobExists(jobGuid))
                        throw new JobNotFoundException(Messages.JobNotFound, id);

                    jobGuids.Add(jobGuid);
                }

                //don't eager load instances: since we don't need the job instances here.
                jobs = ApplicationDao.GetJobs(jobGuids, FetchStrategy.Lazy);

                //should be safe, since this doesn't touch any lazy loaded instances
                statuses = 
                    DataStore.Transform<Utilify.Platform.Manager.Persistence.Job, JobStatusInfo>(jobs).ToArray();
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }
            return statuses;
        }

        /// <summary>
        /// Get list of all job status for a given application.
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public Utilify.Platform.JobStatusInfo[] GetJobStatusForApplication(string applicationId)
        {
            // check owner of application
            // check application exists
            // return all jobs for this application
            if (applicationId == null)
                throw new ArgumentNullException("applicationId");

            SecurityHelper.EnsurePermission(applicationId, EntityType.Application);

            IList<Utilify.Platform.Manager.Persistence.Job> jobs = 
                new List<Utilify.Platform.Manager.Persistence.Job>();

            JobStatusInfo[] statuses = null;

            try
            {
                Guid applicationGuid = Helper.GetGuidSafe(applicationId);
                if (applicationGuid == Guid.Empty)
                    throw new ArgumentException(string.Format(Messages.NullEmptyOrInvalidFormat, "application id", applicationId),
                        "applicationId");

                if (!ApplicationDao.ApplicationExists(applicationGuid))
                    throw new ApplicationNotFoundException(Messages.ApplicationNotFound, applicationId);

                jobs = ApplicationDao.GetJobsForApplication(applicationGuid);
                //should be safe: since we don't touch any lazy loaded instances here
                statuses =
                    DataStore.Transform<Utilify.Platform.Manager.Persistence.Job, JobStatusInfo>(jobs).ToArray();
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return statuses;
        }

        #endregion
    }
}
