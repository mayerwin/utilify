﻿using System;
using System.IO;

using System.Configuration;
using Utilify.Platform.Manager.Security;
using System.Collections.Generic;

#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform.Manager
{
#if !MONO
    [ErrorBehavior(typeof(ServiceErrorHandler))] //attach the error handling behaviour which does Exception to Fault mapping
    [System.ServiceModel.ServiceBehavior(
        Namespace = Namespaces.Manager,
        InstanceContextMode = System.ServiceModel.InstanceContextMode.Single,
        ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Multiple,
        AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
#endif
    internal class DataTransferService : MarshalByRefObject, IDataTransferService
    {
        internal const string UrlScheme = "cloud://";

        internal string RootDirectory { get; set; }

        private Logger logger = new Logger();

        public DataTransferService() 
        {
            //get the root dir from config:
            string cloudStorageRoot = ConfigurationHelper.GetSectionGroup(false).Storage.BasePath;            
            if (!string.IsNullOrEmpty(cloudStorageRoot))
            {
                string root = IOHelper.GetDataDirectory(false);
                if (!Path.IsPathRooted(cloudStorageRoot))
                    cloudStorageRoot = Path.Combine(root, cloudStorageRoot);
                this.RootDirectory = cloudStorageRoot;
            }

            // create output folder, if does not exist
            if (!Directory.Exists(RootDirectory)) 
                Directory.CreateDirectory(RootDirectory);
        }

        private string ResolvePath(string path)
        {
            List<Permission> perms = SecurityHelper.GetAssignedPermissions();

            string username = string.Empty;
            //for requests from an executor, we let it access files from anywhere - since needs to read data
            if (!perms.Contains(Permission.ExecuteJob))
            {
                username = SecurityHelper.GetCurrentUserName();
            } 
            //else the path would have to contain the full path from root.
            //This should be the case when an executor requests to read/write a file
            string resolved = null;
            if (path != null)
            {                
                path = path.Trim().Replace(UrlScheme, string.Empty).Replace('/', Path.DirectorySeparatorChar);

                if (!Path.IsPathRooted(path))
                {
                    resolved = Path.Combine(RootDirectory, Path.Combine(username, path));
                }
                else
                {
                    resolved = path;
                }
            }
            return resolved;
        }

        private string GetObjectName(DataObjectInfo info)
        {
            long dataId = 0;

            bool isValid = Int64.TryParse(info.ObjectId, out dataId);

            if (dataId <= 0)
                return info.ObjectName;

            string objectName = info.ObjectName;

            try
            {
                Persistence.DataStore ds =
                    Persistence.DataStore.GetInstance();

                Persistence.BinaryData data = ds.Get<Persistence.BinaryData>(dataId);

                if (data != null && data.Path != null && data.Path.Trim().Length > 0)
                    objectName = data.Path;
            }
            catch (PersistenceException px)
            {
                logger.Warn("Error trying to get binary data instance.", px);
            }

            return objectName;
        }

        #region IDataTransferService Members

#if !MONO
        [OperationBehavior(AutoDisposeParameters = true)]
#endif
        public DataObjectInfo Put(DataTransferInfo info)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            if (info.ObjectName == null || info.ObjectName.Trim().Length == 0)
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "info");

            SecurityHelper.EnsurePermission(PermissionRequirement.Any, 
                Permission.ManageApplications, 
                Permission.ExecuteJob);

            logger.Debug("Started uploading " + info.ObjectName);
            logger.Debug("Size " + info.Length);

            // delete the target file, if already exists
            string filePath = ResolvePath(info.ObjectName);

            if (info.CreatePath)
            {
                string dir = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }

            if (info.Overwrite && File.Exists(filePath)) 
                File.Delete(filePath);

            int bufferSize = 32 * Constants.Kilo; //32KB buffer
            IOHelper.WriteFile(filePath, info.Data, bufferSize);

            ////save the BinaryData id record after writing the data:
            //Utilify.Platform.Manager.Persistence.BinaryData data = 
            //    new Utilify.Platform.Manager.Persistence.BinaryData(filePath);

            //Utilify.Platform.Manager.Persistence.DataStore.GetInstance().Save(data);

            //don't expose the local path: only send back the object name
            DataObjectInfo result = new DataObjectInfo() {
                ObjectId = info.ObjectName,
                ObjectName = info.ObjectName
            };

            return result;
        }

#if !MONO
        [OperationBehavior(AutoDisposeParameters = true)]
#endif
        public DataTransferInfo Get(DataObjectInfo info)  //need to have a MessageContract type for both params and return values - or else it wont work.
        {
            if (info == null)
                throw new ArgumentNullException("info");

            if ((info.ObjectId == null || info.ObjectId.Trim().Length == 0) &&
                (info.ObjectName == null || info.ObjectName.Trim().Length == 0))
            {
                throw new ArgumentException("Object id and name cannot both be null / empty", "info");
            }

            SecurityHelper.EnsurePermission(PermissionRequirement.Any, 
                Permission.ManageApplications, 
                Permission.ExecuteJob);

            string objectName = GetObjectName(info);

            // get some info about the input file
            string filePath = ResolvePath(objectName);
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);

            // let executors get the files for a job, regardless of which user directory, they are under.
            // check if exists
            if (!fileInfo.Exists) 
                throw new FileNotFoundException("Object not found", objectName);

            logger.Debug("Sending stream " + objectName + " to client. Size = " + fileInfo.Length);

            // open stream
            FileStream stream = new FileStream(filePath, 
                FileMode.Open, 
                FileAccess.Read, 
                FileShare.Read);

            // return result
            DataTransferInfo result = new DataTransferInfo();
            result.ObjectName = objectName;
            result.Length = fileInfo.Length;
            result.Data = stream;
            return result;

            // after returning to the client download starts. 
            //Stream remains open and on server and the client reads it, 
            //although the execution of this method is completed.
        }

        public string[] List(string containerName)
        {
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Permission.ManageApplications);

            if (containerName == null || containerName.Trim().Length == 0)
            {
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "containerName");
            }

            //a user can only list his/her own directory. The ResolvePath returns the user specific sub-directory
            string path = ResolvePath(containerName);
            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException("Could not find path: " + containerName);
            }

            string[] files = Directory.GetFiles(path);

            List<string> fileList = new List<string>();
            foreach (var file in files)
            {
                fileList.Add(Path.GetFileName(file));
            }

            return fileList.ToArray();
        }

        public void MkDir(string containerName)
        {
            SecurityHelper.EnsurePermission(PermissionRequirement.Any, Permission.ManageApplications, Permission.ExecuteJob);

            if (containerName == null || containerName.Trim().Length == 0)
            {
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "containerName");
            }

            string path = ResolvePath(containerName);
            if (Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(string.Format("The path {0} already exists.", containerName));
            }

            Directory.CreateDirectory(path);
        }

        public void RmDir(string containerName)
        {
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Permission.ManageApplications);

            if (containerName == null || containerName.Trim().Length == 0)
            {
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "containerName");
            }

            string path = ResolvePath(containerName);
            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException("Could not find path: " + containerName);
            }

            Directory.Delete(path, false);
        }

        public void Delete(string objectName)
        {
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Permission.ManageApplications);

            if (objectName == null || objectName.Trim().Length == 0)
            {
                throw new ArgumentException(Errors.ValueCannotBeNullOrEmpty, "objectName");
            }

            string path = ResolvePath(objectName);
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Could not find path: " + objectName);
            }

            File.Delete(path);
        }

        #endregion

        #region IManagerService Members

        public void CheckService()
        {
            //nothing to do here. this is just a marker method to make sure this service works.
        }

        #endregion
    }
}
