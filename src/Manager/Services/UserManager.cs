using System;
using System.Collections.Generic;

using Utilify.Platform.Manager.Persistence;
using Utilify.Platform.Manager.Security;

namespace Utilify.Platform.Manager
{
    //todo: (UserManager) expand class summary docs

#if !MONO
    [ErrorBehavior(typeof(ServiceErrorHandler))] //attach the error handling behaviour which does Exception to Fault mapping
    [System.ServiceModel.ServiceBehavior(
        Namespace = Namespaces.Manager,
        InstanceContextMode = System.ServiceModel.InstanceContextMode.Single,
        ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Multiple,
        AddressFilterMode = System.ServiceModel.AddressFilterMode.Any)]
#endif
    internal class UserManager : MarshalByRefObject, IUserManager
    {
        private Logger logger = new Logger();

        #region IUserManager Members

        public void CheckService() { }

        public Utilify.Platform.Security.UserInfo Authenticate()
        {
            string username = SecurityHelper.GetCurrentUserName();
            //should never really happen, but just in case:
            if (string.IsNullOrEmpty(username))
                throw new System.Security.Authentication.AuthenticationException("Could not get caller's identity. Authentication failed");

            User user = SecurityDao.GetUser(username);
            //should never really happen, but just in case someone deletes the user record between the ws call and this method call:
            if (user == null)
                throw new UserNotFoundException("Could not get caller's identity. Authentication failed");

            //logger.Debug("**** Authenticating User ... ");
            //logger.Debug("**** User is : " + user.Name);
            //logger.Debug("**** Group is : " + user.Group.Id + " " + user.Group.Name);

            return user.GetUserInfo();
        }

        public Utilify.Platform.Security.UserInfo[] GetUsers()
        {
            // Ensure user has permissions to Manage users
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ManageUsers);

            List<Utilify.Platform.Security.UserInfo> infos =
                new List<Utilify.Platform.Security.UserInfo>();

            try
            {
                logger.Debug("*** Call to get list of all Users.");

                IList<Utilify.Platform.Manager.Persistence.User> users =
                    SecurityDao.GetUsers(); // should be based on user and/or other criteria

                logger.Debug("*** " + users.Count + " users from db");

                foreach (Utilify.Platform.Manager.Persistence.User user in users)
                {
                    infos.Add(user.GetUserInfo());
                }

                logger.Debug("# Users = " + infos.Count);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return infos.ToArray();
        }

        public Utilify.Platform.Security.GroupInfo[] GetGroups()
        {
            // Ensure user has permissions to Manage users
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ManageUsers);

            List<Utilify.Platform.Security.GroupInfo> infos =
                new List<Utilify.Platform.Security.GroupInfo>();

            try
            {
                logger.Debug("*** Call to get list of all Groups.");

                IList<Utilify.Platform.Manager.Persistence.Group> groups =
                    SecurityDao.GetGroups(); // should be based on user and/or other criteria

                logger.Debug("*** " + groups.Count + " groups from db");

                foreach (Utilify.Platform.Manager.Persistence.Group group in groups)
                {
                    infos.Add(group.GetGroupInfo());
                }

                logger.Debug("# Groups = " + infos.Count);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

            return infos.ToArray();
        }

        public Utilify.Platform.Security.PermissionInfo[] GetPermissions()
        {
            //// Ensure user has permissions to Manage users
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ManageUsers);

            //todoLater: fix this later. this is a temp implementation:
            List<Utilify.Platform.Manager.Security.Permission> perms = SecurityHelper.GetAssignedPermissions();
            List<Utilify.Platform.Security.PermissionInfo>
                infos = new List<Utilify.Platform.Security.PermissionInfo>();
            foreach (var perm in perms)
            {
                Utilify.Platform.Security.PermissionInfo info = new Utilify.Platform.Security.PermissionInfo();
                info.Id = (int)perm;
                info.Name = perm.ToString();

                infos.Add(info);
            }

            return infos.ToArray();
        }

        public void UpdateUser(Utilify.Platform.Security.UserInfo userInfo)
        {
            // Ensure user has permissions to Manage users
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ManageUsers);

            // Check nulls
            if (userInfo == null)
                throw new ArgumentNullException("userInfo");

            try
            {
                // Check user exists
                if (!SecurityDao.UserExists(userInfo.Name))
                    throw new UserNotFoundException(Messages.UserNotFound, "UserInfo.Name");

                // Create persistence user object.
                User user = new User(userInfo);

                // Log some stuff
                logger.Debug("*** Updating user : " + user.Name + " " + user.Group.Name);

                // Save changes to database.
                SecurityDao.UpdateUser(user);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

        }

        public void CreateUser(Utilify.Platform.Security.UserInfo userInfo)
        {
            // Ensure user has permissions to Manage users
            SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ManageUsers);

            // Check nulls
            if (userInfo == null)
                throw new ArgumentNullException("userInfo");

            logger.Debug("*** Call to create user...");
            try
            {
                // Check user exists
                if (SecurityDao.UserExists(userInfo.Name))
                    throw new ArgumentException(Messages.UserAlreadyExists, "UserInfo.Name");

                // Check permissions for user admin
                //SecurityHelper.EnsurePermission(PermissionRequirement.All, Utilify.Platform.Manager.Security.Permission.ManageUsers);

                // Create persistence user object.
                User user = new User(userInfo);

                // Log some stuff
                logger.Debug("*** Creating user : " + user.Name + " " + user.Group.Name);

                // Save changes to database.
                SecurityDao.CreateUser(user);
            }
            catch (PersistenceException px)
            {
                //the ManagerErrorHandler will hide the inner exception
                throw new InternalServerException(Messages.InternalServerError, px);
            }

        }

        #endregion
    }
}
