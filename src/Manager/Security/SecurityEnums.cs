
namespace Utilify.Platform.Manager.Security
{
    internal enum Permission
    {
        // Need to keep consistent with database.
        ExecuteJob = 1,
        ManageApplications = 2,
        ManageAllApplications = 3,
        ManageUsers = 4,
        ManageExecutors = 5
    }

    internal enum IdentityType
    {
        Unknown = 0,
        WindowsIdentity = 1,
        GenericIdentity = 2, //it is important to call this GenericIdentity, since we get the exact same string 'GenericIdentity' when using our custom username authentication via WCF
        X509Identity = 4
    }

    internal enum PermissionRequirement
    {
        All = 0,
        Any = 1
    }
}
