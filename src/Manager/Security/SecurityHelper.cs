using System;
using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.ServiceModel;
using System.Text;

using Utilify.Platform.Manager.Persistence;
using System.Threading;

namespace Utilify.Platform.Manager.Security
{
    internal class SecurityHelper
    {
        internal const string PermissionClaimType = "http://www.utilify.com/permission";
        internal const string IdentityClaimType = "http://www.utilify.com/identity";

        internal static void EnsurePermission(PermissionRequirement requires, params Permission[] permissions)
        {
            bool permitted = CheckPermission(requires, permissions);

            if (!permitted)
            {
                StringBuilder msg = new StringBuilder();
                msg.AppendFormat("Failed to Authorize User '{0}' User does not have {1} of the required permissions:",
                    GetCurrentUserName(), requires.ToString().ToLower());
                bool first = true;
                foreach (Permission permission in permissions)
                {
                    if (!first)
                        msg.Append(", ");
                    msg.AppendFormat("'{0}'", permission.ToString());
                    first = false;
                }
                throw new UnauthorizedAccessException(msg.ToString());
            }
        }

        internal static void EnsurePermission(IEnumerable<string> entityIds, EntityType entity)
        {
            if (entityIds == null)
                throw new ArgumentNullException("entityIds");

            foreach (string id in entityIds)
            {
                if (id == null || id.Trim().Length == 0)
                    throw new ArgumentException(Messages.ElementNullOrEmpty, "entityIds");

                EnsurePermission(id, entity);
            }
        }

        //to simplify and remove redundant code, we combine the permission checks
        internal static void EnsurePermission(string id, EntityType entity)
        {
            if (id == null || id.Trim().Length == 0)
                throw new ArgumentException(Messages.NullOrEmpty, "id");

            switch (entity)
            {
                case EntityType.Application:
                case EntityType.Job:
                case EntityType.Dependency:
                case EntityType.Result:
                    string currentUserName = GetCurrentUserName();
                    //logger.Info("Ensuring permission on {0} for user '{1}'", entity, currentUserName);
                    bool isOwner = SecurityDao.IsOwner(currentUserName, id, entity);
                    //logger.Info("User {0} owns {1} {2}? {3}", currentUserName, entity, id, isOwner);
                    if (isOwner)
                    {
                        EnsurePermission(PermissionRequirement.Any, Permission.ManageApplications, Permission.ManageAllApplications);
                    }
                    else
                    {
                        //logger.Info("Ensuring ALL permission on {0} for user '{1}'", entity, currentUserName);
                        EnsurePermission(PermissionRequirement.All, Permission.ManageAllApplications);
                    }
                    break;
                case EntityType.Executor:
                    EnsurePermission(PermissionRequirement.All, Permission.ManageExecutors);
                    break;
                case EntityType.User:
                case EntityType.Group:
                    EnsurePermission(PermissionRequirement.All, Permission.ManageUsers);
                    break;
            }
        }

        internal static bool CheckPermission(PermissionRequirement requires, params Permission[] permissions)
        {
            bool permitted = false;

            List<Permission> assignedPermissions = GetAssignedPermissions();

            if (requires == PermissionRequirement.All)
            {
                bool failed = false;
                foreach (Permission p in permissions)
                {
                    if (!assignedPermissions.Contains(p))
                    {
                        failed = true;
                        break;
                    }
                }
                if (!failed)
                    permitted = true;
            }
            else if (requires == PermissionRequirement.Any)
            {
                foreach (Permission p in permissions)
                {
                    if (assignedPermissions.Contains(p))
                    {
                        permitted = true;
                        break;
                    }
                }
            }
            return permitted;
        }

        internal static List<Permission> GetAssignedPermissions()
        {
            List<Permission> permissions = new List<Permission>();

            //we could be operating under a non-WCF environment
            //for this reason, we set the Thread's CurrentPrincipal as well.
            //when we don't have a ServiceSecurityContext, we can use that to find out the internal username
            if (ServiceSecurityContext.Current == null)
            {
                //check the current thread's principal:
                if (Thread.CurrentPrincipal.Identity.AuthenticationType == IdentityType.GenericIdentity.ToString())
                {
                    //get the roles
                    foreach (string permission in Enum.GetNames(typeof(Permission)))
                    {
                        //we would have added the appropriate permissions as roles in the authz policy
                        if (Thread.CurrentPrincipal.IsInRole(permission))
                            permissions.Add((Permission)Enum.Parse(typeof(Permission), permission));
                    }
                }
            }
            else
            {
                // Iterate through the ClaimSets to search for Claims that satisfy the requirements
                foreach (ClaimSet cs in ServiceSecurityContext.Current.AuthorizationContext.ClaimSets)
                {
                    // The Custom Claims we have added will be issued by ClaimSet.System, so check here.
                    if (cs.Issuer == ClaimSet.System)
                    {
                        foreach (Claim c in cs.FindClaims(SecurityHelper.PermissionClaimType, Rights.PossessProperty))
                        {
                            permissions.Add((Permission)c.Resource);
                        }
                    }
                }
            }
            return permissions;
        }

        internal static string GetCurrentUserName()
        {
            string username = string.Empty;

            //we could be operating under a non-WCF environment
            //for this reason, we set the Thread's CurrentPrincipal as well.
            //when we don't have a ServiceSecurityContext, we can use that to find out the internal username
            if (ServiceSecurityContext.Current == null)
            {
                //check the current thread's principal:
                if (Thread.CurrentPrincipal.Identity.AuthenticationType == IdentityType.GenericIdentity.ToString())
                {
                    username = Thread.CurrentPrincipal.Identity.Name;
                }
            }
            else
            {
                // Iterate through the ClaimSets to search for Claims that satisfy the requirements
                foreach (ClaimSet cs in ServiceSecurityContext.Current.AuthorizationContext.ClaimSets)
                {
                    // The Custom Claims we have added will be issued by ClaimSet.System, so check here.
                    if (cs.Issuer == ClaimSet.System)
                    {
                        foreach (Claim c in cs.FindClaims(SecurityHelper.IdentityClaimType, Rights.Identity))
                        {
                            // should only be one.
                            username = (string)c.Resource;
                            break;
                        }
                    }

                    if (username != string.Empty)
                        break;
                }
            }
            return username;
        }
    }
}
