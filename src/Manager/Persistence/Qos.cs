using System;
using Utilify.Platform;


namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public class Qos
    {
        private Guid applicationId;
        private int budget;
        private DateTime deadline;
        private DateTime earliestStartTime;
        private DateTime latestStartTime;
        private PriorityLevel priority;
        private long version = DataStore.UnsavedVersionValue;

        private Qos() { }

        //used when creating new Qos objects, that are not yet persisted
        public Qos(QosInfo qos) : this(Guid.Empty, qos){}

        public Qos(Guid applicationId, QosInfo qos)
        {
            if (qos == null)
                throw new ArgumentNullException("qos");

            this.ApplicationId = applicationId;
            this.Budget = qos.Budget;

            this.EarliestStartTime = qos.EarliestStartTime;
            this.Deadline = qos.Deadline;
            this.LatestStartTime = qos.LatestStartTime;

            this.Priority = qos.Priority;
        }

        public Qos(Guid applicationId, int budget, 
            DateTime deadline, DateTime earliestStartTime, DateTime latestStartTime,
            PriorityLevel priority)
        {
            this.ApplicationId = applicationId;
            this.Budget = budget;

            this.EarliestStartTime = earliestStartTime;
            this.Deadline = deadline;
            this.LatestStartTime = latestStartTime;

            this.Priority = priority;
        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public long Version
        {
            get { return version; }
            private set { version = value; }
        }

        /// <summary>
        /// Gets the id of the application the qos applies to.
        /// </summary>
        public Guid ApplicationId
        {
            get { return applicationId; }
            private set { applicationId = value; }
        }
        public int Budget
        {
            get { return budget; }
            private set 
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("budget", "Budget cannot be less than zero");
                budget = value; 
            }
        }

        public DateTime Deadline
        {
            get { return deadline; }
            private set { deadline = Helper.MakeUtc(value); }
        }

        public DateTime EarliestStartTime
        {
            get { return earliestStartTime; }
            private set { earliestStartTime = Helper.MakeUtc(value); }
        }

        public DateTime LatestStartTime
        {
            get { return latestStartTime; }
            private set { latestStartTime = Helper.MakeUtc(value); }
        }

        public PriorityLevel Priority
        {
            get { return priority; }
            private set { priority = value; }
        }

        #region DateTime persistence properties (using ticks)

        private long DeadlineTicks
        {
            get { return deadline.Ticks; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("deadline", string.Format("Deadline cannot be less than {0}", DateTime.MinValue));
                deadline = new DateTime(value, DateTimeKind.Utc);
            }
        }

        private long EarliestStartTimeTicks
        {
            get { return earliestStartTime.Ticks; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("earliestStartTime", string.Format("Earliest start time cannot be less than {0}", DateTime.MinValue));
                earliestStartTime = new DateTime(value, DateTimeKind.Utc);
            }
        }

        private long LatestStartTimeTicks
        {
            get { return latestStartTime.Ticks; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("latestStartTime", string.Format("Latest start time cannot be less than {0}", DateTime.MinValue));
                latestStartTime = new DateTime(value, DateTimeKind.Utc);
            }
        }

        #endregion

        public QosInfo GetInfo()
        {
            return new QosInfo(this.Budget, this.Deadline, this.EarliestStartTime, this.LatestStartTime, this.Priority);
        }

        public override bool Equals(object other)
        {
            if (other == null || other.GetType() != this.GetType())
                return false;

            Qos otherQos = other as Qos;

            bool areEqual =
                (otherQos.Budget == this.Budget) &&
                (Helper.AreEqual(otherQos.Deadline, this.Deadline)) &&
                (Helper.AreEqual(otherQos.EarliestStartTime, this.EarliestStartTime)) &&
                (Helper.AreEqual(otherQos.LatestStartTime, this.LatestStartTime)) &&
                (otherQos.priority == this.priority);
            
            return areEqual;
        }

        private int hashCode = 0;
        /// <summary>
        /// See <see cref="M:System.Object.GetHashCode">System.Object.GetHashCode</see>
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            if (hashCode == 0)
            {
                hashCode = 42 ^ earliestStartTime.GetHashCode() ^ latestStartTime.GetHashCode();
                if (budget != 0)
                    hashCode = budget ^ hashCode;
                if (priority != 0)
                    hashCode = priority.GetHashCode() ^ hashCode;
            }
            return hashCode;
        } //this object is not really intended to be used as a key in a hashtable, but its good to override GetHashCode when overriding Equals

        public override string ToString()
        {
            return string.Format("Qos - Budget: {0}, Deadline: {1:r} {2}, EarliestStart: {3:r} {4}, LatestStart: {5:r} {6}, Priority: {7}",
                budget, deadline, deadline.Kind, earliestStartTime, earliestStartTime.Kind, latestStartTime, latestStartTime.Kind, priority);
        }

        internal void UpdateValues(Qos newQos)
        {
            if (newQos == null)
                throw new ArgumentNullException("newQos");
            this.Budget = newQos.Budget;
            this.Deadline = newQos.Deadline;
            this.EarliestStartTime = newQos.EarliestStartTime;
            this.LatestStartTime = newQos.LatestStartTime;
            this.Priority = newQos.Priority;
        }
    }
}