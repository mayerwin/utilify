﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilify.Platform.Security;

namespace Utilify.Platform.Manager.Persistence
{
    [Serializable]
    internal class Group
    {
        private int id;
        private string name;
        private Permission[] permissions;

        private Group() { }

        public Group(GroupInfo info) : this()
        {
            this.id = info.Id;
            this.name = info.Name;
        }

        public int Id
        {
            get
            {
                return id;
            }
            private set
            {
                this.id = value;
            }
        }

        public string Name
        {
            get { return name; }
            set 
            {
                if (value == null)
                    throw new ArgumentNullException("Name");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("User name cannot be empty", "Name");
                name = value;
            }
        }
        
        internal Permission[] Permissions
        {
            get { return permissions; }
            private set { permissions = value; }
        }

        public GroupInfo GetGroupInfo()
        {
            return new GroupInfo(id, name);
        }
    }
}
