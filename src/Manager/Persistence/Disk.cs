using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

using Utilify.Platform.Contract;

namespace Utilify.Platform.Manager.Persistence
{
    //unfortunately NHibernate forces us to use a 'public' class to generate its proxy.
    //and we need to use a proxy for good performance.
    [Serializable]
    public class Disk : IUpdatable<DiskSystemInfo>, ITransformable<DiskPerformanceInfo>, ITransformable<DiskLimitInfo>
    {
        private long id; //persistence identifier
        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

        private Guid? executorId; // link to system identifier

        private string root; // OS's name/id for the root used for linking by performance and limit infos
        private FileSystemType fileSystem;
        private long sizeTotal; //(in bytes)
        private long sizeTotalFree; //(in bytes)
        private long sizeUsageCurrent; //(in bytes)
        private long sizeUsageLimit; //(in bytes)
        private long version = DataStore.UnsavedVersionValue;

        public Disk() { }

        public Disk(DiskSystemInfo info) : this()
        {
            LoadFrom(info);
        }

        private void LoadFrom(DiskSystemInfo info)
        {
            this.Root = info.Root;
            this.FileSystem = info.FileSystem;
            this.SizeTotal = info.SizeTotal;
        }

        /// <summary>
        /// Gets the persistent version of this object
        /// </summary>
        public virtual long Version
        {
            get { return version; }
            protected set { version = value; }
        }

        public virtual Guid? ExecutorId
        {
            get { return executorId; }
            protected set { executorId = value; }
        }

        internal virtual void UpdateSystem(DiskSystemInfo info)
        {
            this.FileSystem = info.FileSystem;
            this.SizeTotal = info.SizeTotal;
        }

        internal virtual void UpdatePerformance(DiskPerformanceInfo info)
        {
            this.SizeTotalFree = info.SizeTotalFree;
            this.SizeUsageCurrent = info.SizeUsageCurrent;
        }

        internal virtual void UpdateLimits(DiskLimitInfo info)
        {
            this.SizeUsageLimit = info.SizeUsageLimit;
        }

        public virtual string Root
        {
            get { return root; }
            protected set
            {
                this.root = value;
            }
        }

        public virtual FileSystemType FileSystem
        {
            get { return fileSystem; }
            protected set
            {
                this.fileSystem = value;
            }
        }

        public virtual long SizeTotal
        {
            get { return sizeTotal; }
            protected set
            {
                this.sizeTotal = value;
            }
        }

        public virtual long SizeTotalFree
        {
            get { return sizeTotalFree; }
            protected set
            {
                this.sizeTotalFree = value;
            }
        }

        public virtual long SizeUsageCurrent
        {
            get { return sizeUsageCurrent; }
            protected set
            {
                this.sizeUsageCurrent = value;
            }
        }

        public virtual long SizeUsageLimit
        {
            get { return sizeUsageLimit; }
            protected set
            {
                this.sizeUsageLimit = value;
            }
        }

        #region ITransformable<DiskSystemInfo> Members

        DiskSystemInfo ITransformable<DiskSystemInfo>.Transform()
        {
            return new DiskSystemInfo(this.Root, this.FileSystem, this.SizeTotal);
        }

        DiskPerformanceInfo ITransformable<DiskPerformanceInfo>.Transform()
        {
            return new DiskPerformanceInfo(this.Root, this.SizeTotalFree, this.SizeUsageCurrent);
        }

        DiskLimitInfo  ITransformable<DiskLimitInfo >.Transform()
        {
            return new DiskLimitInfo(this.Root, this.SizeUsageLimit);
        }

        #endregion

        #region IUpdatable<DiskSystemInfo> Members

        bool IUpdatable<DiskSystemInfo>.CanUpdate(DiskSystemInfo info)
        {
            return (info != null && info.Root == this.Root);
        }

        void IUpdatable<DiskSystemInfo>.Update(DiskSystemInfo info)
        {
            if ((this as IUpdatable<DiskSystemInfo>).CanUpdate(info))
            {
                LoadFrom(info);
            }
        }

        #endregion
    }
}
