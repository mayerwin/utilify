using System;
using System.Collections.Generic;
using System.Text;

namespace Utilify.Platform.Manager.Persistence
{
    // Mappings are used to record which Executor(s) have been assigned a particular Job.
    // Currently only single Executor per Job is supported.
    // Mappings are created when:
    //   /- A Job is scheduled to an Executor. The Job's status is set to 'Scheduled'.
    // Mappings are removed when:
    //   /- The Job is not picked up by its mapped Executor. The Jobs status is set to 'Ready'.
    //   /- The Scheduler sets a Job's status to 'Completed'.
    //   - An aborted Job is picked up by an Executor.
    //   /- An Executor is updating its list of Jobs to Reset. (UpdateCurrentJobs).
    // When a Job is picked up by an Executor, the Mapping entry stays (perhaps we can record more information in the mappings).
    //   but the Job start time is updated and its status is set to 'Executing'.
    public class JobExecutionMapping
    {
        private long id;
        private DateTime created;
        private long version = DataStore.UnsavedVersionValue;

        private Job job;
        private Executor executor;

        private JobExecutionMapping() { }

        public JobExecutionMapping(Job job, Executor executor)
        {
            this.Job = job;
            this.Executor = executor;
            this.Created = DateTime.UtcNow;
        }

        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }

        public virtual DateTime Created
        {
            get { return created; }
            private set { created = value; }
        }

        protected virtual long CreatedTicks
        {
            get { return created.Ticks; }
            set { created = new DateTime(value, DateTimeKind.Utc); }
        }

        internal virtual Job Job
        {
            get { return job; }
            private set 
            {
                if (value == null)
                    throw new ArgumentNullException("job");

                job = value;
            }
        }

        internal virtual Executor Executor
        {
            get { return executor; }
            private set 
            {
                if (value == null)
                    throw new ArgumentNullException("executor");
                executor = value;
            }
        }

        public virtual long Version
        {
            get { return version; }
            private set { version = value; }
        }
        
    }
}
