using System;
using System.Collections.Generic;
using System.Text;

namespace Utilify.Platform
{
    internal interface ITransformable<T>
    {
        T Transform();
    }

    //funny name! can we improve?
    internal interface IUpdatable<T> : ITransformable<T>
    {
        bool CanUpdate(T info);
        void Update(T info);
    }
}
