using System;

namespace Utilify.Platform.Execution.Shared
{
    [Serializable]
    public class JobExecutionInfo
    {
        private String jobId;
        private String applicationId;
        private byte[] jobInstance;
        private String jobDirectory;

        private JobExecutionInfo() { }

        public JobExecutionInfo(String jobId, String applicationId, byte[] jobInstance, String jobDirectory)
        {
            this.jobId = jobId;
            this.applicationId = applicationId;
            this.JobInstance = jobInstance;
            this.JobDirectory = jobDirectory;
        }

        public String JobId
        {
            get { return jobId; }
            private set { jobId = value; }
        }

        public String ApplicationId
        {
            get { return applicationId; }
            private set { applicationId = value; }
        }

        public byte[] JobInstance
        {
            get { return jobInstance; }
            private set { jobInstance = value; }
        }

        public String JobDirectory
        {
            get { return jobDirectory; }
            private set { jobDirectory = value; }
        }
    }
}