using System;

namespace Utilify.Platform.Demo.RendererLibrary
{
    public class YafrayHelper
    {
        // Convert from coordinates
        public static double ToYafrayCoordX(int x, int width)
        {
            return ((x * 2) / (double)width) - 1;
        }

        public static double ToYafrayCoordY(int y, int height)
        {
            return ((y * -2) / (double)height) + 1;
        }

        public static int FromYafrayCoordX(float x, int width)
        {
            return (int)Math.Round(((x + 1) * width) / 2);
        }

        public static int FromYafrayCoordY(float y, int height)
        {
            return (int)Math.Round(((y - 1) * height) / -2);
        }

        // Change the resolution of the output
        
    }
}
