/** 
 * Decodes and shows 8 bit color mapped,black and white and 16,24 and 32 bit
 * true color uncompressed and RLE Tga images. The compressed color mapped
 * formats 32 and 33 are not implemented in this verson because I have not
 * found any information on these two formats.
 */

/**
 * This software is provided "AS IS," without a warranty of any kind.
 * anyone can use it for free,emails are welcomed concerning bugs or
 * suggestions.
 */

/**
 * TGAReader.java.  
 *
 * @version 1.0 03/26/2007
 * @author Wen Yu, yuwen_66@yahoo.com
 */

using System;
using System.IO;
using System.Drawing;
namespace Renderer {

public class TGAReader : ImageReader
{
      private TgaHeader tgaHeader;
      private int scanMode = 0;
	  private int l = 0, m = 0, n = 0, o = 0;

   /** This method could have returned an Image object directly using
     * Toolkit.getDefaultToolkit().createImage(ImageProducer) method, 
     * but this method is somewhat slow when called in a non-gui class in
     * creating an image, a subclass of JFrame would do much faster!
	 */

   public override void unpackImage(Stream stream)
   {
	   tgaHeader = new TgaHeader();
       tgaHeader.readHeader(stream);
    
	   bitsPerPixel = tgaHeader.bits_per_pixel;
 	   width = tgaHeader.width;
	   height = tgaHeader.height;
	   pix = new int[width*height];
       img = new Bitmap(width, height);
	
	   if (tgaHeader.colourmap_type != 0 && tgaHeader.colourmap_type != 1) {
		   Console.WriteLine("Can only handle colour map types of 0 and 1");
		   throw new ArgumentException("Can only handle colour map types of 0 and 1");
	   }

       scanMode = ((tgaHeader.image_descriptor&0x30)>>4);

       switch (scanMode)
       {
            case 0:
				l = height-1; m = -1; n = 0; o = 1;
				break;
            case 1:
                l = height-1; m = -1; n = width-1; o = -1;
				break;
			case 2:
                l = 0; m = 1; n = 0; o = 1;
				break;
            case 3:
				l = 0; m = 1; n = width-1; o = -1; 
				break;
			default:
                break;
       }

	   switch (tgaHeader.image_type)
	   {
	         case 0:
	               Console.WriteLine("There are no data in the image file");
	               throw new ArgumentException("There are no data in the image file");
	          case 1:
	               unpack_CMP_TgaFile(stream);
		           break;
	          case 2: 
                   unpackTrueColorTgaFile(stream);
		           break;
	          case 3:
			       unpack_BW_TgaFile(stream);
			       break;
		      case 9:
			       unpack_RLE_CMP_TgaFile(stream);
			       break;
       		  case 10:
                   unpack_RLE_TrueColor_TgaFile(stream);
			       break;
			  case 11:
			       unpack_RLE_BW_TgaFile(stream);
			       break;
			  case 32: 
              case 33:
			       Console.WriteLine("Not implemented for compressed color mapped images");
		           throw new ArgumentException("Not implemented for compressed color mapped images");
			  default:
				   Console.WriteLine("I can't find a type matches this");
			       throw new ArgumentException("I can't find a type matches this");
	   }
	}	
   
   private void unpackTrueColorTgaFile(Stream stream)
	{
         int skipover = 0;
         int nindex = 0;

         skipover +=  tgaHeader.id_length;
		 skipover += tgaHeader.colourmap_type * tgaHeader.colourmap_length; 
		 stream.Seek(skipover, SeekOrigin.Current);
         int bytes2read = tgaHeader.bits_per_pixel / 8;
			
	     byte[] brgb = new byte[bytes2read*width*height];
         stream.Read(brgb,0,bytes2read*width*height);
		 
         if (bytes2read == 3) // 24 bit image
	      {
			 Console.WriteLine("24 bits Tga uncompressed image!");

             int r = 0;
             int g = 0;
             int b = 0;

			 for(int i = 0; i < height; i++) {
			     for(int j = 0; j < width; j++) {

                     b = brgb[nindex++];
                     g = brgb[nindex++];
                     r = brgb[nindex++];
                     
                     //Console.WriteLine(String.Format("(r,g,b):({0},{1},{2})", r, g, b));

                     //int pixel = (0xff << 24) | (b & 0xff) | ((g & 0xff) << 8) | ((r & 0xff) << 16);
                     //Console.WriteLine("pixel: " + pixel);
                     //pix[width*(l+m*i)+n+o*j] = pixel;

                     //Console.ReadKey();
                     
                     img.SetPixel(j, i, Color.FromArgb(r,g,b));

		         }
			 }
		  }
		 else if (bytes2read == 4) // 32 bit image 
		  {
			 Console.WriteLine("32 bits Tga uncompressed image!");
            
			 for(int i = 0; i < height; i++) {
			      for(int j = 0; j < width; j++) {
                      pix[width*(l+m*i)+n+o*j] = ((brgb[nindex++]&0xff))|((brgb[nindex++]&0xff)<<8)|((brgb[nindex++]&0xff)<<16)|(255<<24);
			          nindex++;
				  } 
			 }
		  }
		 else if (bytes2read == 2) // 16 bit image
		  {
			 Console.WriteLine("16 bits Tga uncompressed image!");
		  
		     int r = 0,g = 0,b = 0,a = 0;  
  
			 /** The two byte entry is broken down as follows:
			  *  ARRRRRGG GGGBBBBB, where each letter represents a bit.
			  *  but, because of the lo-hi storage order, the first byte
			  *  coming from the file will actually be GGGBBBBB, and the
			  *  second will be ARRRRRGG. "A" represents an attribute.
			  */
		     for(int i = 0; i < height; i++) {
			      for(int j = 0; j < width; j++) {
                       r = ((brgb[++nindex] & 0x7c) <<1);
		               g = (((brgb[nindex] & 0x03) << 6) | ((brgb[nindex-1] & 0xe0) >> 2));
	                   b = ((brgb[nindex-1] & 0x1f)<<3);
                   //  a = (brgb[nindex] & 0x80);
			           a = 0xff;
			  		   nindex++;
		         	   pix[width*(l+m*i)+n+o*j] = ((a<<24)|(r<<16)|(g<<8)|b);
			      }
		     }
	      }
		  stream.Close();
	}
   	
    private void  unpack_RLE_TrueColor_TgaFile(Stream stream)
	{
         int skipover = 0;
		 int nindex = 0;
         int p = 0;
		 int k = 0;
		 int i = 0;
		 int j = 0;

		 skipover += tgaHeader.id_length; 
		 skipover += tgaHeader.colourmap_type * tgaHeader.colourmap_length; 
		 stream.Seek(skipover, SeekOrigin.Current);
	          
         int bytes2read = tgaHeader.bits_per_pixel / 8;
                 
		 int available = (int)stream.Length;
 
		 byte[] brgb = new byte[available];
         stream.Read(brgb,0,available);
		 
		 if (bytes2read == 3) // 24 bit image
	      {
			 Console.WriteLine("24 bits Tga RLE image!");
			 
			 while(p<width*height) { 
			       
			     k = (brgb[nindex++] & 0x7f)+1; 
						 					       
				   if ((brgb[nindex-1] & 0x80) != 0){
					   for(int q = 0; q < k; q++){
						   pix[width*(l+m*i)+n+o*j] = (0xff<<24)|((brgb[nindex]&0xff))|((brgb[nindex+1]&0xff)<<8)|((brgb[nindex+2]&0xff)<<16);
						   j++;
						   if(j%width == 0)
						   {
							   i++;
							   j = 0;
						   }
						   p++;
						   if (p >= width*height)
					       {
						      break;
					       }
					   }
				   nindex += 3;
				   }
				   else {
					   for (int q = 0; q < k; q++)
					   {
						   pix[width*(l+m*i)+n+o*j] = (0xff<<24)|((brgb[nindex++]&0xff))|((brgb[nindex++]&0xff)<<8)|((brgb[nindex++]&0xff)<<16);  
						   j++;
						   if(j%width == 0)
						   {
							   i++;
							   j = 0;
						   }
						   p++;
						   if (p >= width*height)
					       {
							   break;
					       }
					   }
				   }
			  } 
	      }
		 else if (bytes2read == 4) // 32 bit image 
		  {
		     Console.WriteLine("32 bits Tga RLE image!");
            
			
			 while(p<width*height) { 
			        
				   k = (brgb[nindex++] & 0x7f)+1; 
						 					       
					   if ((brgb[nindex-1] & 0x80) != 0){
						   for(int q = 0; q < k; q++){
							   pix[width*(l+m*i)+n+o*j] = ((brgb[nindex]&0xff))|((brgb[nindex+1]&0xff)<<8)|((brgb[nindex+2]&0xff)<<16)|(255<<24);
			     	           j++;
						       if(j%width == 0)
						       {
							     i++;
							     j = 0;
						       }
						       p++;
							   if (p >= width*height)
					     	   {
							      break;
					     	   }
							}
						   nindex += 4;
					    }
					   else {
						   for (int q = 0; q < k; q++)
						   {
							   pix[width*(l+m*i)+n+o*j] = ((brgb[nindex++]&0xff))|((brgb[nindex++]&0xff)<<8)|((brgb[nindex++]&0xff)<<16)|(255<<24);  
						       nindex++;
							   j++;
						       if(j%width == 0)
						       {
							     i++;
							     j = 0;
						       }
						       p++;
							   if (p >= width*height)
						       {
								   break;
						       }
						   }
					   }
			  } 
		  }
		 else if (bytes2read == 2) 
		  {
		     Console.WriteLine("16 bits Tga RLE image!");
			 
			 int r = 0,g = 0,b = 0,a = 0;  
  
			 /** The two byte entry is broken down as follows:
			  *  ARRRRRGG GGGBBBBB, where each letter represents a bit.
			  *  but, because of the lo-hi storage order, the first byte
			  *  coming from the file will actually be GGGBBBBB, and the
			  *  second will be ARRRRRGG. "A" represents an attribute.
			  */
			 
	
			 while(p<width*height) { 
			            
				   k = (brgb[nindex++] & 0x7f)+1; 
						 					       
					   if ((brgb[nindex-1] & 0x80) != 0){
						   r = ((brgb[++nindex] & 0x7c) <<1);
		                   g = (((brgb[nindex] & 0x03) << 6) | ((brgb[nindex-1] & 0xe0) >> 2));
	                       b = ((brgb[nindex-1] & 0x1f)<<3);
			    		   a = 0xff;
						   nindex++;
						   for(int q = 0; q < k; q++){
							   pix[width*(l+m*i)+n+o*j] = ((a<<24)|(r<<16)|(g<<8)|b);
				     	       j++;
						       if(j%width == 0)
						       {
							     i++;
							     j = 0;
						       }
						       p++;
							   if (p >= width*height)
				     	       {
							      break;
				     	       }
						   }
					   }
					   else {
						   for (int q = 0;q<k;q++)
						   {
							   r = ((brgb[++nindex] & 0x7c) <<1);
		                       g = (((brgb[nindex] & 0x03) << 6) | ((brgb[nindex-1] & 0xe0) >> 2));
	                           b = ((brgb[nindex-1] & 0x1f)<<3);
							   a = 0xff;
							   nindex++;
							   pix[width*(l+m*i)+n+o*j] = ((a<<24)|(r<<16)|(g<<8)|b);
							   j++;
						       if(j%width == 0)
						       {
							     i++;
							     j = 0;
						       }
						       p++;
							   if (p >= width*height)
							   {
							     break;
							   }
						   }
					   }
			  } 
		  }
		  stream.Close();
	}

    private void readPalette(Stream stream)
	   {
		  int index = 0, r = 0, g = 0, b = 0, a = 0;
		  int byte_per_pixel = (tgaHeader.colourmap_entry_size+1)/8;
		  int readbytes = byte_per_pixel*(tgaHeader.colourmap_length-tgaHeader.first_entry_index);
		  byte[] brgb = new byte[readbytes];
		  
		  int colorsUsed = tgaHeader.colourmap_length-tgaHeader.first_entry_index; // actual colors used
		  colorPalette = new int[colorsUsed];
		  indexedColor = true;

		  stream.Seek(tgaHeader.id_length, SeekOrigin.Current);
          stream.Seek(tgaHeader.first_entry_index, SeekOrigin.Current);
		  stream.Read(brgb,0,readbytes);

		  switch (tgaHeader.colourmap_entry_size)
		  {
		    
			 case 15:
			 case 16:
                   for (int i = 0; i < tgaHeader.colourmap_length - tgaHeader.first_entry_index; i++)
                     {
                         r = ((brgb[++index] & 0x7c) <<1);
		                 g = (((brgb[index] & 0x03) << 6) | ((brgb[index-1] & 0xe0) >> 2));
	                     b = ((brgb[index-1] & 0x1f)<<3); 
                         a = 0xff;
						 colorPalette[i] = ((a<<24)|(r<<16)|(g<<8)|b);
					     index++;
                     } 
					 break;
			 case 24:
                   for (int i = 0; i < tgaHeader.colourmap_length - tgaHeader.first_entry_index; i++)         
			             colorPalette[i] = (0xff<<24)|((brgb[index++]&0xff))|((brgb[index++]&0xff)<<8)|((brgb[index++]&0xff)<<16);
			       break;
			 case 32:
                   for (int i = 0; i < tgaHeader.colourmap_length - tgaHeader.first_entry_index; i++)         
			             colorPalette[i] = ((brgb[index++]&0xff))|((brgb[index++]&0xff)<<8)|((brgb[index++]&0xff)<<16)|((brgb[index++]&0xff)<<24);
			       break;
		     default:
                 break;
		  }
	   }

	private void unpack_CMP_TgaFile(Stream stream)
	{
          
		  Console.WriteLine("color mapped Tga uncompressed image!");

		  int index = 0;

          readPalette(stream);
         
		  if (tgaHeader.bits_per_pixel != 8)
		  {
			  Console.WriteLine("Can only handle 8 bit color mapped tga file");
			  throw new ArgumentException("Can only handle 8 bit color mapped tga file");
		  }
		
	      byte[] brgb = new byte[width*height];
          stream.Read(brgb,0,width*height);

          for(int i = 0; i < height; i++) {
		       for(int j = 0; j < width; j++) {
                    pix[width*(l+m*i)+n+o*j] = colorPalette[brgb[index++]&0xff];
		       }
		  }
		  stream.Close();
	   }


    private void  unpack_RLE_CMP_TgaFile(Stream stream)
	{
          Console.WriteLine("color mapped Tga RLE image!");

		  int nindex = 0;
          int p = 0;
		  int k = 0;
		  int i = 0;
		  int j = 0;

          if (tgaHeader.bits_per_pixel != 8)
		  {
			  Console.WriteLine("Can only handle 8 bit color mapped tga file");
			  throw new ArgumentException("Can only handle 8 bit color mapped tga file");
		  }

		  readPalette(stream);

		  int available = (int)stream.Length;
 
		  byte[] brgb = new byte[available];
          stream.Read(brgb,0,available);

          
		  while(p<width*height) { 
			            
			   k = (brgb[nindex++] & 0x7f)+1; 
						 					       
				   if ((brgb[nindex-1] & 0x80) != 0){
					   for(int q = 0; q < k; q++){
						   pix[width*(l+m*i)+n+o*j] = colorPalette[brgb[nindex]&0xff];
                           j++;
						   if(j%width == 0)
						   {
							 i++;
							 j = 0;
						   }
						   p++;
			     	       if (p >= width*height)
			     	       {
						      break;
			     	       }
					   }
					   nindex += 1;
				   }
				   else {
					   for (int q = 0; q < k; q++)
					   {
						   pix[width*(l+m*i)+n+o*j] = colorPalette[brgb[nindex++]&0xff];
						   j++;
						   if(j%width == 0)
						   {
							 i++;
							 j = 0;
						   }
						   p++;
					       if (p >= width*height)
					       {
							   break;
					       }
					   }
				   }
			} 
			stream.Close();
	   }

    private void unpack_BW_TgaFile(Stream stream)
	{
          bitsPerPixel = 1;
		  Console.WriteLine("Uncompressed Black and White Tga image!");

		  int index = 0;
		  
		  stream.Seek(tgaHeader.id_length, SeekOrigin.Current);
		  stream.Seek(tgaHeader.colourmap_type * tgaHeader.colourmap_length, SeekOrigin.Current);

          byte[] brgb = new byte[width*height];
          stream.Read(brgb,0,width*height);

          for(int i = 0; i < height; i++) {
		      for(int j = 0; j < width; j++) {
                  pix[width*(l+m*i)+n+o*j] = (0xff<<24)|((brgb[index]&0xff))|((brgb[index]&0xff)<<8)|((brgb[index++]&0xff)<<16);
		      }
		  }
		  stream.Close();
	}

	private void unpack_RLE_BW_TgaFile(Stream stream)
	{
          bitsPerPixel = 1;
          Console.WriteLine("Black and White Tga RLE image!");

		  int nindex = 0;
          int p = 0;
		  int k = 0;
		  int i = 0;
		  int j = 0;
   
          stream.Seek(tgaHeader.id_length, SeekOrigin.Current);
		  stream.Seek(tgaHeader.colourmap_type * tgaHeader.colourmap_length, SeekOrigin.Current);

		  int available = (int)stream.Length;
 		
		  byte[] brgb = new byte[available];
          stream.Read(brgb,0,available);

          while(p<width*height) { 
			            
			   k = (brgb[nindex++] & 0x7f)+1; 
						 					       
			   if ((brgb[nindex-1] & 0x80) != 0){
				   for(int q = 0; q < k; q++){
					   pix[width*(l+m*i)+n+o*j] = (0xff<<24)|((brgb[nindex]&0xff))|((brgb[nindex]&0xff)<<8)|((brgb[nindex]&0xff)<<16);
					   j++;
					   if(j%width == 0)
					   {
						 i++;
						 j = 0;
					   }
					   p++;
		     	       if (p >= width*height)
		     	       {
					      break;
		     	       }
				   }
				   nindex += 1;
			   }
			   else {
				   for (int q = 0; q < k; q++)
				   {
					   pix[width*(l+m*i)+n+o*j] = (0xff<<24)|((brgb[nindex]&0xff))|((brgb[nindex]&0xff)<<8)|((brgb[nindex++]&0xff)<<16);
					   j++;
					   if(j%width == 0)
					   {
						 i++;
						 j = 0;
					   }
					   p++;
				       if (p >= width*height)
				       {
						   break;
				       }
				   }
			   }
		  }
		  stream.Close();
	}

/**
	public static void main(String args[]) throws Exception
	{

		FileInputStream fs = new FileInputStream(args[0]);
        TGAReader reader = new TGAReader();

		long t1 = System.currentTimeMillis();
		reader.unpackImage(fs);
		long t2 = System.currentTimeMillis();
		
		Console.WriteLine("Decoding time: "+(t2-t1)+"ms");
		
		fs.close();
		
		int pix[] = reader.getImageData();
		Dimension dm = reader.getImageSize();
		int width = dm.width;
		int height = dm.height;

		final JFrame jframe = new JFrame("TGAReader");

		jframe.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent evt)
			{
				jframe.dispose();
				System.exit(0);
			}
		});
		Image img = jframe.createImage(new MemoryImageSource(width, height, pix, 0, width));
        JLabel theLabel = new JLabel(new ImageIcon(img));
        jframe.getContentPane().add(new JScrollPane(theLabel));
        jframe.setSize(400,400);
        jframe.setVisible(true);
	}
*/
    private class TgaHeader
    {
       internal byte id_length;
       internal byte colourmap_type;
       internal byte image_type;
        internal short first_entry_index;
        internal short colourmap_length;
        internal byte colourmap_entry_size;
        internal short x_origin;
        internal short y_origin;
        internal short width;
        internal short height;
	   internal byte  bits_per_pixel;
        internal byte image_descriptor;

	   internal void readHeader(Stream stream)
	   {
	     int nindex = 0;
	     byte[] header = new byte[18]; // for the 18 bit header trunk

         stream.Read(header,0,18);
         
		 id_length = header[nindex++];
	     colourmap_type = header[nindex++];
	     image_type = header[nindex++];
	     first_entry_index = (short)((header[nindex++]&0xff)|((header[nindex++]&0xff)<<8));
	     colourmap_length = (short)((header[nindex++]&0xff)|((header[nindex++]&0xff)<<8));
         colourmap_entry_size = header[nindex++];
	     x_origin = (short)((header[nindex++]&0xff)|((header[nindex++]&0xff)<<8));
	     y_origin = (short)((header[nindex++]&0xff)|((header[nindex++]&0xff)<<8));
	     width = (short)((header[nindex++]&0xff)|((header[nindex++]&0xff)<<8));
	     height = (short)((header[nindex++]&0xff)|((header[nindex++]&0xff)<<8));
         bits_per_pixel = header[nindex++];
	     image_descriptor = header[nindex++];
	   } 
    }
}
}
