namespace Utilify.Platform.Demo.FractalGenerator
{
    partial class PaletteDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbColor1 = new System.Windows.Forms.PictureBox();
            this.pbColor2 = new System.Windows.Forms.PictureBox();
            this.pbGradient = new System.Windows.Forms.PictureBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.cp = new System.Windows.Forms.ColorDialog();
            this.pbColor3 = new System.Windows.Forms.PictureBox();
            this.gbGradient = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbGradient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColor3)).BeginInit();
            this.gbGradient.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbColor1
            // 
            this.pbColor1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbColor1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbColor1.Location = new System.Drawing.Point(16, 75);
            this.pbColor1.Name = "pbColor1";
            this.pbColor1.Size = new System.Drawing.Size(25, 50);
            this.pbColor1.TabIndex = 0;
            this.pbColor1.TabStop = false;
            this.pbColor1.Click += new System.EventHandler(this.pbColor1_Click);
            // 
            // pbColor2
            // 
            this.pbColor2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbColor2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbColor2.Location = new System.Drawing.Point(216, 75);
            this.pbColor2.Name = "pbColor2";
            this.pbColor2.Size = new System.Drawing.Size(25, 50);
            this.pbColor2.TabIndex = 1;
            this.pbColor2.TabStop = false;
            this.pbColor2.Click += new System.EventHandler(this.pbColor2_Click);
            // 
            // pbGradient
            // 
            this.pbGradient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbGradient.Location = new System.Drawing.Point(16, 26);
            this.pbGradient.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.pbGradient.Name = "pbGradient";
            this.pbGradient.Size = new System.Drawing.Size(436, 36);
            this.pbGradient.TabIndex = 2;
            this.pbGradient.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(241, 174);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(160, 174);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // pbColor3
            // 
            this.pbColor3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbColor3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbColor3.Location = new System.Drawing.Point(427, 75);
            this.pbColor3.Name = "pbColor3";
            this.pbColor3.Size = new System.Drawing.Size(25, 50);
            this.pbColor3.TabIndex = 6;
            this.pbColor3.TabStop = false;
            this.pbColor3.Click += new System.EventHandler(this.pbColor3_Click);
            // 
            // gbGradient
            // 
            this.gbGradient.Controls.Add(this.pbGradient);
            this.gbGradient.Controls.Add(this.pbColor3);
            this.gbGradient.Controls.Add(this.pbColor1);
            this.gbGradient.Controls.Add(this.pbColor2);
            this.gbGradient.Location = new System.Drawing.Point(12, 12);
            this.gbGradient.Name = "gbGradient";
            this.gbGradient.Size = new System.Drawing.Size(468, 139);
            this.gbGradient.TabIndex = 7;
            this.gbGradient.TabStop = false;
            this.gbGradient.Text = "Gradient";
            // 
            // PaletteDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 209);
            this.ControlBox = false;
            this.Controls.Add(this.gbGradient);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PaletteDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Palette Settings";
            ((System.ComponentModel.ISupportInitialize)(this.pbColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbGradient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColor3)).EndInit();
            this.gbGradient.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbColor1;
        private System.Windows.Forms.PictureBox pbColor2;
        private System.Windows.Forms.PictureBox pbGradient;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ColorDialog cp;
        private System.Windows.Forms.PictureBox pbColor3;
        private System.Windows.Forms.GroupBox gbGradient;
    }
}