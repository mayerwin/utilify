using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Utilify.Framework;
using Utilify.Platform.Demo.FractalGenerator.Worker;
using Utilify.Platform.Configuration;
using Utilify.Framework.UI;
using System.Net;
using System.Threading;

namespace Utilify.Platform.Demo.FractalGenerator
{
    public partial class FractalForm : Form
    {
        #region Class Members

        private const int DEFAULT_XOFFSET = -300;//-300;
        private const int DEFAULT_YOFFSET = -210;//-200;
        private const int DEFAULT_ZOOM = 140;
        private const int DEFAULT_XSEGMENTS = 4;
        private const int DEFAULT_YSEGMENTS = 4;

        private PaletteDialog dlgColour;
        //private Color[] palette;
        private int paletteSize = 5000;
        private ColorBlend blend;
        private Bitmap gradPreviewImg;
        private Bitmap localBitmap;
        private Bitmap remoteBitmap;

        private int height;
        private int width;
        private int xOffset = DEFAULT_XOFFSET;
        private int yOffset = DEFAULT_YOFFSET;
        private int zoom = DEFAULT_ZOOM;

        private int xSegments = DEFAULT_XSEGMENTS;
        private int ySegments = DEFAULT_YSEGMENTS;

        private int xClick;
        private int yClick;
        private ConnectionSettings settings = null;

        private MouseEventHandler zoomMoveEventHandler;
        private MouseEventHandler panMoveEventHandler;

        private static readonly object bitmapLock = new object();

        private static object jobsLock = new object();
        
        private Utilify.Framework.Application app;

        private int finishedJobsCount = 0;
        private int totalNumberOfJobs = 0;

        private Utilify.Platform.Logger logger;

        #endregion

        #region Initialisation
        public FractalForm()
        {
            logger = new Utilify.Platform.Logger();

            logger.Debug("Initialising...");
            InitializeComponent();
            CustomInitializeComponent();
        }

        private void CustomInitializeComponent() {

            height = localDisplay.Height;
            width = localDisplay.Width;
            tbXSegments.Text = DEFAULT_XSEGMENTS.ToString();
            tbYSegments.Text = DEFAULT_YSEGMENTS.ToString();

            // Init Bitmaps
            localBitmap = new Bitmap(width, height);
            remoteBitmap = new Bitmap(width, height);

            localDisplay.Image = localBitmap;
            remoteDisplay.Image = remoteBitmap;

            // Set up Palette Properties & Palette Dialog
            //this.paletteSize = 256;

            blend = new ColorBlend();
            blend.Colors = new Color[] { Color.Yellow, 
                Color.Blue, 
                Color.Red, 
                Color.YellowGreen, 
                Color.Tomato, 
                Color.Teal, 
                Color.RoyalBlue, 
                Color.PaleGoldenrod, 
                Color.FromArgb(255,0,128), 
                Color.FromArgb(0,0,128) };
            blend.Positions = new float[] { 0.0F, 0.01F, 0.05F, 0.15F, 0.20F, 0.25F, 0.30F, 0.35F, 0.40F, 1.0F };

            // Palette stuff
            gradPreviewImg = new Bitmap(pbColour.Width, pbColour.Height);
            pbColour.Image = gradPreviewImg;

            dlgColour = new PaletteDialog(paletteSize, blend);
            ApplySettingsFromColourDialog();
            UpdateGradPreview();

            // Disable buttons
            // btnAbort.Enabled = false;

            // Mouse Event
            zoomMoveEventHandler = new System.Windows.Forms.MouseEventHandler(localDisplay_MouseZoomMove);
            panMoveEventHandler = new System.Windows.Forms.MouseEventHandler(localDisplay_MousePanMove);
        }
        #endregion

        #region Colour Selection

        private void pbColour_Click(object sender, EventArgs e)
        {
            if (dlgColour.ShowDialog() == DialogResult.OK)
            {
                // Do something here to handle data from dialog box.
                ApplySettingsFromColourDialog();
                Console.WriteLine("OK");
            }
            else
            {
                dlgColour.CancelChanges();
                Console.WriteLine("Cancel");
            }
        }
        
        private void UpdateGradPreview()
        {
            LinearGradientBrush gradPrevBrush = new LinearGradientBrush(pbColour.ClientRectangle, Color.Black, Color.White, 90.0F);
            gradPrevBrush.InterpolationColors = blend;
            Graphics gradPrevGraphics = Graphics.FromImage(pbColour.Image);
            gradPrevGraphics.FillRectangle(gradPrevBrush, pbColour.ClientRectangle);
            pbColour.Refresh();
        }

        private void ApplySettingsFromColourDialog()
        {
            dlgColour.ApplyChanges();
            blend = dlgColour.Blend;
            paletteSize = dlgColour.PaletteSize;
            
            //palette = dlgColour.Palette;

            //Redraw Gradient Preview
            UpdateGradPreview();
        }

        #endregion

        #region Generate & Execute Workers
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            Generate();
        }

        private void Generate()
        {
            logger.Debug("Generating Fractal...");
            DoTransitionEffect(true);

            if (chkRunRemote.Checked)
                GenerateRemote();
            if (chkRunLocal.Checked)
                GenerateLocal();
        }

        #region Generate Remote (Utilify)

        private void GenerateRemote()
        {
            Int32.TryParse(tbXSegments.Text, out xSegments);
            Int32.TryParse(tbYSegments.Text, out ySegments);
            if (xSegments < 1)
            {
                MessageBox.Show("Please enter a valid number of x-segments", "Fractal Generator", 
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (ySegments < 1)
            {
                MessageBox.Show("Please enter a valid number of y-segments", "Fractal Generator",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            logger.Debug("Creating Utilify Application.");

            // Add some QoS!
            QosInfo qos = 
                new QosInfo(1, DateTime.MaxValue, 
                                earliestTimePicker.Value,
                                latestTimePicker.Value,
                                PriorityLevel.Normal);

            // Create Username Credentials to be used during AuthN and AuthZ
            if (settings == null)
            {
                LoginForm login = new LoginForm();
                DialogResult result = login.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    settings = login.ConnectionSettings;
                }
                else
                {
                    logger.Info("The user has cancelled the fractal generation before starting.");
                    return;
                }
            }

            app = new Utilify.Framework.Application("Fractal Generator Demo " + DateTime.UtcNow.Ticks, qos, null, settings);
            //app = new Utilify.Framework.Client.Application("Fractal Generator Demo " + DateTime.UtcNow.Ticks, qos);

            totalNumberOfJobs = this.xSegments * this.ySegments;

            for (int x = 0; x < this.xSegments; x++)
            {
                for (int y = 0; y < this.ySegments; y++)
                {
                    //logger.Debug("Creating Segment ({0},{1}).", x, y);
                    // Create new Utilify Worker and add to the Application and start to do work
                    UtilifyFractalWorker job =
                        new UtilifyFractalWorker(
                            x,
                            y,
                            width / xSegments,
                            height / ySegments,
                            xOffset,
                            yOffset,
                            zoom,
                            paletteSize,
                            blend);

                    // todoDiscuss: Why cant I get any details about jobs already added to the application (count etc.).
                    //logger.Debug("Adding Segment ({0},{1}) to Application {2}.", x, y, app.Name);
                    Job remoteJob = app.AddJob(job);
                }
            }

            app.Error += new EventHandler<ErrorEventArgs>(app_Error);
            app.JobStatusChanged += new EventHandler<StatusChangedEventArgs>(app_JobStatusChanged);
            app.JobSubmitted += new EventHandler<JobSubmittedEventArgs>(app_JobSubmitted);
            app.JobCompleted += new EventHandler<JobCompletedEventArgs>(app_JobCompleted);

            logger.Debug("Starting Application {0}.", app.Name);
            app.Start();
        }

        void app_JobSubmitted(object sender, JobSubmittedEventArgs e)
        {
            logger.Debug("Job {0} Submitted.", e.Job.Id);
        }

        void app_JobCompleted(object sender, JobCompletedEventArgs e)
        {
            Interlocked.Increment(ref finishedJobsCount); //completed/cancelled

            if (e.Job.Status == JobStatus.Completed)
            {
                logger.Debug("Updating Bitmap");
                UpdateRemoteBitmap((LocalFractalWorker)e.Job.CompletedInstance);
            }
            ShowProgressMessage();
        }

        private void ShowProgressMessage()
        {
            if (this.InvokeRequired)
            {
                MethodInvoker method = ShowProgressMessage;
                method.BeginInvoke(null, null);
            }
            else
            {
                lock (this)
                {
                    statusBarLabel.Text = string.Format("Finished {0} of {1} tasks.", 
                        finishedJobsCount, totalNumberOfJobs);
                }
            }
        }

        void app_JobStatusChanged(object sender, StatusChangedEventArgs e)
        {
            logger.Debug("Job {0} Status changed {1} .", e.Status.JobId, e.Status.Status);
        }

        void app_Error(object sender, ErrorEventArgs e)
        {
            logger.Debug("Error occured while executing Application.", e.Error);
        }

        #endregion

        #region Generate Local

        private void GenerateLocal()
        {
            Int32.TryParse(tbXSegments.Text, out xSegments);
            Int32.TryParse(tbYSegments.Text, out ySegments);

            for (int x = 0; x < this.xSegments; x++)
            {
                for (int y = 0; y < this.ySegments; y++)
                {
                    // Create new localWorker and pass to new BackgroundWorker thread here and start to do work
                    LocalFractalWorker fractalGenerator =
                        new LocalFractalWorker(
                            x,
                            y,
                            width / xSegments,
                            height / ySegments,
                            xOffset,
                            yOffset,
                            zoom,
                            paletteSize,
                            blend);

                    BackgroundWorker localWorker = new BackgroundWorker();
                    localWorker.DoWork +=
                        new DoWorkEventHandler(localWorker_DoWork);
                    localWorker.RunWorkerCompleted +=
                        new RunWorkerCompletedEventHandler(localWorker_RunWorkerCompleted);

                    localWorker.RunWorkerAsync(fractalGenerator);
                }
            }
        }

        private void localWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LocalFractalWorker worker = (LocalFractalWorker)e.Argument;
            worker.Generate();
            e.Result = worker;
        }

        private void localWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UpdateLocalBitmap((LocalFractalWorker)e.Result);
        }

        #endregion

        #endregion

        #region Graphics Stuff

        private void DoTransitionEffect(bool animate)
        {
            lock (bitmapLock)
            {
                if (animate)
                {
                    Bitmap map = null;
                    for (int i = 0; i < 100; i += 10)
                    {
                        map = (Bitmap)localBitmap.Clone();
                        Graphics gr = Graphics.FromImage(map);
                        gr.FillRectangle(new SolidBrush(Color.FromArgb(i, Color.White)), 0.0F, 0.0F, (float)width, (float)height);
                        localDisplay.Image = map;
                        localDisplay.Refresh();
                        remoteDisplay.Image = map;
                        remoteDisplay.Refresh();
                    }
                    localBitmap = (Bitmap)map.Clone();
                    localDisplay.Image = localBitmap;
                    localDisplay.Refresh();
                    remoteBitmap = (Bitmap)map.Clone();
                    remoteDisplay.Image = remoteBitmap;
                    remoteDisplay.Refresh();
                }
                else
                {
                    Bitmap map = (Bitmap)localBitmap.Clone();
                    Graphics gr = Graphics.FromImage(map);
                    gr.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.White)), 0.0F, 0.0F, (float)width, (float)height);
                    localBitmap = (Bitmap)map.Clone();
                    localDisplay.Image = localBitmap;
                    localDisplay.Refresh();
                    remoteBitmap = (Bitmap)map.Clone();
                    remoteDisplay.Image = remoteBitmap;
                    remoteDisplay.Refresh();
                }
            }
        }

        //private void DoCompletedEffect()
        //{
        //    localBitmap.SetPixel(195, 210, Color.Black);
        //    localDisplay.Refresh();
        //}

        private void UpdateLocalBitmap(LocalFractalWorker worker)
        {
            lock (bitmapLock)
            {
                for (int x = 0; x < worker.SegmentWidth; x++)
                {
                    for (int y = 0; y < worker.SegmentHeight; y++)
                    {
                        localBitmap.SetPixel(x + (worker.XSegmentNum * worker.SegmentWidth),
                                                y + (worker.YSegmentNum * worker.SegmentHeight),
                                                worker.Image.GetPixel(x, y));
                    }
                }
                localDisplay.Refresh();
            }
        }

        private void UpdateRemoteBitmap(LocalFractalWorker worker)
        {
            if (worker == null)
            {
                logger.Error("Got a null worker from the manager.");
                return;
            }
            if (remoteDisplay.InvokeRequired)
            {
                //call ourselves on the UI thread.
                Action<LocalFractalWorker> updateMethod = UpdateRemoteBitmap;
                remoteDisplay.BeginInvoke(updateMethod, worker);
            }
            else
            {
                lock (bitmapLock)
                {
                    for (int x = 0; x < worker.SegmentWidth; x++)
                    {
                        for (int y = 0; y < worker.SegmentHeight; y++)
                        {
                            remoteBitmap.SetPixel(x + (worker.XSegmentNum * worker.SegmentWidth),
                                                    y + (worker.YSegmentNum * worker.SegmentHeight),
                                                    worker.Image.GetPixel(x, y));
                        }
                    }
                    remoteDisplay.Refresh();
                }
            }
        }

        private void Zoom(double zoomAmt, double xCenter, double yCenter, bool isZoomIn)
        {
            if (!isZoomIn)
            {
                zoomAmt = 1 / zoomAmt;
            }
            zoom = (int)(zoom * zoomAmt);

            double diffX = Math.Abs(Math.Abs(xCenter) - Math.Abs(xOffset));
            diffX = (diffX * zoomAmt) - diffX;
            if (Math.Abs(xCenter) < Math.Abs(xOffset))
                diffX *= -1;
            xOffset = (int)(xOffset + diffX);

            double diffY = Math.Abs(Math.Abs(yCenter) - Math.Abs(yOffset));
            diffY = (diffY * zoomAmt) - diffY;
            if (Math.Abs(height / 2.0) < Math.Abs(yOffset))
                diffY *= -1;
            yOffset = (int)(yOffset + diffY);

        }

        private void OffcenterZoom(int xUp, int yUp)
        {
            double steps = 10.0;
            double dragWidth = 0;
            double zoomAmt = 0;
            double dragHeight = 0;
            double tmpX1;
            double tmpX2;
            double tmpY1;
            double tmpY2;

            Bitmap tmpBmpA;
            Bitmap tmpBmpB = (Bitmap)localBitmap.Clone();

            // Zoom the Image (only) using a zoom effect.
            for (double i = 1; i < (steps + 1); i++)
            {
                tmpX1 = (xClick / steps) * i;
                tmpX2 = width - ((width - xUp) / steps) * i;
                tmpY1 = (yClick / steps) * i;
                tmpY2 = height - ((height - yUp) / steps) * i;

                dragWidth = tmpX2 - tmpX1;
                zoomAmt = width / dragWidth;
                dragHeight = height / zoomAmt; // tmpY2 - tmpY1;

                // Draw cropped resized portion
                tmpBmpA = localBitmap.Clone(
                    new Rectangle((int)tmpX1, (int)tmpY1, (int)dragWidth, (int)dragHeight), localBitmap.PixelFormat);
                Graphics gr = Graphics.FromImage(tmpBmpB);
                gr.DrawImage(tmpBmpA, 0, 0, width, height);
                gr.Dispose();
                localDisplay.Image = tmpBmpB;
                localDisplay.Refresh();
            }

            localBitmap = tmpBmpB;
            localDisplay.Image = localBitmap;
            localDisplay.Refresh();

            // Translate off-center to center
            double dragCenterX = xClick + (dragWidth / 2.0);
            double dragCenterY = yClick + (dragHeight / 2.0);

            double diffX = (width / 2.0) - dragCenterX;
            double diffY = (height / 2.0) - dragCenterY;

            xOffset = (int)(xOffset - diffX);
            yOffset = (int)(yOffset - diffY);

            // Then Zoom the Fractal itself.
            Zoom(zoomAmt, width / 2.0, height / 2.0, true);
        }

        private void Pan(int xUp, int yUp) {

            int panX = xUp - xClick;
            int panY = yUp - yClick;

            xOffset = xOffset - panX;
            yOffset = yOffset - panY;
        }

        #endregion

        #region Mouse Events

        private void localDisplay_MouseDown(object sender, MouseEventArgs e)
        {
            Console.WriteLine("Clicked Fractal x:"+e.X+" y:"+e.Y);
            xClick = e.X;
            yClick = e.Y;
            if (e.Button == MouseButtons.Left)
                localDisplay.MouseMove += zoomMoveEventHandler;
            else if (e.Button == MouseButtons.Right)
                localDisplay.MouseMove += panMoveEventHandler;
        }

        private void localDisplay_MouseZoomMove(object sender, MouseEventArgs e)
        {
            double x = e.X;
            if (e.X >= width)
                x = width-1;
                
            double dragWidth = x - xClick;
            if (dragWidth <= 0)
                dragWidth = 1;
            double zoomAmt = width / dragWidth;
            double dragHeight = height / zoomAmt;

            Bitmap map = null;
            lock (bitmapLock)
            {
                map = (Bitmap)localBitmap.Clone();
            
                Graphics gr = Graphics.FromImage(map);
                gr.DrawRectangle(new Pen(new SolidBrush(Color.FromArgb(200,Color.White)), 1.5F), (float)xClick, (float)yClick, (float)dragWidth, (float)dragHeight);
                localDisplay.Image = map;
                localDisplay.Refresh();
            }
        }

        private void localDisplay_MousePanMove(object sender, MouseEventArgs e)
        {
            lock (bitmapLock)
            {
                Bitmap mapA = (Bitmap)localBitmap.Clone();
                Bitmap mapB = (Bitmap)localBitmap.Clone();

                int panX = e.X - xClick;
                int panY = e.Y - yClick;

                Graphics gr = Graphics.FromImage(mapA);
                gr.Clear(Color.Black);
                gr.DrawImageUnscaled(mapB, panX, panY);
                localDisplay.Image = mapA;
                localDisplay.Refresh();
            }

            #region Alternate Pan Effects
                /*
            // ColorMatrix elements
            float[][] ptsArray = 
            { 
                new float[] {1, 0, 0, 0, 0},
                new float[] {0, 1, 0, 0, 0},
                new float[] {0, 0, 1, 0, 0},
                new float[] {0, 0, 0, 0.5F, 0},
                new float[] {0, 0, 0, 0, 1}
            };
            // Create a ColorMatrix
            ColorMatrix clrMatrix = new ColorMatrix(ptsArray);
            // Create ImageAttributes
            ImageAttributes imgAttribs = new ImageAttributes(); 
            // Set color matrix
            imgAttribs.SetColorMatrix(clrMatrix, 
            ColorMatrixFlag.Default,
            ColorAdjustType.Default);
            // Draw image with no affects
            g.DrawImage(curBitmap, 0, 0, 200, 200);
            // Draw image with ImageAttributes
            g.DrawImage(curBitmap,
            new Rectangle(205, 0, 200, 200), 
            0, 0, curBitmap.Width, curBitmap.Height, 
            GraphicsUnit.Pixel, imgAttribs) ;
            // Dispose
            curBitmap.Dispose();
            g.Dispose();
*/

                /*
                            Graphics gr = Graphics.FromImage(map);
                            gr.DrawRectangle(new Pen(new SolidBrush(Color.FromArgb(200, Color.White)), 1.5F), (float)xClick, (float)yClick, (float)dragWidth, (float)dragHeight);
                            localDisplay.Image = map;
                            localDisplay.Refresh(); 
                 */
                #endregion
           
        }

        private void localDisplay_MouseUp(object sender, MouseEventArgs e)
        {
            Console.WriteLine("Finished Clicking Fractal");

            if (e.Button == MouseButtons.Left)
            {
                localDisplay.MouseMove -= zoomMoveEventHandler;
                int x = e.X;
                if (e.X >= width)
                    x = width - 1;
                lock (bitmapLock)
                {
                    OffcenterZoom(x, e.Y);
                }
                Generate();
            }
            else if (e.Button == MouseButtons.Right)
            {
                lock (bitmapLock)
                {
                    localBitmap = (Bitmap)localDisplay.Image;
                    localDisplay.MouseMove -= panMoveEventHandler;
                    Pan(e.X, e.Y);
                }
                Generate();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            xOffset = -300;
            yOffset = -200;
            zoom = 140;
        }

        private void btnZoomIn_Click(object sender, EventArgs e)
        {
            // Use the difference of difference of the distance
            Zoom(1.5, width / 2.0, height / 2.0, true);
            Generate();
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            Zoom(1.5, width / 2.0, height / 2.0, false);
            Generate();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            yOffset -= 100;
            Generate();
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            yOffset += 100;
            Generate();
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            xOffset -= 100;
            Generate();
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            xOffset += 100;
            Generate();
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            try
            {
                app.Cancel();
                MessageBox.Show("Fractal successfully cancelled.", "Cancelled!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (Exception ex)
            {
                logger.Error("Error cancelling fractal. " + ex.Message, ex);
                MessageBox.Show("Error cancelling Fractal: " + ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        #endregion
    }
}